package at.umoser.fab.melodealer;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class SendCommand extends AsyncTask<Integer, List<String>, List<String>>{


    private ActivityGUI currentActivityGUI;
    private Session connection;
    private String command;
    private int outputType;


    SendCommand(int outputType, String command) {
        this.connection = CurrentSession.getHolder().getSession();
        this.outputType = outputType;
        this.command = command;

    }

    @Override
    protected List<String> doInBackground(Integer... integers) {
        try {
            if (outputType==0){
                execCommand();
                return null;
            } else {
                return execOutputCommand();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private List<String> execOutputCommand() throws JSchException, IOException {

        Channel channel = connection.openChannel("exec");
        ((ChannelExec)channel).setCommand(command);
        channel.setInputStream(null);
        ((ChannelExec)channel).setErrStream(System.err);
        InputStream in = channel.getInputStream();
        channel.connect();

        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        String line;
        List<String> output = new ArrayList<>();
        int index = 0;
        output.add(index, null);
        while ((line = r.readLine()) != null) {
            output.add(index, null);
            output.set(index, line);
            index++;
        }
        if (output.size()>1){
            output.remove(index);
        }
        channel.disconnect();
        return output;
    }
    private void execCommand() throws JSchException, IOException {

        Channel channel = connection.openChannel("exec");
        ((ChannelExec)channel).setCommand(command);
        channel.setInputStream(null);
        ((ChannelExec)channel).setErrStream(System.err);
        InputStream in=channel.getInputStream();
        channel.connect();

        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        StringBuilder total = new StringBuilder(in.available());
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line).append("\n");
        }

        channel.disconnect();
    }


    @Override
    protected void onPostExecute(List<String> output) {
        try {
            if (outputType!=0 && output != null && checkForError(output)){

                currentActivityGUI = CurrentSession.getHolder().getActivity();

                switch (outputType) {
                    case App.getPlaylist:
                        currentActivityGUI.createTrackList(output);
                        break;
                    case App.getStatus:
                        if (output.size()>1 && !output.get(1).startsWith("volume")) {

                            CurrentSession.getHolder().setMPDStopped(false);

                            if (command.startsWith("mpc next")) {
                                currentActivityGUI.onTrackPlayingSwitch(output.get(0), 1);
                            } else if (command.startsWith("mpc prev")) {
                                currentActivityGUI.onTrackPlayingSwitch(output.get(0), 2);
                            } else {
                                currentActivityGUI.onTrackPlayingSwitch(output.get(0), 0);
                            }

                            if (output.get(1).startsWith("[paused]")) {
                                currentActivityGUI.playpauseButton.setSelected(false);
                                currentActivityGUI.playpauseButton.setChecked(false);
                            } else if (output.get(1).startsWith("[playing]")){
                                currentActivityGUI.playpauseButton.setSelected(true);
                                currentActivityGUI.playpauseButton.setChecked(true);
                            }

                            //Update Seekbar
                            if (!command.startsWith("mpc play && mpc seek")) {
                                //Getting Percentage out of output String.
                                int playtimePercentage = Integer.parseInt(output.get(1).substring(output.get(1).lastIndexOf("(") + 1, output.get(1).lastIndexOf("%")));
                                currentActivityGUI.seekBarPlayTime.setProgress(playtimePercentage);
                            }

                            //Update Song Time Values
                            String playtimeinTime1 = output.get(1).substring(output.get(1).lastIndexOf("#"), output.get(1).indexOf("(") - 1);
                            String playtimeinTime = playtimeinTime1.substring(playtimeinTime1.lastIndexOf(" ") + 1);
                            String SongCurrTime = playtimeinTime.substring(0, playtimeinTime.indexOf("/"));
                            String SongLength = playtimeinTime.substring(playtimeinTime.indexOf("/") + 1, playtimeinTime.length());
                            currentActivityGUI.updateSongPlayTimes(SongLength, SongCurrTime);

                            String playlistSize = output.get(1).substring(output.get(1).indexOf("#") + 1, output.get(1).indexOf(":") - 4);
                            currentActivityGUI.updatePlaylistSize(playlistSize);

                            //Gets Volume - error sometimes?
                            String volumeString = output.get(2).substring( output.get(2).indexOf(":")+1 ,output.get(2).indexOf("%") );
                            if (volumeString.contains(" ")){
                                volumeString = volumeString.replace(" " ,"");
                            }
                            int volume = Integer.parseInt(volumeString);
                            if (currentActivityGUI.getVolume()!=volume) {
                                currentActivityGUI.setVolume(volume);
                            }


                        } else {
                            //MPD is stopped
                            CurrentSession.getHolder().setMPDStopped(true);
                            currentActivityGUI.updateSongPlayTimes("0:00", "0:00");
                            currentActivityGUI.seekBarPlayTime.setProgress(0);
                            String nomusic = currentActivityGUI.getString(R.string.no_music_select);
                            currentActivityGUI.onTrackPlayingSwitch(nomusic, 0);
                            currentActivityGUI.playpauseButton.setChecked(false);
                            currentActivityGUI.playpauseButton.setSelected(false);


                        }
                        break;
                    case App.getServerStats:
                        //Setting Server Stats in Settings Fragment
                        FragmentSettings fs = (FragmentSettings) currentActivityGUI.getSupportFragmentManager().findFragmentByTag("SETTINGS");
                        if (fs!=null){
                            fs.findPreference("pref_cat_device_info").setTitle(CurrentSession.getHolder().getActivity().name + " Information");
                            String summary="";

                            //Creating information Strings
                            for (int i=0; i<=7; i++){
                                summary = summary.concat(output.get(i)) + "\n";
                            }
                            summary = summary.substring(0, summary.lastIndexOf("\n"));
                            fs.findPreference("pref_device_info").setSummary(summary);
                        }

                        break;
                    case App.getAllPlaylists:
                        //MPD available Playlists
                        FragmentPlaylistControl fragmentPlaylistControl = (FragmentPlaylistControl) CurrentSession.getHolder().getActivity().getSupportFragmentManager().findFragmentByTag("PLAYLIST_CONTROL");
                        if (fragmentPlaylistControl !=null) {
                            fragmentPlaylistControl.populateList(output);
                        }
                        break;
                }
            } else if (CurrentSession.getHolder().getRestarting()) {
                Toast.makeText(CurrentSession.getHolder().getActivity(), "Synchronizing...", Toast.LENGTH_SHORT).show();
                command = "sudo service mpd status";
                CurrentSession.getHolder().setRestarting(false);
                CurrentSession.getHolder().updateMPD();
                currentActivityGUI.updatePlaylist();
                CurrentSession.getHolder().updateStatus();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        super.onPostExecute(output);
    }



    private Boolean checkForError(List<String> output) throws IOException, JSchException {
        for (String checkError : output) {
            if (checkError!=null && checkError.contains("ERROR")) {
                Log.i(App.TAG, "Found Error in Output String");
                command = "sudo service mpd restart";
                execOutputCommand();
                CurrentSession.getHolder().setRestarting(true);
                return false;
            }
        }
        return true;
    }
}
