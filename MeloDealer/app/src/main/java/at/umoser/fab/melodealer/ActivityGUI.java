package at.umoser.fab.melodealer;


import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.Session;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class ActivityGUI extends AppCompatActivity
        implements
        AdapterView.OnItemClickListener,
        FragmentSettings.FragmentListener,
        FragmentDialog.FragmentDialogListener,
        SearchView.OnQueryTextListener{


    ActivityPhone activityPhone;
    private SharedPreferences sharedPreferences;
    private String mac;
    public String name;
    private boolean settingsDebug;

    //Song ListView
    private ListView tracks;
    private ActionMode tracksActionMode;
    private AdapterMusicTitles adapterMusicTitles;
    private ArrayList<Song> trackList = new ArrayList<>();
    private ArrayList<Song> allTracks = new ArrayList<>();
    private MenuItem searchMenuItem;
    //Stores List of MPD Positions from selected Songs
    TreeSet<Song> selected_items_list = new TreeSet<>();
    private boolean allRadio;
    private int SORT_RULE = 0;

    //Empty List Layout
    private LinearLayout layoutListEmpty;

    //Volume Control stuff
    private TextView volumeTitle;
    private static int volume = 1000;
    private LinearLayout layoutSeekBarVolume;
    public SeekBar seekbarVolume;
    public final Handler volumeHandler = new Handler();
    public Runnable volumeRunnable;
    private static int volumeDisplayTime=4000;

    private SwipeRefreshLayout swipeRefreshLayout;

    //Music Control stuff
    private TextView currentTrack, songMaxTime, songCurrTime;
    SeekBar seekBarPlayTime;
    private static int SeekbarUpdateDelay = 1000;
    private TextView playlistSize;
    ToggleButton playpauseButton;
    private ImageButton nextButton, prevButton;

    //Floating Action Button Action
    private FloatingActionButton fab_add, fab_add_smartphone, fab_add_test;
    private TextView text_add_smartphone, text_add_test;
    private Animation FabOpen, FabClose, FabRotateClockwise, FabRotateAntiClockwise, TextOutRight, TextInRight;
    public Boolean isFabOpen = false;


    //TODO Make Sure Removing, state selecting and downloading works..
    //try filtering

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gui);

        swipeRefreshLayout       = (SwipeRefreshLayout)     findViewById(R.id.switchableRefresh);
        tracks                   = (ListView)               findViewById(R.id.listviewTracks);
        currentTrack             = (TextView)               findViewById(R.id.textView_currentTrack);
        songMaxTime              = (TextView)               findViewById(R.id.song_max_time);
        songCurrTime             = (TextView)               findViewById(R.id.song_curr_time);
        playlistSize             = (TextView)               findViewById(R.id.textview_playlist_size);
        volumeTitle              = (TextView)               findViewById(R.id.textViewVolumeTitle);
        seekBarPlayTime          = (SeekBar)                findViewById(R.id.seekBarPlayTime);
        seekbarVolume            = (SeekBar)                findViewById(R.id.seekbarVolume);
        playpauseButton          = (ToggleButton)           findViewById(R.id.togglePlay);
        nextButton               = (ImageButton)            findViewById(R.id.buttonNext);
        prevButton               = (ImageButton)            findViewById(R.id.buttonPrev);
        layoutSeekBarVolume      = (LinearLayout)           findViewById(R.id.layoutSeekbarVolume);
        fab_add                  = (FloatingActionButton)   findViewById(R.id.fab_add);
        fab_add_smartphone       = (FloatingActionButton)   findViewById(R.id.fab_smartphone);
        fab_add_test             = (FloatingActionButton)   findViewById(R.id.fab_test);
        text_add_smartphone      = (TextView)               findViewById(R.id.text_smartphone);
        text_add_test            = (TextView)               findViewById(R.id.text_test);
        layoutListEmpty          = (LinearLayout)           findViewById(R.id.layoutListEmpty);

        FabOpen                  = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        FabClose                 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        FabRotateClockwise       = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise);
        FabRotateAntiClockwise   = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_anticlockwise);
        TextOutRight             = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
        TextInRight              = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        settingsDebug = sharedPreferences.getBoolean(App.PREF_DEBUG, getResources().getBoolean(R.bool.default_debug));
        changeStatusBarBackgroundColor();
        getBundle(savedInstanceState);

        swipeRefreshLayoutListener();
        volumeListener();
        playTimeListener();
        playPauseButtonListener();
        nextButtonListener();
        prevButtonListener();
        floatingActionButtonListener();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save Tracklist
        if (!trackList.isEmpty()){
            outState.putParcelableArrayList(App.KEY_SONGS, allTracks);
            outState.putBoolean(App.KEY_FAB, isFabOpen);
            outState.putBoolean(App.KEY_RADIO, allRadio);
            if (!selected_items_list.isEmpty()){
                outState.putParcelableArrayList(App.KEY_SELECTED, new ArrayList<>(selected_items_list));
            }
        }
    }

    @Override
    public void onBackPressed() {
        //save Bundle to ActivitySearch
        Bundle tracksBundle = new Bundle();
        tracksBundle.putParcelableArrayList(App.KEY_SONGS, trackList);
        tracksBundle.putBoolean(App.KEY_FAB, isFabOpen);
        CurrentSession.getHolder().setBundle(tracksBundle);
        CurrentSession.getHolder().setActivity(null);
        super.onBackPressed();
    }

    public boolean updatePlaylist() {
        if (settingsDebug && CurrentSession.getHolder().getMac()!=null && CurrentSession.getHolder().getMac().equals("ff:ff:ff:ff")){
            populateTrackList();
            return false;
        } else {
            return CurrentSession.getHolder().updatePlaylist();
        }
    }
    public void updateSongPlayTimes(String maxTime, String currTime){
        songMaxTime.setText(maxTime);
        songCurrTime.setText(currTime);
    }
    public void updatePlaylistSize(String size){
        playlistSize.setText(size);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    //volume up
                    final String command = "mpc volume +2";
                    seekbarVolume.setProgress(seekbarVolume.getProgress()+2);
                    CurrentSession.getHolder().exec(App.doNothing, command);
                    layoutSeekBarVolume.setVisibility(View.VISIBLE);
                    keepVolumeSeekbarAlive();
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    //volume down
                    final String command = "mpc volume -2";
                    seekbarVolume.setProgress(seekbarVolume.getProgress()-2);
                    CurrentSession.getHolder().exec(App.doNothing, command);
                    layoutSeekBarVolume.setVisibility(View.VISIBLE);
                    keepVolumeSeekbarAlive();
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    public int getVolume(){
        return volume;
    }

    public void setVolume(int volume) {
        if (volume<100){
            ActivityGUI.volume = volume;
            seekbarVolume.setProgress(volume);
        } else if (volume>=100){
            seekbarVolume.setProgress(100);
        }
    }


    private void changeStatusBarBackgroundColor(){
        //Toolbar setup
        Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        //Changes status bar background color on lollipo devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void getBundle(Bundle savedInstanceState){
        CurrentSession.getHolder().setActivity(this);

        Bundle extras = getIntent().getExtras();

        if (extras!=null){
            mac = extras.getString(App.KEY_MAC);
            name = sharedPreferences.getString(mac, null);
            if (name!=null && !name.isEmpty() && getSupportActionBar()!=null) {
                getSupportActionBar().setTitle(name);
            }

            FragmentSettings fragmentSettings = (FragmentSettings) getSupportFragmentManager().findFragmentByTag(App.FRAG_SETTINGS);
            if (fragmentSettings!=null && fragmentSettings.isAdded()){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            if (savedInstanceState!=null){
                ArrayList<Song> arraySelected = savedInstanceState.getParcelableArrayList(App.KEY_SELECTED);
                if (arraySelected!=null && !arraySelected.isEmpty()){
                    selected_items_list.addAll(arraySelected);
                }
                allRadio = savedInstanceState.getBoolean(App.KEY_RADIO, true);
                isFabOpen = !savedInstanceState.getBoolean(App.KEY_FAB);
                showFab(true);
                allTracks = savedInstanceState.getParcelableArrayList(App.KEY_SONGS);
                if (allTracks!=null){
                    trackList.addAll(allTracks);
                }
                populateTrackList();
            } else {
                allRadio = true;
                isFabOpen = !extras.getBoolean(App.KEY_FAB);
                showFab(false);
                List<Song> bundleTrackList = null;
                bundleTrackList = extras.getParcelableArrayList(App.KEY_SONGS);
                if (bundleTrackList!=null){
                    allTracks.addAll(bundleTrackList);
                    trackList.addAll(bundleTrackList);
                    populateTrackList();
                } else {
                    updatePlaylist();
                }
            }

        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_control, menu);

        //TODO SearchView Orientation change
        // Associate searchable configuration (xml.searchable) with the SearchView
        searchMenuItem = menu.findItem(R.id.action_search);
        //Here the Menu Item is converted to the support SearchView
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    private void collapseSearchView(){
        if (searchMenuItem!=null){
            searchMenuItem.collapseActionView();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //Handles the displaying of the options menu in the settingsfragment
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragmentSettings = fm.findFragmentByTag(App.FRAG_SETTINGS);
        if (fragmentSettings!=null && fragmentSettings.isAdded()){
            for (int x=0;x<menu.size();x++){
                menu.getItem(x).setEnabled(false);
                menu.getItem(x).setVisible(false);
            }
        } else {
            for (int x=0;x<menu.size();x++){
                menu.getItem(x).setEnabled(true);
                menu.getItem(x).setVisible(true);
            }
        }



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //collapseSearchView();
        switch (item.getItemId()) {
            case R.id.action_settings:
                //open Settings
                getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, new FragmentSettings(), App.FRAG_SETTINGS)
                        .addToBackStack(null)
                        .commit();
                return true;

            case R.id.action_sort:
                //Sort Playlist
                //Display Popupmenu
                PopupMenu popupMenu = new PopupMenu(this, findViewById(R.id.action_sort));
                MenuInflater menuInflater = popupMenu.getMenuInflater();
                menuInflater.inflate(R.menu.popup_sort, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.popup_sortby_default:
                                SORT_RULE = App.SORT_BY_DEFAULT;
                                break;
                            case R.id.popup_sortby_alphabet:
                                SORT_RULE = App.SORT_BY_ALPHABET;
                                break;
                            case R.id.popup_sortby_type:
                                SORT_RULE = App.SORT_BY_TYPE;
                                break;
                            case R.id.popup_sortby_artist:
                                SORT_RULE = App.SORT_BY_ARTIST;
                                break;
                        }
                        sortTrackList();
                        return true;
                    }
                });
                popupMenu.show();


                //on Popuselected, sort the playlist
                return true;

            case R.id.action_control_playlist:
                //Show FragmentPlaylistControl
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, 0);
                fragmentTransaction.add(R.id.fragmentContainer, new FragmentPlaylistControl(), App.FRAG_PLAYLIST);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                return true;

            case R.id.action_addUSBMusic:
                //Add Songs from USB Stick
                try {
                    CurrentSession.getHolder().updateMPD();
                    CurrentSession.getHolder().exec(App.doNothing, "mpc add usbstick0/");

                    Thread.sleep(500);
                    updatePlaylist();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return true;

            case R.id.action_addLocalMusic:
                //Add Songs from MPD Music Folder
                try {
                    //adds all files directly in /home/pi/Musik folder
                    CurrentSession.getHolder().exec(App.doNothing, "sudo find /home/pi/Musik/ -maxdepth 1 -type f | mpc add");
                    Thread.sleep(500);
                    updatePlaylist();
                    CurrentSession.getHolder().updateStatus();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return true;

            case R.id.action_addRadio:
                //Add Radio Streams
                CurrentSession.getHolder().exec(App.doNothing, "mpc load Radio");
                updatePlaylist();
                CurrentSession.getHolder().updateStatus();
                return true;

            case R.id.action_clearPlaylist:
                //empty Playlist
                CurrentSession.getHolder().exec(App.doNothing, "mpc clear");
                updatePlaylist();
                CurrentSession.getHolder().updateStatus();
                return true;

            case R.id.action_system_restart:
                //restart Raspberry
                CurrentSession.getHolder().exec(App.doNothing, "sudo reboot");
                Toast.makeText(getApplicationContext(), R.string.toast_reboot, Toast.LENGTH_SHORT).show();
                CurrentSession.getHolder().clear();
                return true;

            case R.id.action_system_shutdown:
                //shutdown Raspberry
                CurrentSession.getHolder().exec(App.doNothing, "sudo shutdown -h now");
                Toast.makeText(getApplicationContext(), R.string.toast_poweroff, Toast.LENGTH_SHORT).show();
                CurrentSession.getHolder().clear();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }





    public void createTrackList(List<String> songs) {
        trackList.clear();
        allTracks.clear();
        int count;
        for (int playlistPos = 1; playlistPos < songs.size(); playlistPos++) {
            Song currSong = new Song(0, null, null, null, null);
            count = 0;

            for (String song : songs) {
                if (count==3 && song.startsWith(String.valueOf(playlistPos))) {
                    song = song.substring(song.indexOf(" ")+1);
                    currSong.setTimeLength(song);
                    currSong.initializeFlags();
                    trackList.add(currSong);
                    allTracks.add(currSong);

                    break;
                }
                if (count==2 && song.startsWith(String.valueOf(playlistPos))) {
                    song = song.substring(song.indexOf(" ")+1);
                    currSong.setTitle(song);
                    count++;
                }

                if (count==1 && song.startsWith(String.valueOf(playlistPos))) {
                    song = song.substring(song.indexOf(" ")+1);
                    currSong.setArtist(song);
                    count++;
                }

                if (count==0 && song.startsWith(String.valueOf(playlistPos))) {
                    String songNoString = song.substring(0, song.indexOf(" "));
                    int songNo = Integer.parseInt(songNoString);
                    song = song.substring(song.indexOf(" ")+1);
                    currSong.setPlaylistNo(songNo);
                    currSong.setFilePath(song);
                    count++;
                }
            }
        }
        populateTrackList();
    }

    /**
     * Sorts TrackList by SORT_RULE
     */
    private void sortTrackList(){
        switch (SORT_RULE){
            case App.SORT_BY_DEFAULT:
                trackList.clear();
                trackList.addAll(allTracks);
                break;

            case App.SORT_BY_ALPHABET:
                //Sort by alphabet
                Collections.sort(trackList, new Comparator<Song>() {
                    @Override
                    public int compare(Song a, Song b) {
                        return a.getSongName().compareTo(b.getSongName());
                    }
                });
                break;

            case App.SORT_BY_TYPE:
                //Sort by Type
                Collections.sort(trackList, new Comparator<Song>() {
                    @Override
                    public int compare(Song a, Song b) {
                        if (a.getFLAG()==b.getFLAG()){
                            return 0;
                        } else if (a.getFLAG()>b.getFLAG()){
                            return -1;
                        } else {
                            return 1;
                        }
                    }
                });
                break;

            case App.SORT_BY_ARTIST:
                //Sort by artist
                Collections.sort(trackList, new Comparator<Song>() {
                    @Override
                    public int compare(Song a, Song b) {
                        return a.getArtist().compareTo(b.getArtist());
                    }
                });
                break;
        }

        CurrentSession.getHolder().moveMpdPositions(trackList);
        populateTrackList();

    }

    public void populateTrackList() {
        //DEBUG Purposes
        if (settingsDebug && CurrentSession.getHolder().getMac().equals("ff:ff:ff:ff")){
            trackList.clear();
            trackList.add(new Song(4, "Radio Song", null, "Radio 1", ""));
            trackList.add(new Song(3, "USB SONG", null, "USB STICK 1", "USB1\\"));
            trackList.add(new Song(0, "Titel 1", null, "Artist 1", "Filepath 1"));
            trackList.add(new Song(0, "Titel 2", null, "Artist 2", "Filepath 2"));
        }

        //Creates ListView with Adapter
        if (adapterMusicTitles == null) {
            adapterMusicTitles = new AdapterMusicTitles(this, trackList);
            tracks.setAdapter(adapterMusicTitles);
            tracks.setOnItemClickListener(this);
            tracks.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            tracks.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                Song currentSong;
                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                    currentSong = trackList.get(position);
                    if (!checked) {
                        selected_items_list.remove(currentSong);
                    } else {
                        selected_items_list.add(currentSong);
                    }
                    mode.setTitle(selected_items_list.size() + getString(R.string.context_actionmode_title));
                    for (Song song:
                         selected_items_list) {
                        Log.i(App.TAG,"Selected : " + song.getSongName());
                    }
                    Log.i(App.TAG, "--------------------------------------------");


                    //Needed to enable/disable Download option
                    mode.invalidate();
                }

                @Override
                public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                    MenuInflater menuInflater = actionMode.getMenuInflater();
                    menuInflater.inflate(R.menu.context_menu_tracks, menu);
                    actionMode.setTitle(selected_items_list.size() + getString(R.string.context_actionmode_title));
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                    if (currentSong!=null && currentSong.getFLAG()!=4){
                        allRadio = false;
                    }
                    //Sets the Download Menu Item disabled  if only Radio tracks are selected
                    menu.getItem(0).setEnabled(!allRadio);
                    if (allRadio){
                        menu.getItem(0).getIcon().setAlpha(130);
                    } else {
                        menu.getItem(0).getIcon().setAlpha(255);
                    }
                    tracksActionMode = actionMode;
                    return true;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem) {
                    FragmentDialog dialogFragment;
                    ArrayList<Song> list = new ArrayList<>();
                    //MenuItem clicked
                    switch (menuItem.getItemId()) {
                        case R.id.context_download_file:
                            //TODO Download File - Ovveride Dialog Buttons - Show Progress
                            //Removes all Radio Songs before passing to FragmentDialog, since you cant download these
                            if (hasExternalStoragePermission()){
                                for (Song song : selected_items_list) {
                                    if (song.getFLAG()!=App.FLAG_RADIO){
                                        list.add(song);
                                    }
                                }
                                dialogFragment = FragmentDialog.newInstance(App.STATE_DOWNLOAD, list, allRadio);
                                dialogFragment.show(getSupportFragmentManager(), App.FRAG_DIALOG);
                            }
                            return true;
                        case R.id.context_remove:
                            list.addAll(selected_items_list);
                            dialogFragment = FragmentDialog.newInstance(App.STATE_REMOVE, list, allRadio);
                            dialogFragment.show(getSupportFragmentManager(), App.FRAG_DIALOG);
                            return true;
                        default:
                            return false;
                    }
                }

                @Override
                public void onDestroyActionMode(ActionMode actionMode) {
                    //this is called when tracksactionmode.finish() is used
                    selected_items_list.clear();
                    tracksActionMode = null;
                    currentSong = null;
                    allRadio = true;
                    tracks.clearChoices();
                }
            });

            adapterMusicTitles.notifyDataSetChanged();
        } else {
            adapterMusicTitles.notifyDataSetChanged();
        }
        showEmptyListLayout();

        swipeRefreshLayout.setRefreshing(false);
    }

    private void showEmptyListLayout(){
        //Shows text if list is empty
        if (trackList.isEmpty()){
            layoutListEmpty.setVisibility(View.VISIBLE);
        } else {
            layoutListEmpty.setVisibility(View.GONE);
        }
    }

    public void sendFileToPhone(Song song){

        String filepath = song.getFilePath();
        //Path where the Remote File is
        filepath = '"' + "/home/pi/Musik/" + filepath+ '"';
        boolean add = false;
        FileOutputStream fos = null;
        try {
            //Path where the remote File should be copied to
            String lfile = sharedPreferences.getString(App.KEY_DIRPATH, null);
            Log.i(App.TAG, "Sending " + song.getSongName() + " to location: " + lfile);
            Toast.makeText(getApplicationContext(), R.string.toast_download + lfile, Toast.LENGTH_SHORT).show();

            String prefix=null;
            if(new File(lfile).isDirectory()){
                prefix=lfile+File.separator;
            }

//            JSch jsch=new JSch();
            Session session = CurrentSession.getHolder().getSession();
            //For Authentication?
//            UserInfo ui=new MyUserInfo();
//            session.setUserInfo(ui);
//            session.connect();

            // exec 'scp -f rfile' remotely
            //String command="scp -f "+rfile;
            String command="scp -f "+filepath;
            Channel channel=session.openChannel("exec");
            ((ChannelExec)channel).setCommand(command);

            // get I/O streams for remote scp
            OutputStream out=channel.getOutputStream();
            InputStream in=channel.getInputStream();

            channel.connect();

            byte[] buf=new byte[1024];

            // send '\0'
            buf[0]=0; out.write(buf, 0, 1); out.flush();


            while(true){
                int c=checkAck(in);
                if(c!='C'){
                    break;
                }

                // read '0644 '
                in.read(buf, 0, 5);

                long filesize=0L;
                while(true){
                    if(in.read(buf, 0, 1)<0){
                        // error
                        break;
                    }
                    if(buf[0]==' ')break;
                    filesize=filesize*10L+(long)(buf[0]-'0');
                }

                String file=null;
                for(int i=0;;i++){
                    in.read(buf, i, 1);
                    if(buf[i]==(byte)0x0a){
                        file=new String(buf, 0, i);
                        break;
                    }
                }

                // send '\0'
                buf[0]=0; out.write(buf, 0, 1); out.flush();

                // read a content of lfile
                fos=new FileOutputStream(prefix==null ? lfile : prefix+file);
                int foo;
                while(true){
                    if(buf.length<filesize) foo=buf.length;
                    else foo=(int)filesize;
                    foo=in.read(buf, 0, foo);
                    if(foo<0){
                        // error
                        break;
                    }
                    fos.write(buf, 0, foo);
                    filesize-=foo;
                    if(filesize==0L) break;
                }
                fos.close();
                fos=null;

                if(checkAck(in)!=0){
                    //System.exit(0);
                    Log.i(App.TAG, "System.exit(0)");

                }

                // send '\0'
                buf[0]=0; out.write(buf, 0, 1); out.flush();
            }

            //This?
            channel.disconnect();
            //session.disconnect();

            //System.exit(0);

            Log.i(App.TAG, "Success! " + song.getFilePath() + " has been sent to " + lfile);
            Toast.makeText(this, "Song " + R.string.toast_filesent, Toast.LENGTH_SHORT).show();

            addToDatabase(lfile, song);

        } catch (Exception e){
            Log.i(App.TAG, e.toString());
            try{if(fos!=null)fos.close();}catch(Exception ee){}
        }


    }

    /**
     * Adds a given Song to the MediaStore Database
     * @param song - used to get filepath
     */
    private void addToDatabase(String directoryPath, Song song){
        String songPath = directoryPath + "/" + song.getFilePath();
        Log.i(App.TAG, "Adding " + songPath + " ...");

        MediaScannerConnection.scanFile(this,
                new String[]{songPath},
                null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String s, Uri uri) {
                        if (uri!=null){
                            Log.i(App.TAG, "addToDatabase: Success at adding Song to Database in MediaScanner.onScanCompleted");
                        } else {
                            Log.i(App.TAG, "addToDatabase: Failed at adding Song to Database in MediaScanner.onScanCompleted");
                        }

                    }
                });
    }



    static int checkAck(InputStream in) throws IOException{
        int b=in.read();
        // b may be 0 for success,
        //          1 for error,
        //          2 for fatal error,
        //          -1
        if(b==0) return b;
        if(b==-1) return b;

        if(b==1 || b==2){
            StringBuffer sb=new StringBuffer();
            int c;
            do {
                c=in.read();
                sb.append((char)c);
            }
            while(c!='\n');
            if(b==1){ // error
                Log.i(App.TAG, sb.toString());
            }
            if(b==2){ // fatal error
                Log.i(App.TAG, sb.toString());

            }
        }
        return b;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        //Sending Play Command
        if (CurrentSession.getHolder().isConnected()){
            Song song = trackList.get(position);
            TextView songTitle = (TextView) view.findViewById(R.id.song_title);
            if (!TextUtils.equals(songTitle.getText(), getString(R.string.empty_playlist))){
                String command = "mpc play " + song.getPlaylistPos();
                CurrentSession.getHolder().exec(App.getStatus, command);
            }
        }
    }



    private void swipeRefreshLayoutListener(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Update MPD Database and Playlist
                CurrentSession.getHolder().updateMPD();
                updatePlaylist();
            }
        });
        //Hides Floating Action Buttons
        tracks.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (fab_add_smartphone.getVisibility() == View.VISIBLE){
                    showFab(true);
                }
                return false;
            }
        });
    }

    public void volumeListener() {
        layoutSeekBarVolume.setVisibility(View.GONE);
        if (name!=null) {
            volumeTitle.setText(name);
        } else {
            volumeTitle.setText(getString(R.string.app_name));
        }

        seekbarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressvalue;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressvalue = progress;
                keepVolumeSeekbarAlive();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (progressvalue <= 100) {
                    final String setVolumeCommand = "mpc volume " + progressvalue;
                    CurrentSession.getHolder().exec(App.doNothing, setVolumeCommand);
                }
            }
        });
    }

    public void keepVolumeSeekbarAlive(){
        volumeHandler.removeCallbacks(volumeRunnable);
        volumeHandler.postDelayed(volumeRunnable = new Runnable() {
            @Override
            public void run() {
                layoutSeekBarVolume.setVisibility(View.GONE);
            }
        }, volumeDisplayTime);
    }



    private void playTimeListener() {
        //Updatet die Seekbar alle 300 Millisekunden.
        final Handler playTimeHandler = new Handler();
        playTimeHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!CurrentSession.getHolder().updateStatus()){
                    //TODO Test: If Session is null, the Handler should stop sending commands.
                    playTimeHandler.removeCallbacksAndMessages(null);
                } else {
                    playTimeHandler.postDelayed(this, SeekbarUpdateDelay);
                }
            }
        }, SeekbarUpdateDelay);


        seekBarPlayTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressValue;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressValue = progress;
                if (progress == 0) {
                    CurrentSession.getHolder().updateStatus();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                String command = "mpc play && mpc seek " + progressValue + "%";
                CurrentSession.getHolder().exec(App.getStatus, command);
            }
        });

    }

    private void playPauseButtonListener() {

        playpauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CurrentSession.getHolder().isConnected()) {
                    if (playpauseButton.isChecked()) {
                        // playpauseButton ist on
                        //playpauseButton.setSelected(true); //Ändert Button Hintergrund
                        final String playpausecommand = "mpc play";
                        CurrentSession.getHolder().exec(App.getStatus, playpausecommand);

                    } else {
                        // playpauseButton ist off
                        //playpauseButton.setSelected(false);
                        final String playpausecommand = "mpc pause";
                        CurrentSession.getHolder().exec(App.getStatus, playpausecommand);
                    }
                }

            }
        });
    }

    private void nextButtonListener() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CurrentSession.getHolder().isConnected()) {
                    final String nextcommand = "mpc next";
                    CurrentSession.getHolder().exec(App.getStatus, nextcommand);

                }
            }
        });
    }

    private void prevButtonListener() {
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CurrentSession.getHolder().isConnected()) {
                    final String prevcommand = "mpc prev";
                    CurrentSession.getHolder().exec(App.getStatus, prevcommand);
                }
            }
        });
    }


    private void floatingActionButtonListener(){
        //Has to be set for Pre Lollipop Devices for animation
        if(Build.VERSION.SDK_INT > 11){
            fab_add.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseSearchView();
                showFab(true);
            }
        });

        fab_add_smartphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseSearchView();
                if (hasExternalStoragePermission()){

                    if (activityPhone==null){
                        activityPhone = new ActivityPhone();
                    }
                    Intent phoneAct = new Intent(getApplicationContext(), ActivityPhone.class);
                    String name = sharedPreferences.getString(CurrentSession.getHolder().getMac(), null);
                    startActivity(phoneAct);
                    overridePendingTransition( android.R.anim.fade_in, android.R.anim.fade_out );
                    //Hides Fab after activityphone is added
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showFab(false);
                        }
                    }, 100);
                }

            }
        });

        fab_add_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseSearchView();
                showFab(false);
                Toast.makeText(getApplicationContext(), "Clicked Test Fab", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showFab(Boolean showAnimation){
        if (isFabOpen){
            if (showAnimation){
                fab_add.startAnimation(FabRotateAntiClockwise);
                fab_add_smartphone.startAnimation(FabClose);
                fab_add_test.startAnimation(FabClose);
                text_add_smartphone.startAnimation(TextOutRight);
                text_add_test.startAnimation(TextOutRight);
            } else {
                fab_add.clearAnimation();
            }
            fab_add_smartphone.setVisibility(View.GONE);
            fab_add_test.setVisibility(View.GONE);
            fab_add_smartphone.setClickable(false);
            fab_add_test.setClickable(false);
            text_add_smartphone.setVisibility(View.GONE);
            text_add_test.setVisibility(View.GONE);
            isFabOpen = false;
        } else {
            if (showAnimation){
                fab_add.startAnimation(FabRotateClockwise);
                fab_add_smartphone.startAnimation(FabOpen);
                fab_add_test.startAnimation(FabOpen);
                text_add_smartphone.startAnimation(TextInRight);
                text_add_test.startAnimation(TextInRight);
            } else {
                fab_add.clearAnimation();
            }
            fab_add_smartphone.setVisibility(View.VISIBLE);
            fab_add_test.setVisibility(View.VISIBLE);
            fab_add_smartphone.setClickable(true);
            fab_add_test.setClickable(true);
            text_add_smartphone.setVisibility(View.VISIBLE);
            text_add_test.setVisibility(View.VISIBLE);
            isFabOpen = true;
        }
    }


    public void onTrackPlayingSwitch(String currSongTitle, int animType) {
        if (currSongTitle != null && !currSongTitle.isEmpty()) {
            //Animationsdefinition für den TextSwitcher trackCurrPlayingSwitcher
            Animation leftin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_left);
            Animation rightout = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
            Animation rightin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right);
            Animation leftout = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);


            // Default Text wenn kein Songtitel zurückkommt.
            if (currSongTitle.startsWith("Updating") || currSongTitle.startsWith("volume:")) {
                currSongTitle = "";
            }
            //USB Dateipfad entfernen
            else if (currSongTitle.startsWith("usbstick")) {
                currSongTitle = currSongTitle.substring(currSongTitle.lastIndexOf("/")+1, currSongTitle.length());
            }
            //Radio Name aus String filtern
            else if (currSongTitle.startsWith("http://")) {
                currSongTitle = currSongTitle.split("#")[1];
            }
            //Bei Songname Dateiendung entfernen.
            else {
                //DateiEndung aus String schneiden
                currSongTitle = currSongTitle.split(".mp3")[0];
            }


             //TODO ANIMATIONS
            //Wählt TextswitchAnimation aus.
            switch (animType) {
                //case 0 ist mpc current befehl
                //Keine Animation wird benötigt
                case 0:
                    currentTrack.setText(currSongTitle);
                    break;

                //case 1 ist mpc next befehl
                case 1:
                    //Animationswechsel definierung
                    //currentTrack.setInAnimation(rightin);
                    //currentTrack.setOutAnimation(leftout);
                    currentTrack.setText(currSongTitle);
                    break;

                //case 2 ist mpc prev befehl
                case 2:
                    //Animationswechsel definierung
                    //currentTrack.setInAnimation(leftin);
                    //currentTrack.setOutAnimation(rightout);
                    currentTrack.setText(currSongTitle);
                    break;
            }

        } else {
            currentTrack.setText("");
        }
    }

    @Override
    public void closeSettings(boolean checkStatus, boolean checkHideResults, FragmentManager.OnBackStackChangedListener listener) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportFragmentManager().removeOnBackStackChangedListener(listener);
        FragmentSettings fragmentSettings = (FragmentSettings) getSupportFragmentManager().findFragmentByTag(App.FRAG_SETTINGS);
        if (fragmentSettings!=null){
            getSupportFragmentManager().beginTransaction().remove(fragmentSettings).commit();
        }
    }

    //Checks need Permissions for Reading/Writing Data
    private boolean hasExternalStoragePermission(){
        //Checks Permission for SD Card writing
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            //Request Permission to read external Data
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        App.MY_PERMISSION_REQUEST_EXTERNAL_STORAGE);
            }
            return false;
        }
    }


    @Override
    public void onDialogOption1(int STATE) {
        switch (STATE){
            case App.STATE_DOWNLOAD:
                if (hasExternalStoragePermission()){
                    //Sending all the songs to device
                    for (Song song : selected_items_list) {
                        //removing radio links since those cant be downloaded
                        //Sending remote File to Android location
                        //Display Dialog box with list to accept download
                        if (song.getFLAG()!=4 && CurrentSession.getHolder().isConnected()){
                            sendFileToPhone(song);
                        }
                    }
                    if (tracksActionMode!=null){
                        tracksActionMode.finish();
                    }
                }

                break;
            case App.STATE_REMOVE:
                //Removing Tracks from List
                int countRemove = 0;
                for (Song song : selected_items_list) {
                    int mpdPosition = song.getPlaylistPos();
                    mpdPosition = mpdPosition - countRemove;
                    String removeFromPlaylistCommand = "mpc del " + mpdPosition;
                    Log.i(App.TAG, "Deleting " + song.getSongName() + " mpd pos: " + mpdPosition);
                    CurrentSession.getHolder().exec(App.doNothing, removeFromPlaylistCommand);
                    countRemove = countRemove + 1;
                }
                break;
        }

        if (tracksActionMode!=null){
            tracksActionMode.finish();
        }
        updatePlaylist();
    }

    @Override
    public void onDialogOption2(int STATE) {
        switch (STATE){
            case App.STATE_REMOVE:
                //Deleting Tracks
                int countDelete = 0;
                for (Song song : selected_items_list) {
                    int mpdPosition = song.getPlaylistPos();
                    mpdPosition = mpdPosition - countDelete;
                    String removeFromPlaylistCommand = "mpc del " + mpdPosition;
                    CurrentSession.getHolder().exec(App.doNothing, removeFromPlaylistCommand);
                    countDelete = countDelete + 1;

                    if (song.getFLAG()==0) {
                        String deleteFromPiCommand = "sudo rm \"/home/pi/Musik/" + song.getFilePath() + "\"";
                        Toast.makeText(getApplicationContext(), R.string.toast_delete, Toast.LENGTH_SHORT).show();
                        CurrentSession.getHolder().exec(App.doNothing, deleteFromPiCommand);
                    }
                }
                break;
        }
        if (tracksActionMode!=null){
            tracksActionMode.finish();
        }
        updatePlaylist();
    }

    /**
     * Called when every search is submitted, not needed
     * @param query
     * @return
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * Called when every new char is added to SearchView
     * @param newText
     * @return
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        //Search Magic
        newText = newText.toLowerCase();
        trackList.clear();

        for (Song song : allTracks) {
            String name = song.getSongName().toLowerCase();
            if (name.contains(newText)){
                trackList.add(song);
            }
        }
        populateTrackList();
        return true;
    }
}
