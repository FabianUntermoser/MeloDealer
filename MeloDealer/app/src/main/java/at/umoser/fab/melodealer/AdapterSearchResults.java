package at.umoser.fab.melodealer;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeSet;

class AdapterSearchResults extends RecyclerView.Adapter<AdapterSearchResults.ViewHolder> {


    private ActivitySearch activitySearch;
    private Context context;
    private ArrayList<SearchResult> adapterList = new ArrayList<>();

    private boolean isAdapterEmpty = false;
    //Indicates whether the "Found Result" Text should be animated (only true if list got bigger or smaller)
    private boolean animText = false;


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        ActivitySearch activitySearch;
        ImageView musikbox_icon;
        TextView musikbox_name;
        TextView musikbox_ip;
        TextView musikbox_mac;

        ViewHolder(View itemView, ActivitySearch activitySearch) {
            super(itemView);
            this.activitySearch = activitySearch;
            musikbox_icon = (ImageView) itemView.findViewById(R.id.musikbox_icon);
            musikbox_name = (TextView) itemView.findViewById(R.id.musikbox_name);
            musikbox_ip = (TextView) itemView.findViewById(R.id.musikbox_ip);
            musikbox_mac = (TextView) itemView.findViewById(R.id.musikbox_mac);

            itemView.setOnClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        //ITEMVIEW ONCLICK LISTENER
        @Override
        public void onClick(View view) {
            activitySearch.onRecyclerItemClick(view, getAdapterPosition());
        }

        //ITEMVIEW ONCONTEXTCREATE
        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            activitySearch.onRecyclerCreateContext(contextMenu, view, contextMenuInfo, getAdapterPosition());
        }

    }

    AdapterSearchResults(ArrayList<SearchResult> adapterList, Context context){
        isAdapterEmpty = true;
        this.adapterList = adapterList;
        this.context = context;
        activitySearch = (ActivitySearch) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        //Inflates Null Layout if List is empty
        View view;
        if(adapterList.isEmpty()){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.null_item, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_server_names, parent, false);
        }
        return new ViewHolder(view, activitySearch);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Does no work if null layout is used
        if (!adapterList.isEmpty()){

            SearchResult result = adapterList.get(position);

            String name = result.getName();
            String ip = result.getIp();
            String mac = result.getMac();
            int status = result.getStatus();
            boolean hide = result.getHide();

            //Default values
            holder.musikbox_ip.setTextColor(Color.LTGRAY);
            holder.musikbox_mac.setTextColor(Color.LTGRAY);
            holder.musikbox_name.setTextColor(Color.LTGRAY);
            holder.musikbox_icon.setBackgroundResource(R.drawable.selected_speaker);

            if (name != null) {
                holder.musikbox_name.setText(name);
                holder.musikbox_ip.setText("IP: " + ip);
                holder.musikbox_mac.setText("MAC: " + mac);
            } else {
                holder.musikbox_name.setText(ip);
                holder.musikbox_ip.setText("");
                holder.musikbox_mac.setText("");
            }

            //Layout changes on status
            switch (status){
                case App.STATUS_SSH:
                    //Device Found has open SSH Port
                    holder.musikbox_icon.setBackgroundResource(R.drawable.ic_router_black_24px);
                    holder.musikbox_ip.setText(context.getString(R.string.t_openssh));
                    holder.musikbox_mac.setText(context.getString(R.string.t_openssh_desc));
                    break;
                case App.STATUS_MPD:
                    //Device Found has open MPD Port
                    holder.musikbox_ip.setText(context.getString(R.string.t_openmpd));
                    holder.musikbox_mac.setText(context.getString(R.string.t_openmpd_desc));
                    break;
                case App.STATUS_SSH_MPD:
                    //Device Found has open SSH & MPD Port
                    holder.musikbox_ip.setText(context.getString(R.string.t_opensshmpd));
                    holder.musikbox_mac.setText(context.getString(R.string.t_opensshmpd_desc));
                    holder.musikbox_name.setTextColor(context.getResources().getColor(R.color.textLightBlue));

                    break;
                case App.STATUS_OK:
                    //Connectable Music Server
                    holder.musikbox_name.setTextColor(context.getResources().getColor(R.color.textLightBlue));
                    holder.musikbox_ip.setTextColor(context.getResources().getColor(R.color.textLightBlue));
                    holder.musikbox_mac.setTextColor(context.getResources().getColor(R.color.textLightBlue));


                    if (CurrentSession.getHolder().getMac()!=null && CurrentSession.getHolder().getMac().equals(mac)){
                        holder.musikbox_icon.setSelected(true);
                    } else {
                        holder.musikbox_icon.setSelected(false);
                    }
                    break;
            }


            if (hide){
                holder.itemView.setVisibility(View.GONE);
                holder.itemView.setEnabled(false);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0));
            } else {
                holder.itemView.setVisibility(View.VISIBLE);
                holder.itemView.setEnabled(true);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }

            //Uses Animation for Text "Results found"
            if (animText){
                AnimationList.animationBlendIn(holder.activitySearch.getResultText());
            }
            if (isAdapterEmpty){
                AnimationList.animationBlendIn(holder.itemView);
                isAdapterEmpty = false;
            }
            update();
        }

    }

    //Returns Size of currently displayed List
    @Override
    public int getItemCount() {
        return adapterList.size();
    }

    //Returns SearchResult Object at given position
    SearchResult getItemAtPosition(int pos){
        return adapterList.get(pos);
    }

    //Checks if List is empty
    Boolean isEmpty(){
        return adapterList.isEmpty();
    }

    //Returns Adapter Array<SearchResult>
    ArrayList<SearchResult> getAll(){
        return adapterList;
    }

    //Removes all Items from List
    void removeAll(){
        isAdapterEmpty = true;
        adapterList.clear();
        notifyItemRangeRemoved(0, getItemCount());
        activitySearch.showEmptyListLayout();
        activitySearch.updateResultText();
    }

    //TODO after connecting, then changing login info, clear connection and seelected speaker icon
    //TODO fix replace, after checkStatus is called.
    //Adding all Items that are not currently in the Arraylist with correct animtions
    void replace(ArrayList<SearchResult> results) {

        //Log.i(App.TAG, "Replacing List ----------------------------------------");
        //Log.i(App.TAG, "    NewList Size: " + results.size());
        //Log.i(App.TAG, "    OldList Size: " + adapterList.size());
        //for (SearchResult re : results) {
        //    Log.i(App.TAG, "    New Results: " + re.getIp() + " " + re.getMac());
        //}
        //for (SearchResult re : adapterList) {
        //    Log.i(App.TAG, "    Old Results: " + re.getIp() + " " + re.getMac());
        //}
        //Log.i(App.TAG, "--------------------------");

        //Checks if Dataset only needs to be updated or replaced
        if (results.size()==adapterList.size()){
            //Log.i(App.TAG, "    Replacing Items with no Animations");
            animText = false;
            adapterList.clear();
            adapterList.addAll(results);
        } else {

            animText = true;
            isAdapterEmpty = false;
            int currentListSize = getItemCount();

            //Swap New Results with old Results if they match
            TreeSet<Integer> indexCurr = new TreeSet<>();
            TreeSet<Integer> indexNew = new TreeSet<>();
            int biggerList = currentListSize>results.size()?currentListSize:results.size();
            for (int x=0; x<biggerList; x++) {
                SearchResult cur = null;
                if (x<currentListSize){
                    cur = getItemAtPosition(x);
                }
                if (cur!=null) {
                    for (int y=0; y<results.size(); y++) {
                        SearchResult newR = results.get(y);
                        if (cur.equals(newR)){
                            //swapping results
                            adapterList.set(x, results.get(y));
                            notifyItemChanged(x);
                            indexCurr.add(x);
                            indexNew.add(y);
                        }
                    }
                }
            }

            //Gets positions of currentList and new List that are not indexed
            TreeSet<Integer> curNotIndexed = new TreeSet<>();
            TreeSet<Integer> newNotIndexed = new TreeSet<>();
            for (int i=0; i<biggerList; i++){
                if (i<currentListSize && !indexCurr.contains(i)){
                    curNotIndexed.add(i);
                }
                if (i<results.size() && !indexNew.contains(i)){
                    newNotIndexed.add(i);
                }
            }

            //Removing indexes from list that did not match with any new result
            int countRemoved = 0;
            for (Integer i : curNotIndexed) {
                remove((i-countRemoved));
                countRemoved++;
            }

            //Adds indexes from new list that did not match with any old result
            for (Integer i : newNotIndexed) {
                add(0, results.get(i));
            }
        }
        update();
    }

    //Adds SearchResult Item to List
    void add(int pos, SearchResult item){
        adapterList.add(pos, item);
        notifyItemInserted(pos);
        update();
    }

    //Removes SearchResult Item from List at given position
    void remove (int position){
        adapterList.remove(position);
        notifyItemRemoved(position);
        if (getItemCount()==0){
            isAdapterEmpty = true;
        } else {
            AnimationList.animationBlendIn(activitySearch.getResultText());
        }
        update();
    }

    //Updates Layouts and Texts
    void update(){
        activitySearch.showEmptyListLayout();
        activitySearch.updateResultText();
    }




}
