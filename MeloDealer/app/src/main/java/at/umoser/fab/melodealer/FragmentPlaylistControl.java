package at.umoser.fab.melodealer;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;


public class FragmentPlaylistControl extends ListFragment {

    View rootView;
    ActivityGUI activityControl;
    Button okButton;
    Button savePlaylistButton;

    TextInputLayout layName;
    EditText playlistName;

    List<String> playlists = new ArrayList<>();

    ListView listViewPlaylists;
    AdapterDialog<String> adapter;


    public FragmentPlaylistControl() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_playlist_controls, container, false);
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        activityControl = (ActivityGUI) getActivity();
        adapter = new AdapterDialog<>(getActivity().getApplicationContext(), playlists);
        setListAdapter(adapter);

        layName = (TextInputLayout) rootView.findViewById(R.id.text_layout_playlist);
        playlistName = (EditText) rootView.findViewById(R.id.save_playlist_name);
        okButton = (Button) rootView.findViewById(R.id.button_playlist_ok);
        savePlaylistButton = (Button) rootView.findViewById(R.id.button_playlist_save);
        listViewPlaylists = getListView();
        registerForContextMenu(listViewPlaylists);

        //initialize list
        updateList();

        //fades out fragment and removes it
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment thisFragment = getFragmentManager().findFragmentByTag(App.FRAG_PLAYLIST);
                if (thisFragment != null) {
                    getFragmentManager().beginTransaction().setCustomAnimations(0, android.R.anim.fade_out).remove(thisFragment).commit();
                    getFragmentManager().popBackStack();
                    activityControl.updatePlaylist();
                }
            }
        });

        //saves playlist under specified name
        savePlaylistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = playlistName.getText().toString();

                if (!name.isEmpty()) {
                    layName.setHintEnabled(true);
                    layName.setErrorEnabled(false);
                    //remove keyboard
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
                    playlistName.setText("");
                    if (CurrentSession.getHolder().isConnected()) {
                        name = name.replace(" ", "\\ ");

                        String savePlaylistCommand = "mpc save " + name;
                        CurrentSession.getHolder().exec(App.doNothing, savePlaylistCommand);
                        Toast.makeText(getActivity().getApplicationContext(), "Playlist saved as " + name, Toast.LENGTH_SHORT).show();
                    }
                    updateList();
                } else {
                    layName.setHintEnabled(false);
                    layName.setErrorEnabled(true);
                    layName.setError(getString(R.string.empty_playlist));
                    layName.setHintEnabled(false);
                }
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_playlist_add, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        String playlistName = (String) listViewPlaylists.getItemAtPosition(info.position);

        switch (item.getItemId()) {
            case R.id.context_playlist_add:

                addPlaylist(playlistName);

                return super.onContextItemSelected(item);
            case R.id.context_playlist_delete:

                removePlaylist(playlistName);

                return super.onContextItemSelected(item);
            default:
                return super.onContextItemSelected(item);
        }

    }

    private void updateList(){
        if (CurrentSession.getHolder().isConnected()) {
            String listPlaylists = "mpc lsplaylists";
            CurrentSession.getHolder().exec(App.getAllPlaylists, listPlaylists);
        }
    }

    public void populateList(List<String> playlists) {
        this.playlists.clear();
        this.playlists.addAll(playlists);
        adapter.notifyDataSetChanged();
    }

    public void addPlaylist(String playlistName) {
        if (CurrentSession.getHolder().isConnected() && adapter.getCount() > 0) {
            playlistName = playlistName.replace(" ", "\\ ");
            String addPlaylistCommand = "mpc load " + playlistName;
            CurrentSession.getHolder().exec(App.doNothing, addPlaylistCommand);
            activityControl.updatePlaylist();
            Toast.makeText(getActivity().getApplicationContext(), "Loading Playlist " + playlistName, Toast.LENGTH_SHORT).show();
        }
    }

    public void removePlaylist(String playlistName) {
        if (CurrentSession.getHolder().isConnected() && adapter.getCount() > 0) {
            playlistName = playlistName.replace(" ", "\\ ");
            String removePlaylistCommand = "mpc rm " + playlistName;
            CurrentSession.getHolder().exec(App.doNothing, removePlaylistCommand);
            activityControl.updatePlaylist();
            Toast.makeText(getActivity().getApplicationContext(), "Playlist " + playlistName + " deleted", Toast.LENGTH_SHORT).show();
        }
        updateList();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        TextView playlistNameTextView = (TextView) v.findViewById(R.id.t_dir);
        String playlistName = (String) playlistNameTextView.getText();
        addPlaylist(playlistName);
        super.onListItemClick(l, v, position, id);
    }

}