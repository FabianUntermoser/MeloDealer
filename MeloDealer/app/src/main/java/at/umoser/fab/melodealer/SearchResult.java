package at.umoser.fab.melodealer;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.Comparator;

public class SearchResult {


    private SharedPreferences sharedPreferences;

    private String name;
    private String ipaddr;
    private String macaddr;
    private boolean hide = false;

    private int status = 0;


    public SearchResult(SharedPreferences preferences, String ip, String mac, int status){
        sharedPreferences = preferences;
        ipaddr = ip;
        macaddr = mac;
        this.status = status;

    }

    public String getName() {
        name = sharedPreferences.getString(macaddr, null);
        return name;
    }

    void setName(SharedPreferences preferences, String newName) {
        sharedPreferences = preferences;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(macaddr, newName);
        editor.commit();
        name = newName;
    }

    String getIp() {
        return ipaddr;
    }

    String getMac() {
        return macaddr;
    }

    int getStatus() {
        return status;
    }

    boolean getHide() {
        return hide;
    }

    void setHide(boolean hide) {
        this.hide = hide;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SearchResult){
            SearchResult r = (SearchResult) obj;
            if (getMac()!=null && r.getMac()!=null){
                return getMac().equals(r.getMac());
            } else {
                return getIp().equals(r.getIp());
            }
        } else {
            return false;
        }
    }
}
