package at.umoser.fab.melodealer;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;


public class FragmentConnect extends Fragment {


    private SharedPreferences sharedPreferences;

    private View rootView;
    private ConnectionCallbacks mCallback;

    private TextView textView;
    private boolean startControlAct;
    private String ip;


    public FragmentConnect() {
    }

    interface ConnectionCallbacks {
        void onPostExecute(Session session, String ip, String mac, boolean startControlAct);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ConnectionCallbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    public static FragmentConnect newInstance(String ip, boolean startControlAct){
        FragmentConnect fragmentConnect = new FragmentConnect();
        Bundle bundle = new Bundle();
        bundle.putString(App.KEY_IP, ip);
        bundle.putBoolean(App.KEY_CONTROLACT, startControlAct);
        fragmentConnect.setArguments(bundle);
        return fragmentConnect;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_connect, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        textView = (TextView) rootView.findViewById(R.id.loading_spinner_main_text);

        ip = bundle.getString(App.KEY_IP);
        startControlAct = bundle.getBoolean(App.KEY_CONTROLACT);

        String loadingText;
        if (startControlAct){
            loadingText = getString(R.string.t_connect) + ip;
        } else {
            loadingText = getString(R.string.t_setupreset) + ip;
        }
        textView.setText(loadingText);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        new AsyncTask<Void, Integer, Session>() {
            String macAddress;

            @Override
            protected Session doInBackground(Void... voids) {
                try {
                    JSch jSch = new JSch();
                    Session session = jSch.getSession(CurrentSession.getHolder().getPiUsername(), ip, App.sshPort);
                    session.setPassword(CurrentSession.getHolder().getPiPassword());
                    //Avoid asking for key confirmation
                    Properties prop = new Properties();
                    prop.put("StrictHostKeyChecking", "no");
                    session.setConfig(prop);
                    session.setTimeout(3000);
                    session.connect();

                    //Gets MAC Address via linux command
                    Channel channel = session.openChannel("exec");
                    ((ChannelExec)channel).setCommand("cat /sys/class/net/eth0/address");
                    channel.setInputStream(null);
                    ((ChannelExec) channel).setErrStream(System.err);
                    InputStream in = channel.getInputStream();
                    channel.connect();

                    BufferedReader r = new BufferedReader(new InputStreamReader(in));
                    macAddress = r.readLine();
                    channel.disconnect();

                    session.sendKeepAliveMsg();
                    if (!ip.equals(App.defaultIP)){
                        SharedPreferences.Editor edt = sharedPreferences.edit();
                        edt.putString(App.KEY_RECENT, ip);
                        edt.apply();
                    }
                    return session;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return null;
                }

            }

            @Override
            protected void onPostExecute(Session session) {
                if (mCallback!=null){
                    if (session!=null && !session.isConnected()){
                        startControlAct = false;
                    }
                    mCallback.onPostExecute(session, ip, macAddress, startControlAct);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }
}
