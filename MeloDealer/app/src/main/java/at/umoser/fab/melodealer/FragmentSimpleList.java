package at.umoser.fab.melodealer;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class FragmentSimpleList extends ListFragment
        implements AdapterView.OnItemClickListener {


    View rootView;
    FragmentListener mListener;
    Context mContext = CurrentSession.getHolder().getActivity();

    AdapterPlaylistPhone playlistAdapter;
    List<Song> allTracks = new ArrayList<>();
    List<Song> arrayList = new ArrayList<>();
    TreeSet<Integer> selected_items_number = new TreeSet<>();

    private int sectionNumber;


    public static FragmentSimpleList newInstance(int sectionNumber) {
        FragmentSimpleList fragment = new FragmentSimpleList();
        Bundle args = new Bundle();
        args.putInt(App.KEY_SECTION, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentSimpleList() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_phone, container, false);
        Bundle bundle = getArguments();
        sectionNumber = bundle.getInt(App.KEY_SECTION, -1);
        return rootView;
    }

    /**
     * Method used in ActivityPhone to update ListView
     * @param playlistPhone
     */
    public void postData(List<Song> playlistPhone){
        arrayList.clear();
        switch (sectionNumber){
            case App.SECTION_TRACKS:

                //Settings a list that is used in the listadapter
                arrayList.addAll(playlistPhone);

                break;
            case App.SECTION_DOWNLOADED:
                Log.i(App.TAG, " Music Filepath: " + App.EXT_PUBLIC_DIR);


                break;
        }
        //Displaying List in ListView
        populateListView();
    }

    /**
     * Populates ListView
     */
    private void populateListView(){

        if (this.isAdded()){

            if (playlistAdapter==null) {
                playlistAdapter = new AdapterPlaylistPhone(getActivity().getApplicationContext(), getListView().getId(), arrayList);
                setListAdapter(playlistAdapter);
                getListView().setOnItemClickListener(this);
                getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
                getListView().setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                    @Override
                    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                        if (!checked) {
                            selected_items_number.remove(position);
                        } else {
                            selected_items_number.add(position);
                        }
                        mode.setTitle(selected_items_number.size() + " items selected");
                    }

                    @Override
                    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                        MenuInflater inflater = mode.getMenuInflater();
                        inflater.inflate(R.menu.context_menu_smartphone, menu);
                        return true;
                    }

                    @Override
                    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                        //Sets the Send Icon Item disabled  if Smartphone Page was opened from Settings
                        if (CurrentSession.getHolder().isConnected() && CurrentSession.getHolder().getName()!=null){
                            menu.getItem(0).setEnabled(true);
                            menu.getItem(0).getIcon().setAlpha(255);
                        } else {
                            menu.getItem(0).setEnabled(false);
                            menu.getItem(0).getIcon().setAlpha(130);
                        }
                        return true;
                    }

                    @Override
                    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.context_send_music:
                                for (int listViewPosition : selected_items_number) {
                                    Song currSong = (Song) getListView().getItemAtPosition(listViewPosition);
                                    mListener.sendFile(currSong);
                                }
                                if (selected_items_number.size()>1){
                                    Toast.makeText(mContext, mContext.getString(R.string.toast_sending) + selected_items_number.size() +  mContext.getString(R.string.toast_files), Toast.LENGTH_SHORT).show();
                                } else {
                                    Song currSong = (Song) getListView().getItemAtPosition(selected_items_number.first());
                                    Toast.makeText(mContext, mContext.getString(R.string.toast_sending) + currSong.getSongName(), Toast.LENGTH_SHORT).show();
                                }

                                mode.finish();
                                mListener.closeActivity();

                                return true;
                            default:
                                return false;
                        }
                    }

                    @Override
                    public void onDestroyActionMode(ActionMode mode) {
                        getListView().clearChoices();
                        selected_items_number.clear();
                        CurrentSession.getHolder().updatePlaylist();
                    }
                });
                playlistAdapter.notifyDataSetChanged();
            } else {
                playlistAdapter.notifyDataSetChanged();
            }

        }

    }

    /**
     * Item in ListView was clicked
     * @param parent
     * @param view - View of Item Clicked
     * @param position - Position in Adapter used to get ListView Item
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Song songClicked = (Song) parent.getItemAtPosition(position);

        if (CurrentSession.getHolder().isConnected()) {
            //Musik auf Pi schicken
            mListener.sendFile(songClicked);
            Toast.makeText(mContext, mContext.getString(R.string.toast_sending) + songClicked.getSongName(), Toast.LENGTH_SHORT).show();
            mListener.closeActivity();
        } else {
            //Reconnect?
            System.err.println("Connection ist null!!");
        }

    }

    /**
     * Interface used in ActivityPhone
     */
    public interface FragmentListener {
        void closeActivity();
        void sendFile(Song song);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (FragmentListener) context;
        } catch (Exception e) {e.printStackTrace();}
    }

    //TODO Search for Song, select it, remove search query, wrong song is selected
    //possible solution: Store index position on each song along with the flag.


    /**
     * SearchView onQueryTextChange in ActivityPhone
     * @param results
     */
    public void onSearchUpdate(List<Song> results){
        arrayList.clear();
        arrayList.addAll(results);
        populateListView();
    }

}



class AdapterPlaylistPhone extends ArrayAdapter<Song> implements SectionIndexer {


    private HashMap<String, Integer> mapIndex;
    private HashMap<Integer, Integer> positionIndexer;
    private String[] sections;


    AdapterPlaylistPhone(Context context, int resource, List<Song> songs) {
        super(context, resource, songs);
        mapIndex = new LinkedHashMap<String, Integer>();
        positionIndexer = new LinkedHashMap<Integer, Integer>();
        for (int x=0; x<songs.size(); x++){
            String a = songs.get(x).getSongName().substring(0,1).toUpperCase();
            mapIndex.put(a, x);
        }
        Set<String> sectionLetters = mapIndex.keySet();
        //create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);
        sections = new String[sectionList.size()];
        sectionList.toArray(sections);


        List<Integer> values = new ArrayList();
        for (int i : mapIndex.values()) {
            values.add(i);
        }
        values.add(songs.size()-1);
        Collections.sort(values);

        int k = 0;
        int z = 0;
        for(int i = 0; i < values.size()-1; i++) {
            int temp = values.get(i+1);
            do {
                positionIndexer.put(k, z);
                k++;
            } while(k < temp);
            z++;
        }

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PlaceHolder holder;

        if (convertView==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.song_name, parent, false);
            holder = new PlaceHolder();
            holder.currSongTitle = (TextView) convertView.findViewById(R.id.song_title);
            holder.currSongArtist = (TextView) convertView.findViewById(R.id.song_artist);
            holder.currSongTimeLenght = (TextView) convertView.findViewById(R.id.song_time_lenght);
            convertView.setTag(holder);
        } else {
            holder = (PlaceHolder) convertView.getTag();
        }

        Song songClicked = getItem(position);
        String songName = songClicked.getSongName();
        String songArtist = songClicked.getArtist();


        holder.currSongArtist.setVisibility(View.VISIBLE);
        holder.currSongTimeLenght.setVisibility(View.GONE);

        if (songName==null){
            holder.currSongTitle.setText(R.string.empty_playlist);
            holder.currSongArtist.setVisibility(View.INVISIBLE);
        } else {
            holder.currSongTitle.setText(songName);
            if (songArtist!=null && !songArtist.isEmpty()) {
                holder.currSongArtist.setText(songArtist);
            } else {
                holder.currSongArtist.setVisibility(View.INVISIBLE);
            }
            holder.currSongTitle.setTextColor(getContext().getResources().getColor(R.color.songTitle));
            holder.currSongArtist.setTextColor(getContext().getResources().getColor(R.color.songArtist));
        }


        return convertView;

    }
    private class PlaceHolder {
        TextView currSongTitle;
        TextView currSongArtist;
        TextView currSongTimeLenght;
    }


    @Override
    public Object[] getSections() {
        return sections;
    }

    @Override
    public int getPositionForSection(int section) {
        return mapIndex.get(sections[section]);
    }

    @Override
    public int getSectionForPosition(int position) {

        //Due to Android Bug, this acts weird. returning static makes for better solution for now
        //return positionIndexer.get(position);
        return 0;
    }





}
