package at.umoser.fab.melodealer;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class FragmentInfo extends Fragment {


    private View rootView;
    private InterfaceInfo mListener;
    private int STATE;
    private EditText edtInfo1;
    private EditText edtInfo2;
    private Button btnSend;


    public static FragmentInfo newInstance(int STATE) {
        FragmentInfo info = new FragmentInfo();
        Bundle bundle = new Bundle();
        bundle.putInt(App.KEY_STATE, STATE);
        info.setArguments(bundle);
        return info;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_info, container, false);
        Bundle bundle = getArguments();
        this.STATE = bundle.getInt(App.KEY_STATE);
        return rootView;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView txHeader = (TextView) rootView.findViewById(R.id.fraginfo_header);
        final TextInputLayout layInfo1 = (TextInputLayout) rootView.findViewById(R.id.text_layout_info1);
        final TextInputLayout layInfo2 = (TextInputLayout) rootView.findViewById(R.id.text_layout_info2);
        edtInfo1 = (EditText) rootView.findViewById(R.id.edit_text_info1);
        edtInfo2 = (EditText) rootView.findViewById(R.id.edit_text_info2);
        btnSend = (Button) rootView.findViewById(R.id.button_send_login_info);


        switch (STATE){
            case App.STATE_LOGIN:
                txHeader.setText(getString(R.string.i_setlogin));
                layInfo1.setHint(getString(R.string.default_loginname));
                layInfo2.setHint(getString(R.string.h_loginpw));
                break;
            case App.STATE_CONNECT:
                txHeader.setText(getString(R.string.i_wifi));
                layInfo1.setHint(getString(R.string.h_wifi));
                layInfo2.setHint(getString(R.string.h_wifipass));
                break;
        }



        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String info1 = edtInfo1.getText().toString();
                String info2 = edtInfo2.getText().toString();

                switch (STATE){
                    case App.STATE_LOGIN:
                        if (info1.isEmpty()){
                            info1 = (String) layInfo1.getHint();
                        }

                        if (info2.isEmpty()){
                            info2 = (String) layInfo2.getHint();
                        }
                        sendData(info1, info2);
                        break;
                    case App.STATE_CONNECT:
                        if (!info1.isEmpty()){
                            sendData(info1, info2);
                        } else {
                            Toast.makeText(getActivity().getApplicationContext(), "Bitte geben Sie einen WLAN Namen ein", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            }
        });



    }


    private void sendData(String info1, String info2) {
        switch (STATE){
            case App.STATE_LOGIN:
                //Changing Login Information
                mListener.setLogin(info1, info2);
                Toast.makeText(getActivity().getApplicationContext(), "Username: " + info1 +"\nPassword: " + info2, Toast.LENGTH_SHORT).show();

                break;
            case App.STATE_CONNECT:
                //Setting up Device
                String command = "sudo echo '" + info1 + "' > /home/pi/wifiname && " + "sudo echo '" + info2 + "' > /home/pi/password && " + "sudo /home/pi/MoveHotspotConfig";
                CurrentSession.getHolder().exec(App.doNothing,command);
                Toast.makeText(getActivity().getApplicationContext(), "Nun können Sie sich mit diesem Netzwerk verbinden", Toast.LENGTH_SHORT).show();
                break;
        }

        //removing fragment
        Fragment frag = getFragmentManager().findFragmentByTag(App.FRAG_INFO);
        if (frag!=null){
            getFragmentManager().beginTransaction().remove(frag).commit();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InterfaceInfo) {
            mListener = (InterfaceInfo) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface InterfaceInfo {
        void setLogin(String user, String pass);
    }
}
