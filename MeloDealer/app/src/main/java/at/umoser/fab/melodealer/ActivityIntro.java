package at.umoser.fab.melodealer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class ActivityIntro extends AppCompatActivity {


    private ImageView Logo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        Logo = (ImageView) findViewById(R.id.logoImageView);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.logo_animation);
        Logo.setAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Logo.setVisibility(View.GONE);
                Logo.setAnimation(null);
                ActivityIntro.this.finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
}
