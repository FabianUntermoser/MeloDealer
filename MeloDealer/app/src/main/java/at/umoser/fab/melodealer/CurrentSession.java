package at.umoser.fab.melodealer;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.jcraft.jsch.Session;

import java.util.ArrayList;

public class CurrentSession {


    private Session session;
    private String mac, name;
    private ActivityGUI activityControl;
    private Bundle lastOpen;
    private boolean restarting, stopped;
    private String piUsername, piPassword;
    private static boolean flag_wifiService = false, flag_hotspotWifi = false;


    String getMac() {return mac;}
    void setMac(String mac) {this.mac = mac;}

    String getName() {
        return name;
    }
    void setName(String name) {
        this.name = name;
    }

    Session getSession(){return session;}
    void setSession(Session session){this.session=session;}

    ActivityGUI getActivity(){return activityControl;}
    void setActivity(ActivityGUI activityControl){
        this.activityControl=activityControl;
    }

    boolean getRestarting(){return restarting;}
    void setRestarting(boolean restarting){this.restarting=restarting;}

    void setMPDStopped(boolean stopped){this.stopped=stopped;}

    boolean getWifiFlag(){
        return flag_wifiService;
    }
    void setWifiFlag(Boolean flag){
        flag_wifiService = flag;
    }

    boolean getHotspotFlag(){return flag_hotspotWifi;}
    void setHotspotFlag(Boolean flag){flag_hotspotWifi = flag;}

    String getPiPassword() {return piPassword;}
    void setPiPassword(String piPassword) {this.piPassword = piPassword;}

    String getPiUsername() {
        return piUsername;
    }
    void setPiUsername(String piUsername) {
        this.piUsername = piUsername;
    }

    public Bundle getBundle() {return lastOpen;}
    public void setBundle(Bundle lastOpen) {this.lastOpen = lastOpen;}

    private static final CurrentSession holder = new CurrentSession();
    public static CurrentSession getHolder(){return holder;}

    boolean isConnected(){
        if (getSession()!=null && getSession().isConnected()){
            return true;
        } else {
            return false;
        }
    }

    void clear(){
        setSession(null);
        setMac(null);
        setActivity(null);
        setBundle(null);
    }

    void exec(int processValue, String command){
        new SendCommand(processValue, command).execute();
    }



    boolean updateMPD() {
        if (isConnected()){
            new SendCommand(App.doNothing, "mpc update").execute();
            return true;
        } else{
            return false;
        }
    }
    boolean updateServerStats(){
        if (isConnected()){
            new SendCommand(App.getServerStats, "mpc stats").execute();
            return true;
        } else{
            return false;
        }
    }
    boolean updatePlaylist() {
        if (isConnected()){
            new SendCommand(App.getPlaylist, "mpc playlist -f '[%position% %file%\\n]&[%position% %artist%\\n][%position% [%title%]]\\n&[%position% %time%]'").execute();
            return true;
        } else{
            return false;
        }
    }
    boolean updateStatus() {
        if (isConnected()){
            new SendCommand(App.getStatus,"mpc status").execute();
            return true;
        } else{
            return false;
        }
    }

    /**
     * Moves Items to correct positions in the server after sortTrackList sorted the listview
     */
    //TODO: why command looks like this?
    // Move Command: mpc move 50 50
    //mpc move 51 51
    //mpc move 52 52
    //mpc move 53 53
    //mpc move 54 54
    void moveMpdPositions(ArrayList<Song> sortedList){
        if (isConnected()){
            for (int i=1; i<=sortedList.size(); i++){

                //Get Song in sorted list
                Song sortedSong = sortedList.get(i-1);
                int oldMpdPos = sortedSong.getPlaylistPos();
                Log.i(App.TAG, "Checking Song : " + sortedSong.getSongName() + " with pos " + sortedSong.getPlaylistPos());
                //Get Song which has to be switched with song
                Song unsortedSong = null;
                for (int x=1; x<=sortedList.size(); x++) {
                    if (sortedList.get(x-1).getPlaylistPos() == i){
                        unsortedSong = sortedList.get(x-1);

                        Log.i(App.TAG, "Found matching Song : " + unsortedSong.getSongName() + " with pos " + unsortedSong.getPlaylistPos());
                        break;
                    }
                }
                int newMpdPos = unsortedSong.getPlaylistPos();
                //Move Songs at MPD Server
                String command;
                if (newMpdPos>oldMpdPos){
                    command = "mpc move " + newMpdPos + " " + oldMpdPos;
                } else {
                    command = "mpc move " + oldMpdPos + " " + newMpdPos;
                }
                new SendCommand(App.doNothing, command).execute();
                //Switch Positions in object
                Log.i(App.TAG, "Moving Song: " + command);
                int count = 1;
                for (Song song : sortedList) {
                    Log.i(App.TAG, count +": " + song.getSongName() +" at " + song.getPlaylistPos());
                    count++;
                }
                Log.i(App.TAG, "------------" + i + "---------------");
                sortedSong.setPlaylistNo(newMpdPos);
                unsortedSong.setPlaylistNo(oldMpdPos);

            }
        }
    }
//    void moveMpdPositions(ArrayList<Song> list){
//        if (isConnected()){
//            for (int i=1; i<=list.size(); i++){
//                Song song = list.get(i-1);
//                int mpdPos = song.getPlaylistPos();
//                Song song2 = list.get(mpdPos-1);
//                int mpdPos2 = song2.getPlaylistPos();
//                String command = "mpc move " + mpdPos + " " + mpdPos2;
//                Log.i(App.TAG, "Replacing " + song2.getPlaylistPos() + "," + song2.getSongName()  + " with " + song.getPlaylistPos() + "," + song.getSongName());
//                //Log.i(App.TAG, "Moving Song " + song.getSongName() + " | Move Command: " + command);
//                //Log.i(App.TAG, "    Song 1: " +song2.getSongName() + " from " + song2.getPlaylistPos() +" to: " + mpdPos);
//                //Log.i(App.TAG, "    Song 2: " +song.getSongName() + " from " + song.getPlaylistPos() +" to: " + i);
//                for (Song songt: list) {
//                    Log.i(App.TAG, "    " + songt.getPlaylistPos() + ": " + songt.getSongName());
//                }
//                Log.i(App.TAG, "    setting: " + song.getSongName() + " to " + i + " from " + song.getPlaylistPos());
//                song.setPlaylistNo(mpdPos2);
//                Log.i(App.TAG, "    setting: " + song2.getSongName() + " to " + mpdPos + " from " + song2.getPlaylistPos());
//                song2.setPlaylistNo(mpdPos);
//                Log.i(App.TAG, i + "-------------------------------------------");
//                new SendCommand(App.doNothing, command).execute();
//            }
//        }
//    }

}
