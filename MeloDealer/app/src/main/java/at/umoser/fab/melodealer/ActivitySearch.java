package at.umoser.fab.melodealer;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ActivitySearch extends AppCompatActivity
        implements
        FragmentRename.InterfaceRename,
        FragmentSettings.FragmentListener,
        FragmentInfo.InterfaceInfo,
        FragmentConnect.ConnectionCallbacks{


    //TODO Implement USER Feedbak when isConnected is null

    //Setting Values
    private boolean
            settingsShowIntro,
            settingsAutoConnect,
            settingsAutoSearch,
            settingsDebug,
            settingsSearchShowSSH,
            settingsSearchShowMPD;

    private boolean connectAfterOnActivityResult = false;
    private String connectAfterOnActivityResultIP;

    private Toolbar toolbar;
    private String privateIpAddress;
    private int timeoutSearch = 500;

    private SharedPreferences sharedPreferences;

    private NetworkInfo wifiConnection;

    //TODO: ActionProcessButton keeps animation on on orientation change
    //Bug from library, either switch or use own button to fix
    private ActionProcessButton buttonSearch;
    private LinearLayout layoutListEmpty;
    private TextView t_resultsFound;
    private RecyclerView recyclerView;
    private AdapterSearchResults rListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        TextView version = (TextView) findViewById(R.id.t_version);
        version.setText(App.version);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        buttonSearch = (ActionProcessButton) findViewById(R.id.buttonSearch);
        layoutListEmpty = (LinearLayout) findViewById(R.id.layoutListEmpty);
        recyclerView = (RecyclerView) findViewById(R.id.listviewAddresses);
        t_resultsFound = (TextView) findViewById(R.id.t_resultsfound);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        buttonSearch.setMode(ActionProcessButton.Mode.ENDLESS);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        changeStatusBarBackgroundColor();
        checkWifiStatus();

        //restoring state
        if (savedInstanceState!=null){
            //Sets Navigation Button in Top Left Corner
            FragmentSettings fragmentSettings = (FragmentSettings) getSupportFragmentManager().findFragmentByTag(App.FRAG_SETTINGS);
            if (fragmentSettings!=null && fragmentSettings.isAdded() && getSupportActionBar()!=null){
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            ArrayList<String> arrayIP = savedInstanceState.getStringArrayList(App.KEY_IP);
            ArrayList<String> arrayMAC = savedInstanceState.getStringArrayList(App.KEY_MAC);
            ArrayList<Integer> arrayStatus = savedInstanceState.getIntegerArrayList(App.KEY_STATUS);

            ArrayList<SearchResult> dataResultList = new ArrayList<>();
            SearchResult dataResult;
            if (arrayIP!=null && arrayMAC!=null && arrayStatus!=null) {
                for (int i=0; i<arrayIP.size(); i++){
                    String ip = arrayIP.get(i);
                    String mac = arrayMAC.get(i);
                    int status = arrayStatus.get(i);
                    dataResult = new SearchResult(sharedPreferences, ip, mac, status);
                    dataResultList.add(dataResult);
                }
                populateServerList(dataResultList);
            }
        } else {
            //App was startet
            checkSettings();
        }


        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkWifiStatus();

                if (CurrentSession.getHolder().getWifiFlag() && privateIpAddress!=null) {
                    //Start Animation for ProcessButton
                    searchNetwork();
                } else if (checkSettingsDebug()){
                    ArrayList<SearchResult> test = new ArrayList<SearchResult>();
                    SearchResult result1 = new SearchResult(sharedPreferences, "1.1.1.1", "ff:ff:ff:ff", 4);
                    CurrentSession.getHolder().setMac("ff:ff:ff:ff");

                    test.add(result1);
                    populateServerList(test);
                } else if (rListAdapter!=null){
                    //Removes List Items if network cant be searched
                    rListAdapter.removeAll();
                }
            }
        });
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Saves SearchResults for next app start
        if (rListAdapter !=null && !rListAdapter.isEmpty()){
            int size = rListAdapter.getItemCount();

            ArrayList<String> arrayIP = new ArrayList<>();
            ArrayList<String> arrayMAC = new ArrayList<>();
            ArrayList<Integer> arrayStatus = new ArrayList<>();
            for (int i=0; i<size; i++){
                SearchResult item = rListAdapter.getItemAtPosition(i);

                arrayIP.add(i,item.getIp());
                arrayMAC.add(i, item.getMac());
                arrayStatus.add(i, item.getStatus());
            }
            outState.putStringArrayList(App.KEY_IP, arrayIP);
            outState.putStringArrayList(App.KEY_MAC, arrayMAC);
            outState.putIntegerArrayList(App.KEY_STATUS, arrayStatus);
        }

    }

    //RECYCLER CLICK LISTENER
    public void onRecyclerItemClick(View view, int position){
        SearchResult currentResult = rListAdapter.getItemAtPosition(position);

        String currentMac = currentResult.getMac();
        String currentName = currentResult.getName();
        int status = currentResult.getStatus();

        switch (status){
            case App.STATUS_OK:
                if (currentName!=null && !currentName.isEmpty()) {
                    if (CurrentSession.getHolder().getHotspotFlag()){
                        //Starts ConnectFragment for setting up Device
                        setupDevice();

                    } else if (CurrentSession.getHolder().isConnected() && TextUtils.equals(currentMac, CurrentSession.getHolder().getMac())){
                        //if session is already present for the same device, restores Playlist from bundle
                        CurrentSession.getHolder().setMac(currentMac);
                        CurrentSession.getHolder().setName(currentName);
                        startControlAct(currentMac, true);
                    } else {
                        //building new connection for device
                        buildConnection(currentResult.getIp(), true);
                    }
                } else {
                    startNameAct(currentResult.getIp(), currentMac);
                }
                break;

            case App.STATUS_SSH_MPD:
                //If MAC-address couldn't be obtained, probably the login or password was wrong
                changeLogin();
                break;

            default:

                Log.i(App.TAG, "Clicked Name: " + currentResult.getName());
                Log.i(App.TAG, "Clicked MAC: " + currentResult.getMac());
                Log.i(App.TAG, "Clicked IP: " + currentResult.getIp());
                Log.i(App.TAG, "Clicked Status: " + currentResult.getStatus());

                break;
        }
    }

    //ONCREATE CONTEXT MENU FOR RECYCLERVIEW
    public void onRecyclerCreateContext (ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo, final int position) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_servers, menu);
        //Hides certain context menu items if a name has already been set to a device or when you offline and conected to the hotspot.

        final SearchResult result = rListAdapter.getItemAtPosition(position);
        int currStatus = result.getStatus();

        switch (currStatus){
            case App.STATUS_SSH:
                //SSH Server only
                menu.setGroupVisible(R.id.context_group_musicserver, false);
                menu.setGroupVisible(R.id.context_group_online, false);
                menu.setGroupVisible(R.id.context_group_offline, false);
                menu.setGroupVisible(R.id.context_group_change_login, false);
                break;
            case App.STATUS_MPD:
                //MPD Server only
                menu.setGroupVisible(R.id.context_group_musicserver, false);
                menu.setGroupVisible(R.id.context_group_online, false);
                menu.setGroupVisible(R.id.context_group_offline, false);
                menu.setGroupVisible(R.id.context_group_change_login, false);
                break;
            case App.STATUS_SSH_MPD:
                //SSH & MPD But couldn't connect
                menu.setGroupVisible(R.id.context_group_musicserver, false);
                menu.setGroupVisible(R.id.context_group_online, false);
                menu.setGroupVisible(R.id.context_group_offline, false);
                menu.setGroupVisible(R.id.context_group_change_login, true);
                break;
            case App.STATUS_OK:
                //Connectable Music Server
                menu.setGroupVisible(R.id.context_group_change_login, false);

                if (result.getName()!=null && result.getName().isEmpty()){
                    menu.setGroupVisible(R.id.context_group_musicserver, false);
                } else {
                    menu.setGroupVisible(R.id.context_group_musicserver, true);
                }

                if (CurrentSession.getHolder().getHotspotFlag()){
                    menu.setGroupVisible(R.id.context_group_online, false);
                    menu.setGroupVisible(R.id.context_group_offline, true);
                } else{
                    menu.setGroupVisible(R.id.context_group_online, true);
                    menu.setGroupVisible(R.id.context_group_offline, false);
                }
                break;
        }

        //MENU: RENAME DEVICE
        menu.findItem(R.id.context_rename).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                reNameDevice(position);
                return true;
            }
        });

        //MENU: REMOVE NAME
        menu.findItem(R.id.context_removeName).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                removeName(position);
                return true;
            }
        });

        //MENU: OPEN WEBSITE IN BROWSER
        menu.findItem(R.id.context_website).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                openWebsite(position);
                return true;
            }
        });

        //MENU: RESET DEVICE
        menu.findItem(R.id.context_reset).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                resetDevice(position);
                return true;
            }
        });

        //MENU: CONTROL OFFLINE
        menu.findItem(R.id.context_control_offline).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                buildConnection(result.getIp(), true);
                return true;
            }
        });

        //MENU: Change Login Information
        menu.findItem(R.id.context_change_login).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                changeLogin();
                return true;
            }
        });
    }

    private boolean checkSettingsDebug(){
        settingsDebug = sharedPreferences.getBoolean(App.PREF_DEBUG, getResources().getBoolean(R.bool.default_debug));
        return settingsDebug;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //Handles the displaying of the options menu in the settingsfragment
        FragmentManager fm = getSupportFragmentManager();
        FragmentSettings fragmentSettings = (FragmentSettings) fm.findFragmentByTag(App.FRAG_SETTINGS);
        if (fragmentSettings!=null && fragmentSettings.isAdded()){
            for (int x=0;x<menu.size();x++){
                menu.getItem(x).setEnabled(false);
                menu.getItem(x).setVisible(false);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        ArrayList<SearchResult> list = new ArrayList<>();

        switch (item.getItemId()){
            case R.id.action_settings:

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentContainer, new FragmentSettings(), App.FRAG_SETTINGS)
                        .addToBackStack(null)
                        .commit();

                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void changeLogin(){
        //Changing SSH Login Info
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        FragmentInfo fragInfo = (FragmentInfo) fm.findFragmentByTag(App.FRAG_INFO);
        if (fragInfo != null){
            ft.remove(fragInfo);
        } else {
            fragInfo = FragmentInfo.newInstance(App.STATE_LOGIN);
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.add(R.id.fragmentContainer, fragInfo, App.FRAG_INFO);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void checkSettings() {

        //Check Settings for Intro
        settingsShowIntro = sharedPreferences.getBoolean(App.PREF_INTRO, getResources().getBoolean(R.bool.default_intro));
        settingsAutoConnect = sharedPreferences.getBoolean(App.PREF_AUTOCONNECT, getResources().getBoolean(R.bool.default_autoconnect));
        settingsAutoSearch = sharedPreferences.getBoolean(App.PREF_AUTOSEARCH, getResources().getBoolean(R.bool.default_autosearch));
        checkSettingsDebug();

        if (settingsShowIntro) {
            Intent introAct = new Intent(this, ActivityIntro.class);
            startActivityForResult(introAct, 1);
        } else {
            if (CurrentSession.getHolder().getWifiFlag()){
                autoConnect();
            }
        }

        //If Autosearch is auto called if Autoconnect is enabled
        if (!settingsAutoConnect && settingsAutoSearch && CurrentSession.getHolder().getWifiFlag() && privateIpAddress!=null) {
            searchNetwork();
        }

        if (checkSettingsDebug()){
            Toast.makeText(getApplicationContext(), "Debugging is on", Toast.LENGTH_SHORT).show();
        }
        String piUsername = sharedPreferences.getString(App.PREF_LOGINNAME, null);
        String piPassword = sharedPreferences.getString(App.PREF_LOGINPASS, null);
        CurrentSession.getHolder().setPiUsername(piUsername);
        CurrentSession.getHolder().setPiPassword(piPassword);
    }

    //Automatically connects to last ip that was connected.
    private void autoConnect(){
        if (settingsAutoConnect && !CurrentSession.getHolder().getHotspotFlag()) {
            String recentIP = sharedPreferences.getString(App.KEY_RECENT, null);
            if (recentIP!=null && !recentIP.isEmpty()) {
                if (CurrentSession.getHolder().getWifiFlag() && privateIpAddress!=null) {
                    searchNetwork();
                }
                buildConnection(recentIP, true);
            }
        }
    }

    private void changeStatusBarBackgroundColor(){
        //Toolbar setup
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        //Changes status bar background color on lollipo devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

    }


    private void checkWifiStatus(){
        //Checks if the phone is connected to the melodealer hotspot
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiConnection = connManager.getActiveNetworkInfo();
        if (wifiConnection!=null && wifiConnection.isConnected()){
            //connected to some network
            WifiManager wifiMgr = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();

            //Endianness converting
            if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                ip = Integer.reverseBytes(ip);
            }
            byte[] ipByteArray = BigInteger.valueOf(ip).toByteArray();
            try {
                privateIpAddress = InetAddress.getByAddress(ipByteArray).getHostAddress();
            } catch (UnknownHostException ex) {
                Log.e(App.TAG, "Could not find private IP-address.");
                privateIpAddress = null;
            }

            String wifi_SSID = wifiInfo.getSSID();
            wifi_SSID = wifi_SSID.substring(1, wifi_SSID.length()-1);

            if (wifiConnection.getType()==1){
                CurrentSession.getHolder().setWifiFlag(true);
                //using wifi
                if (wifi_SSID.equals(App.hotspotSSID)){
                    //Einrichtung
                    CurrentSession.getHolder().setHotspotFlag(true);
                } else {
                    CurrentSession.getHolder().setHotspotFlag(false);
                }

            } else if (wifiConnection.getType()==0 && wifi_SSID.equals(App.hotspotSSID)){
                //connected to the right wifi signal, but since there's no internet connnection, phone uses mobile data
                CurrentSession.getHolder().setWifiFlag(false);
                CurrentSession.getHolder().setHotspotFlag(true);

                if (rListAdapter !=null){
                    rListAdapter.removeAll();
                }
                new AlertDialog.Builder(this).setTitle("WLAN Verbindung").setMessage("Bitte deaktivieren Sie ihre Mobilfunkverbindung.").setIcon(R.drawable.ic_signal_cellular_off_black_24px).setNeutralButton("OK!", null).show();

            } else {
                //using mobile data
                CurrentSession.getHolder().setWifiFlag(false);
                CurrentSession.getHolder().setHotspotFlag(false);

                if (rListAdapter !=null){
                    rListAdapter.removeAll();
                }
                new AlertDialog.Builder(this).setTitle("WLAN Verbindung").setMessage("Bitte aktivieren Sie ihre WLAN Funktion.").setIcon(R.drawable.ic_network_wifi_black_24px).setNeutralButton("OK!", null).show();

            }

        } else {
            //no network available
            CurrentSession.getHolder().setWifiFlag(false);
            CurrentSession.getHolder().setHotspotFlag(false);
            new AlertDialog.Builder(this).setTitle("Netzwerkverbindung").setMessage("Es besteht keine Netzwerkverbindung").setIcon(R.drawable.ic_signal_cellular_off_black_24px).setNeutralButton("OK!", null).show();

        }
        setTitle();
    }

    public void setTitle() {
        if (getSupportActionBar() != null) {
            if (CurrentSession.getHolder().getWifiFlag() && !CurrentSession.getHolder().getHotspotFlag()) {
                getSupportActionBar().setTitle("Online");
                this.setTitle("online");

            } else {
                getSupportActionBar().setTitle("Offline");
                this.setTitle("offline");
            }
        }

    }

    public void searchNetwork() {
        final ExecutorService es = Executors.newCachedThreadPool();
        final ArrayList<Future<SearchResult>> futureAddresses = new ArrayList<>();

        settingsSearchShowSSH = sharedPreferences.getBoolean(App.PREF_FILTER_SSH, getResources().getBoolean(R.bool.default_showSSH));
        settingsSearchShowMPD = sharedPreferences.getBoolean(App.PREF_FILTER_MPD, getResources().getBoolean(R.bool.default_showMPD));

        //Set Animation for Search Button
        buttonSearch.setProgress(1);

        new AsyncTask<Integer, Integer, ArrayList<SearchResult>>() {
            @Override
            protected ArrayList<SearchResult> doInBackground(Integer... integers) {

                String ip = privateIpAddress;
                Log.i(App.TAG, "This is the private IP Address: " + ip);

                for (int x=1;x<=254;x++) {
                    ip = ip.substring(0, ip.lastIndexOf(".")+1);
                    ip = ip + String.valueOf(x);
                    //filter possible addresses for open port (6600)
                    futureAddresses.add(portIsOpen(es, ip));
                }

                ArrayList<SearchResult> addresses = new ArrayList<>();

                for (Future<SearchResult> devices : futureAddresses) {
                    try {
                        if (devices.get().getStatus()!=0) {
                            addresses.add(devices.get());
                        }
                    } catch (Exception ignored) {
                    }
                }


                if (!addresses.isEmpty()){
                    for (SearchResult result: addresses) {
                        int status = result.getStatus();
                        if (status==App.STATUS_SSH && !settingsSearchShowSSH){
                            result.setHide(true);
                        } else if (status==App.STATUS_SSH && settingsSearchShowSSH) {
                            result.setHide(false);
                        }
                        if (status==App.STATUS_MPD && !settingsSearchShowMPD){
                            result.setHide(true);
                        } else if (status==App.STATUS_MPD && settingsSearchShowMPD) {
                            result.setHide(false);
                        }
                    }
                }

                return addresses;
            }

            @Override
            protected void onPostExecute(ArrayList<SearchResult> addr) {
                populateServerList(addr);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    public Future<SearchResult> portIsOpen(final ExecutorService es, final String ip) {
        return es.submit(new Callable<SearchResult>() {
            @Override public SearchResult call() {
                return checkPortForOne(ip);
            }
        });
    }

    private SearchResult checkPortForOne(String ip){
        int status = 0;
        // 0 = nothing
        // 1 = open SSH Port (22)
        // 2 = open MPD Port (6600)
        // 3 = open SSH & MPD Port
        // 4 = could get MAC Address from SSH Connection

        String macAddress = null;
        Socket socket;
        JSch jSch;
        Session session;
        Channel channel;
        BufferedReader r;

        try {
            //Checks open MPD Port
            socket = new Socket();
            socket.connect(new InetSocketAddress(ip, App.mpdPort), timeoutSearch);
            socket.close();

            //Port MPD is open
            status = 2;

        } catch (Exception ignored) {}

        try {
            //Checks open SSH Port
            socket = new Socket();
            socket.connect(new InetSocketAddress(ip, App.sshPort), timeoutSearch);
            socket.close();

            if (status==2){
                //Port SSH AND MPD is open
                status = 3;
            } else {
                //MPD is not open, but SSH is
                status = 1;
            }

            jSch = new JSch();
            session = jSch.getSession(CurrentSession.getHolder().getPiUsername(), ip, App.sshPort);
            session.setPassword(CurrentSession.getHolder().getPiPassword());
            //Avoid asking for key confirmation
            Properties prop = new Properties();
            prop.put("StrictHostKeyChecking", "no");
            session.setConfig(prop);
            session.connect();

            channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand("cat /sys/class/net/eth0/address");
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);
            InputStream in = channel.getInputStream();
            channel.connect();

            r = new BufferedReader(new InputStreamReader(in));
            macAddress = r.readLine();
            //Mac address obtained
            status = 4;

            channel.disconnect();
            session.disconnect();

            //Here a proper Raspberry Pi has been found. both Ports (SSH & MPD) are open,
            //and the login information was correct, hence the macAddress could be read
            return new SearchResult(sharedPreferences, ip, macAddress, status);

        } catch (Exception ex) {
            //Upon failing to get the mac Address, it checks if a mac Address has been set to a Searchresult before.
            //This makes sure that the user can change login information but keep the info from before changing it,
            //since the MAC Address can only be obtained with correct Login Information.
            if (status>=1 && rListAdapter.getItemCount()!=0){
                for (SearchResult result : rListAdapter.getAll()) {
                    if (ip.equals(result.getIp())){
                        macAddress = result.getMac();
                    }
                }
            }
            //Here the found Device either doesn't have one or both of the two Ports (SSH & MPD) open,
            //or the login information was incorrect.
            return new SearchResult(sharedPreferences, ip, macAddress, status);
        }
    }

    private void populateServerList(ArrayList<SearchResult> addresses) {
        if (recyclerView.getAdapter()!=null){
            rListAdapter.replace(addresses);
        } else {
            rListAdapter = new AdapterSearchResults(addresses, ActivitySearch.this);
            recyclerView.setAdapter(rListAdapter);
            registerForContextMenu(recyclerView);
            rListAdapter.update();
        }
        //Ends the Animation for the ProcessButton
        buttonSearch.setProgress(0);
    }

    private void buildConnection(final String hostname, final boolean startControlAct) {
        Log.i(App.TAG,"Building Connection to " + hostname);
        //Adds FragmentConnect
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, FragmentConnect.newInstance(hostname, startControlAct), App.FRAG_CONNECT);
        fragmentTransaction.commit();
    }

    //Interface: Connection Callback from FragmentConnect
    @Override
    public void onPostExecute(Session session, String ip, String macAddress, boolean startControlAct) {
        //Removing FragmentConnect after Connection to Pi was established
        FragmentConnect fragmentConnect = (FragmentConnect) getSupportFragmentManager().findFragmentByTag(App.FRAG_CONNECT);
        if (fragmentConnect!=null){
            getSupportFragmentManager().beginTransaction().remove(fragmentConnect).commit();
        }

        if (session == null && checkSettingsDebug() && startControlAct) {
            startControlAct("FF:FF:FF:FF:FF", false);
        }

        if (session!=null){
            CurrentSession.getHolder().setSession(session);
            CurrentSession.getHolder().setMac(macAddress);
            //needed to update Selection Drawable for SearchResult clicked
            if (rListAdapter!=null){
                rListAdapter.notifyDataSetChanged();
            }
            if (startControlAct){
                //starts control activity
                startControlAct(macAddress, false);
            } else if (App.defaultIP.equals(session.getHost())){
                rListAdapter.removeAll();
            } else {
                //resets device
                String command = "sudo /home/pi/RevertToHotspot";
                CurrentSession.getHolder().exec(App.doNothing, command);
            }
        } else {
            Toast.makeText(ActivitySearch.this, R.string.connection_error, Toast.LENGTH_SHORT).show();
            String recentIP = sharedPreferences.getString(App.KEY_RECENT, null);

            if (TextUtils.equals(recentIP, ip)) {
                SharedPreferences.Editor edt = sharedPreferences.edit();
                edt.putBoolean(App.PREF_AUTOCONNECT, false);
                edt.apply();
            }
        }
    }

    private void startNameAct(String ip, String mac){
        Intent nameAct = new Intent(this, ActivityName.class);
        nameAct.putExtra(App.KEY_IP, ip);
        nameAct.putExtra(App.KEY_MAC, mac);
        startActivityForResult(nameAct, 2);
        overridePendingTransition( android.R.anim.fade_in, android.R.anim.fade_out );
    }

    private void reNameDevice(int position) {
        FragmentManager fm = this.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        FragmentRename fragmentRename = new FragmentRename();

        fm.popBackStack();
        fm.executePendingTransactions();
        ft.add(R.id.fragmentContainer, fragmentRename, App.FRAG_RENAME);
        ft.addToBackStack(null);
        fragmentRename.getPosition(position);
        ft.commit();
    }


    private void removeName(int position) {
        SearchResult currResult = rListAdapter.getItemAtPosition(position);
        currResult.setName(sharedPreferences, "");
        rListAdapter.notifyDataSetChanged();
    }

    private void openWebsite(int position){
        String host;
        if (CurrentSession.getHolder().getHotspotFlag()){
            host = "http://" + App.defaultIP;
        } else {
            SearchResult currResult = rListAdapter.getItemAtPosition(position);
            host = "http://" + currResult.getIp();
        }
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(host));
        startActivity(i);
    }

    /**
     * Removes Wifi Info from Raspberry Pi and reverts it back into Hotspot Mode
     * @param position - used to identify which Speaker was selected in list.
     */
    private void resetDevice(int position) {
        SearchResult currResult = rListAdapter.getItemAtPosition(position);
        //Check if session already in memory
        if (CurrentSession.getHolder().isConnected() && TextUtils.equals(CurrentSession.getHolder().getSession().getHost(), currResult.getIp())){
            String command = "sudo /home/pi/RevertToHotspot";
            //RESET NOW
            CurrentSession.getHolder().exec(App.doNothing, command);
        } else {
            //No Session or different Session
            buildConnection(currResult.getIp(), false);
        }
        rListAdapter.remove(position);
    }

    /**
     * Sends Home Wifi Login Data to Raspberry Pi for it to connect to
     */
    private void setupDevice(){
        buildConnection(App.defaultIP, false);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        FragmentInfo fragmentConnect = (FragmentInfo) fm.findFragmentByTag(App.FRAG_INFO);
        if (fragmentConnect != null && fragmentConnect.isAdded()){
            ft.remove(fragmentConnect);
        } else {
            fragmentConnect = FragmentInfo.newInstance(App.STATE_CONNECT);
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.add(R.id.fragmentContainer, fragmentConnect, App.FRAG_INFO);
        ft.addToBackStack(null);
        ft.commit();

    }


    private void startControlAct(String mac, Boolean restore) {
        //Starts ActivityGui
        Intent guiAct = new Intent(this, ActivityGUI.class);
        Bundle bundle = CurrentSession.getHolder().getBundle();
        if (bundle != null && restore){
            bundle.putString(App.KEY_MAC, mac);
            guiAct.putExtras(bundle);
        } else {
            guiAct.putExtra(App.KEY_MAC, mac);
        }
        overridePendingTransition( android.R.anim.fade_in, android.R.anim.fade_out );
        startActivity(guiAct);
    }





    @Override
    public void renameInterface(String name, int position) {
        SearchResult currResult = rListAdapter.getItemAtPosition(position);
        currResult.setName(sharedPreferences, name);
        rListAdapter.notifyDataSetChanged();

        FragmentManager fm = this.getSupportFragmentManager();
        fm.popBackStack();
        fm.executePendingTransactions();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case 1:
                //Used when Settings "show intro" and "autoconnect" are both set to true
                //After intro is shown, autoConnect() is called
                if (!settingsAutoConnect && settingsAutoSearch && CurrentSession.getHolder().getWifiFlag() && privateIpAddress!=null) {
                    searchNetwork();
                } else {
                    autoConnect();
                }
                break;
            case 2:
                //Result from Name Activity
                if (resultCode == RESULT_OK) {
                    String resultName = data.getStringExtra(App.KEY_NAME);
                    String resultMac = data.getStringExtra(App.KEY_MAC);
                    String ip = data.getStringExtra(App.KEY_IP);
                    //sets new name for device named
                    if (resultName!=null && !resultName.isEmpty()){
                        for (int x = 0; x< rListAdapter.getItemCount(); x++) {
                            SearchResult currResult = rListAdapter.getItemAtPosition(x);
                            String mac = currResult.getMac();
                            if (mac != null && mac.equals(resultMac)){
                                currResult.setName(sharedPreferences, resultName);
                                rListAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    //What to do after naming Device in offline mode
                    if (!CurrentSession.getHolder().getHotspotFlag()){
                        connectAfterOnActivityResult = true;
                        connectAfterOnActivityResultIP = ip;
                    }
                }
                break;
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        //BugFix - Activity is not added when onActivityResult gets called.
        //So before adding a transaction, you have to wait until the activity is fully added.
        if (connectAfterOnActivityResult){
            buildConnection(connectAfterOnActivityResultIP, true);
            connectAfterOnActivityResult = false;
            connectAfterOnActivityResultIP = null;
        }
    }

    @Override
    public void closeSettings(boolean checkStatus, boolean checkHide, FragmentManager.OnBackStackChangedListener listener) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportFragmentManager().removeOnBackStackChangedListener(listener);

        FragmentSettings fragmentSettings = (FragmentSettings) getSupportFragmentManager().findFragmentByTag(App.FRAG_SETTINGS);
        if (fragmentSettings!=null){
            getSupportFragmentManager().beginTransaction().remove(fragmentSettings).commit();
        }

        if (rListAdapter!=null){
            rListAdapter.update();
        }

        //Displays or Hides Searchresults based on Settings
        if (checkHide){
            //Hides Search Items from Searchlist
            settingsSearchShowSSH = sharedPreferences.getBoolean(App.PREF_FILTER_SSH, getResources().getBoolean(R.bool.default_showSSH));
            settingsSearchShowMPD = sharedPreferences.getBoolean(App.PREF_FILTER_MPD, getResources().getBoolean(R.bool.default_showMPD));
            if (rListAdapter !=null){
                for (SearchResult result: rListAdapter.getAll()) {
                    int status = result.getStatus();
                    if (status==App.STATUS_SSH && !settingsSearchShowSSH){
                        result.setHide(true);
                    } else if (status==App.STATUS_SSH && settingsSearchShowSSH) {
                        result.setHide(false);
                    }
                    if (status==App.STATUS_MPD && !settingsSearchShowMPD){
                        result.setHide(true);
                    } else if (status==App.STATUS_MPD && settingsSearchShowMPD) {
                        result.setHide(false);
                    }
                }
                rListAdapter.notifyDataSetChanged();
            }
        }

        if (checkStatus && rListAdapter !=null && !rListAdapter.isEmpty()){
            checkStatus();
        }
    }

    private void checkStatus(){
        final ExecutorService es = Executors.newCachedThreadPool();
        final List<Future<SearchResult>> futureAddresses = new ArrayList<>();

        new AsyncTask<Integer, Integer, List<Future<SearchResult>>>() {
            @Override
            protected List<Future<SearchResult>> doInBackground(Integer... integers) {
                for (SearchResult result:rListAdapter.getAll()) {
                    futureAddresses.add(portIsOpen(es,result.getIp()));
                }
                return futureAddresses;
            }

            @Override
            protected void onPostExecute(List<Future<SearchResult>> futureAddresses) {
                ArrayList<SearchResult> newList = new ArrayList<>();
                for (Future<SearchResult> fresult : futureAddresses) {
                    try {
                        if (fresult.get().getStatus()!=0){
                            newList.add(0, fresult.get());
                        }
                    } catch (Exception ignored) {
                    }
                }
                rListAdapter.replace(newList);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Interface from FragmentInfo - changes login info
     * @param user
     * @param pass
     */
    @Override
    public void setLogin(String user, String pass) {
        //Removing Soft Keyboard
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        //SSH Connection Login Info has been changed
        SharedPreferences.Editor edt = sharedPreferences.edit();
        edt.putString(App.PREF_LOGINNAME, user);
        edt.putString(App.PREF_LOGINPASS, pass);
        edt.apply();
        CurrentSession.getHolder().setPiUsername(user);
        CurrentSession.getHolder().setPiPassword(pass);
        CurrentSession.getHolder().clear();
        searchNetwork();
    }



    void showEmptyListLayout(){
        //Shows text if list is empty
        if (rListAdapter.getItemCount()==0){
            layoutListEmpty.setVisibility(View.VISIBLE);
            t_resultsFound.setVisibility(View.GONE);
        } else {
            layoutListEmpty.setVisibility(View.GONE);
            t_resultsFound.setVisibility(View.VISIBLE);
        }
    }

    //Needed to set aniamtion on TextView on Resultlist size change
    TextView getResultText (){
        return t_resultsFound;
    }

    void updateResultText(){
        t_resultsFound.setText(getString(R.string.t_resultsfound, rListAdapter.getItemCount()));

    }

}
