package at.umoser.fab.melodealer;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityName extends AppCompatActivity {


    private String ip;
    private String mac;

    private TextInputLayout layName;
    private TextView musikbox_ip;
    private TextView musikbox_mac;
    private EditText editTextName;
    private Button buttonName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_pi);

        Bundle extras = getIntent().getExtras();
        if (extras!=null) {
            ip = extras.getString(App.KEY_IP);
            mac = extras.getString(App.KEY_MAC);
        }

        layName = (TextInputLayout) findViewById(R.id.text_layout_name);
        musikbox_ip = (TextView) findViewById(R.id.musikbox_ip);
        musikbox_mac = (TextView) findViewById(R.id.musikbox_mac);
        editTextName = (EditText) findViewById(R.id.editTextName);
        buttonName = (Button) findViewById(R.id.buttonName);


        if (ip!=null && mac!=null) {
            musikbox_ip.setText(ip);
            musikbox_mac.setText(mac);
        }

        buttonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = editTextName.getText().toString();
                if (!name.isEmpty()) {
                    //Post Result and name Device
                    Intent resultData = new Intent();
                    resultData.putExtra(App.KEY_NAME, name);
                    resultData.putExtra(App.KEY_MAC, mac);
                    resultData.putExtra(App.KEY_IP, ip);
                    setResult(RESULT_OK, resultData);
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    finish();


                } else {
                    //display message to tell user to put in name
                    layName.setError(getString(R.string.empty_name));
                }
            }
        });

    }



}
