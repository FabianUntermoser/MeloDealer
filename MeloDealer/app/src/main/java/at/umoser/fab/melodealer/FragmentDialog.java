package at.umoser.fab.melodealer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FragmentDialog extends DialogFragment{


    FragmentDialogListener mListener;

    //Views for first Dialog
    View firstDia_rootView;
    LinearLayout firstDia_lay_currDir;
    TextView firstDia_t_currDir;
    ListView firstDia_selectedList;

    //Views for change Directory Dialog
    View secondDia_rootView;
    TextView secondDia_t_currDir;
    Button secondDia_btn_up;
    ListView secondDia_l_Folders;
    ListView secondDia_l_FoldersQ;

    int STATE = 0;

    private TextView t_selected;
    private String cancel;
    private String option1;
    private String option2;
    private String title;
    private int icon_id;

    private ArrayList<Song> songList;
    private boolean allRadio;
    private String lastPath;

    private File root;
    private File curFolder;
    private List<File> fileList = new ArrayList<>();
    private List<String> fileListQ = new ArrayList<>();

    private SharedPreferences sharedPreferences;

    private AlertDialog mainDialog;
    private AlertDialog changeDirDialog;


    public interface FragmentDialogListener{
        void onDialogOption1(int STATE);
        void onDialogOption2(int STATE);
    }

    public static FragmentDialog newInstance(int STATE, ArrayList<Song> LIST, boolean allRadio) {
        FragmentDialog dialog = new FragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(App.KEY_STATE, STATE);
        bundle.putBoolean(App.KEY_RADIO, allRadio);
        bundle.putParcelableArrayList(App.KEY_SONGS, LIST);
        dialog.setArguments(bundle);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        root = new File(Environment.getRootDirectory().getAbsolutePath());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //Getting Bundle State --> The STATE decides which Dialog is used.
        Bundle bundle = getArguments();
        this.STATE = bundle.getInt(App.KEY_STATE);
        allRadio = bundle.getBoolean(App.KEY_RADIO);
        songList = new ArrayList<>();
        songList = bundle.getParcelableArrayList(App.KEY_SONGS);
        cancel = getString(R.string.dia_cancel);

        //Component needed by First Dialog
        AlertDialog.Builder firstBuilder = new AlertDialog.Builder(getActivity());
        //FragmentDialog is used by multiple Actions
        switch (STATE){
            case App.STATE_DOWNLOAD:
                prepareDialogDownload();
                return stateDownload(firstBuilder);
            case App.STATE_REMOVE:
                return stateRemove(firstBuilder);
            default: return firstBuilder.create();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



    private void prepareDialogDownload(){
        //Adds last used Path to Quick Select
        lastPath = sharedPreferences.getString(App.KEY_DIRPATH, null);
        if (lastPath == null){
            if (curFolder!=null){
                lastPath = curFolder.getAbsolutePath();
            } else {
                curFolder = root;
                lastPath = root.getAbsolutePath();
            }
        } else {
            curFolder = new File(lastPath);
        }

        //Adds Default Music Folder to Quick Select
        File file = App.EXT_PUBLIC_DIR;
        if (file!=null){
            fileListQ.add(file.getAbsolutePath());
            if (!lastPath.equals(file.getAbsolutePath())){
                fileListQ.add(lastPath);
            }
        }
    }

    private Dialog stateDownload(AlertDialog.Builder firstBuilder){
        title = getString(R.string.dia_t_download);
        option1 = getString(R.string.dia_download);
        option2 = getString(R.string.dia_changedir);
        icon_id = R.drawable.ic_file_download_black_24px;

        //Second Dialog - Allows User to select Directory where Songs are sent to.
        final AlertDialog.Builder bChangeDir = new AlertDialog.Builder(getActivity(), android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);

        setupChangeDirDialog(bChangeDir);

        setupFirstDialog(firstBuilder);
        firstDia_lay_currDir.setVisibility(View.VISIBLE);

        listDir(curFolder);

        //Creates Adapter for the Quick Select List Item
        ArrayAdapter<String> directoryListQ = new ArrayAdapter<String>(mainDialog.getContext(), R.layout.list_directories, R.id.t_dir, fileListQ);
        secondDia_l_FoldersQ.setAdapter(directoryListQ);

        return mainDialog;
    }

    private Dialog stateRemove(AlertDialog.Builder firstBuilder){
        title = getString(R.string.dia_t_remove);
        option1 = getString(R.string.dia_removeTrack);
        option2 = getString(R.string.dia_deleteTrack);
        icon_id = R.drawable.ic_delete_black_24px;

        setupFirstDialog(firstBuilder);
        firstDia_lay_currDir.setVisibility(View.GONE);

        return mainDialog;
    }


    @Override
    public void onStart() {
        super.onStart();

        switch (STATE){
            case App.STATE_DOWNLOAD:
                //Disable Download function if selected Folder is not writeable
                File parentFolder = new File(lastPath);
                if (parentFolder.canRead() && parentFolder.canWrite()){
                    mainDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setEnabled(true);
                } else {
                    mainDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setEnabled(false);
                }
                break;
            case App.STATE_REMOVE:
                //Disable Delete function if only Radio was selected
                if (allRadio){
                    mainDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                } else {
                    mainDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
                }
                break;
        }

    }

    private void setupFirstDialog(final AlertDialog.Builder firstBuilder){
        //First Dialog
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        firstDia_rootView = layoutInflater.inflate(R.layout.dialog_main, null);
        firstBuilder.setView(firstDia_rootView);
        firstBuilder.setIcon(icon_id)
                .setTitle(title)
                .setNeutralButton(cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Dont do anything
                        mainDialog.cancel();
                    }
                })
                .setPositiveButton(option2, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //User Selected Option 2: change Directory
                        if (STATE==App.STATE_DOWNLOAD){
                            changeDirDialog.show();
                        } else if (STATE==App.STATE_REMOVE){
                            mListener.onDialogOption2(STATE);
                        }
                        dismiss();

                    }
                })
                .setNegativeButton(option1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //User Selected Option 1: Download
                        mListener.onDialogOption1(STATE);
                    }
                });



        mainDialog = firstBuilder.create();

        firstDia_lay_currDir = (LinearLayout) firstDia_rootView.findViewById(R.id.layout_curDir);
        firstDia_t_currDir = (TextView) firstDia_rootView.findViewById(R.id.t_currDir);
        t_selected = (TextView) firstDia_rootView.findViewById(R.id.t_selected);
        firstDia_selectedList = (ListView) firstDia_rootView.findViewById(R.id.l_tracksSelect);
        firstDia_t_currDir.setText(lastPath);

        //If only one item is selected, songname is just set as text
        if (songList.size()>1){
            t_selected.setVisibility(View.GONE);
            AdapterDialog<Song> adapterDialog = new AdapterDialog<Song>(mainDialog.getContext(), songList);
            firstDia_selectedList.setAdapter(adapterDialog);
        } else {
            t_selected.setVisibility(View.VISIBLE);
            t_selected.setText(songList.get(0).getSongName());
        }
    }

    private void setupChangeDirDialog(AlertDialog.Builder bChangeDir){
        //second Dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        secondDia_rootView = inflater.inflate(R.layout.dialog_choose_directory, null);
        bChangeDir.setView(secondDia_rootView);
        bChangeDir.setTitle(R.string.dia_changedir)
                .setCancelable(true)
                .setNeutralButton(cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Cancel Button clicked
                        mainDialog.show();
                    }
                })
                .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //User Selected Option to change Directory
                        //Saving path for future quick select
                        SharedPreferences.Editor edt = sharedPreferences.edit();
                        edt.putString(App.KEY_DIRPATH, curFolder.getAbsolutePath());
                        edt.apply();
                        //Cant download to Read/Write Protected Folder
                        if (curFolder.canRead() && curFolder.canWrite()){
                            mainDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setEnabled(true);
                        } else {
                            mainDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setEnabled(false);
                        }
                        mainDialog.show();
                    }
                });
        //Need AlertDialog to change Layout
        changeDirDialog = bChangeDir.create();

        secondDia_t_currDir = (TextView) secondDia_rootView.findViewById(R.id.t_currDir);
        secondDia_btn_up = (Button) secondDia_rootView.findViewById(R.id.btn_parentdir);
        secondDia_l_Folders = (ListView) secondDia_rootView.findViewById(R.id.l_folders);
        secondDia_l_FoldersQ = (ListView) secondDia_rootView.findViewById(R.id.l_foldersQuickSelect);
        secondDia_t_currDir.setText(lastPath);

        //Go to Parent Directory Button
        secondDia_btn_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (curFolder.getParentFile()!=null){
                    listDir(curFolder.getParentFile());
                    secondDia_l_FoldersQ.clearChoices();
                }

            }
        });

        //ListView that displays child Directories
        secondDia_l_Folders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                File selected = new File(fileList.get(position).getAbsolutePath());
                if (selected.isDirectory()){
                    listDir(selected);
                } else {
                    changeDirDialog.dismiss();
                }
            }
        });

        //Quick Select List View
        secondDia_l_FoldersQ.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                File selected = new File(fileListQ.get(position));
                if (selected.isDirectory()){
                    listDir(selected);
                } else {
                    changeDirDialog.dismiss();
                }
            }
        });
    }

    //populates songList view so user can choose folder
    private void listDir(File f){
        if (f.equals(root)){
            secondDia_btn_up.setEnabled(false);
        } else {
            secondDia_btn_up.setEnabled(true);
        }

        curFolder = f;
        secondDia_t_currDir.setText(f.getPath());
        firstDia_t_currDir.setText(f.getPath());
        File[] files = f.listFiles();
        fileList.clear();


        //Setting the folders to display
        if (files!=null){
            for (File file : files) {

                if (file.isDirectory()){
                    fileList.add(file);
                }
            }
        }
        //ArrayAdapter<String> directoryList = new ArrayAdapter<String>(mainDialog.getContext(), R.layout.list_directories, R.id.l_quickselect, fileList);
        AdapterDialog<File> adapterDialog = new AdapterDialog<File>(mainDialog.getContext(), fileList);
        secondDia_l_Folders.setAdapter(adapterDialog);

    }



    @Override
    public void onDestroyView() {
        songList.clear();
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mListener = (FragmentDialogListener) context;
        }catch (ClassCastException  e){
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

}

//Adapter for FileListView and for Selected Songs
class AdapterDialog<T> extends ArrayAdapter<T>{

    AdapterDialog(Context context, List<T> resource) {
        super(context, 0, resource);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        PlaceHolder holder;

        if (convertView==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_directories, parent, false);
            holder = new PlaceHolder();
            holder.dirName = (TextView) convertView.findViewById(R.id.t_dir);
            convertView.setTag(holder);
        } else {
            holder = (PlaceHolder) convertView.getTag();
        }

        T t = getItem(position);
        //For Files
        if (t instanceof File){
            File thisFile = (File) t;
            holder.dirName.setText(thisFile.getName());

            if (!thisFile.canWrite()){
                holder.dirName.setTextColor(Color.RED);
            } else {
                holder.dirName.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            }
        }

        //For Songs
        if (t instanceof Song){
            Song song = (Song) t;
            holder.dirName.setText(song.getSongName());
        }

        //For Playlists
        if (t instanceof String){
            String string = (String) t;
            holder.dirName.setText(string);
            holder.dirName.setTextColor(convertView.getResources().getColor(R.color.textLightBlue));
        }

        return convertView;
    }

    private class PlaceHolder{
        TextView dirName;
    }
}