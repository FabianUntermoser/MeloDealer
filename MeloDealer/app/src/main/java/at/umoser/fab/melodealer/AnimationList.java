package at.umoser.fab.melodealer;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;

class AnimationList {

    static void animationBlendIn(View view){
        //List remove Animation
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(view, View.ALPHA,  0f, 1f);
        fadeOut.setDuration(App.durationAnimation);
        animatorSet.playTogether(fadeOut);
        animatorSet.start();
    }

}
