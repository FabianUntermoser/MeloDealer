package at.umoser.fab.melodealer;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.Session;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class ActivityPhone extends AppCompatActivity
implements FragmentSimpleList.FragmentListener,
            SearchView.OnQueryTextListener{


    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private Toolbar toolbar;
    List<Fragment> fragmentList = new ArrayList<>();

    FragmentSimpleList fragmentSimpleList;
    FragmentSimpleList fragmentSimpleListDownloaded;

    List<Song> allTracks = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = sharedPreferences.getString(CurrentSession.getHolder().getMac(), null);

        hasExternalStoragePermission();

        toolbar          = (Toolbar)                findViewById(R.id.toolbar);
        changeStatusBarBackgroundColor(name);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_phone);
        tabLayout.setTabTextColors(Color.GRAY, Color.WHITE);
        tabLayout.setupWithViewPager(mViewPager);

        getSmartphoneMusic();
    }

    /**
     * INTERFACE METHOD: Called from FragmentSimpleList
     */
    @Override
    public void closeActivity() {
        finish();
    }

    /**
     * INTERFACE METHOD: Called from FragmentSimpleList
     */
    @Override
    public void sendFile(Song song) {
        scpFileToPi(song);
    }

    /**
     * Styles the Toolbar
     * @param name
     */
    private void changeStatusBarBackgroundColor(String name){
        //Toolbar setup
        toolbar.setTitleTextColor(Color.WHITE);
        if (name!=null){
            toolbar.setTitle(name);
        }
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //Changes status bar background color on lollipop devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_music, menu);

        // Associate searchable configuration (xml.searchable) with the SearchView
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        //Here the Menu Item is converted to the support SearchView
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                closeActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called after SearchView Text was submitted - not needed
     * @param query
     * @return
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * Called when every new char is added to SearchView
     * @param newText
     * @return
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        //Search Magic
        newText = newText.toLowerCase();
        List<Song> newList = new ArrayList<>();
        for (Song song : allTracks) {
            String name = song.getSongName().toLowerCase();
            if (name.contains(newText)){
                newList.add(song);
            }
        }
        for (Fragment fr : fragmentList) {
            FragmentSimpleList simpleList = (FragmentSimpleList) fr;
            simpleList.onSearchUpdate(newList);
        }

        return true;
    }


    /**
     * Scans smartphone for music files
     */
    public void getSmartphoneMusic() {
        //Checks Permissions first
        if (hasExternalStoragePermission()){
            new AsyncTask<Integer, List<Song>, List<Song>>() {

                @Override
                protected List<Song> doInBackground(Integer... params) {
                    ContentResolver musicResolver = getContentResolver();
                    Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
                    List<Song> playlistPhone = new ArrayList<>();

                    try {
                        if(musicCursor !=null && musicCursor.moveToFirst()){
                            //Audio informationen holen
                            int idColumn = musicCursor.getColumnIndex
                                    (MediaStore.Audio.Media._ID);
                            int displayNameColumn = musicCursor.getColumnIndex
                                    (MediaStore.Audio.Media.DISPLAY_NAME);
                            int titleColumn = musicCursor.getColumnIndex
                                    (MediaStore.Audio.Media.TITLE);
                            int artistColumn = musicCursor.getColumnIndex
                                    (MediaStore.Audio.Media.ARTIST);
                            int dataColumn = musicCursor.getColumnIndex
                                    (MediaStore.Audio.Media.DATA);
                            //Infos in Song Objekt einfügen
                            do {
                                String thisTitle = musicCursor.getString(titleColumn);
                                String thisDisplayName = musicCursor.getString(displayNameColumn);
                                String thisArtist = musicCursor.getString(artistColumn);
                                String filepath = musicCursor.getString(dataColumn);

                                playlistPhone.add(new Song(1, thisTitle, thisDisplayName, thisArtist, filepath));
                            }
                            while (musicCursor.moveToNext());
                        }

                        //Sort by alphabet
                        Collections.sort(playlistPhone, new Comparator<Song>() {
                            @Override
                            public int compare(Song a, Song b) {
                                return a.getSongName().compareTo(b.getSongName());
                            }
                        });

                        return playlistPhone;

                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    } finally {
                        //MusicCursor schließen
                        try {
                            if (musicCursor != null && !musicCursor.isClosed()) {
                                musicCursor.close();
                            }
                        } catch (Exception ex){ex.printStackTrace();}
                    }


                }

                @Override
                protected void onPostExecute(List<Song> playlistPhone) {

                    //Setting a list that contains all results
                    allTracks.clear();
                    allTracks.addAll(playlistPhone);

                    for (Fragment fr : fragmentList) {
                        FragmentSimpleList simpleList = (FragmentSimpleList) fr;
                        simpleList.postData(playlistPhone);
                    }

                    super.onPostExecute(playlistPhone);
                }
            }.execute(0);

        } else {
            closeActivity();
        }

    }

    /**
     * Checks need Permissions for Reading/Writing Data
     * @return - if Permissions were granted
     */
    private boolean hasExternalStoragePermission(){
        //Checks Permission for SD Card writing
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            //Request Permission to read external Data
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        App.MY_PERMISSION_REQUEST_EXTERNAL_STORAGE);
            }
            return false;
        }
    }

    /**
     * Called after Permissions were asked in hasExternalStoragePermission()
     * @param requestCode - determines which Permission Request was returned
     * @param permissions - Array of permissons that were requested
     * @param grantResults - result of request, if granted smartphone can be searched, if not, activity is closed
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case App.MY_PERMISSION_REQUEST_EXTERNAL_STORAGE:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getSmartphoneMusic();
                } else {
                    closeActivity();
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Uses SCP Function of Jsch Libary to send File to Raspberry Pi
     * @param songClicked - song used to get necessary data for scp
     */
    private void scpFileToPi(final Song songClicked) {
        new AsyncTask<Integer, Object, Object>() {
            @Override
            protected Object doInBackground(Integer... params) {

                final Session session = CurrentSession.getHolder().getSession();

                FileInputStream fis=null;
                try{
                    String lfile = songClicked.getFilePath();
                    String rfile = "/home/pi/Musik";

                    boolean ptimestamp = true;

                    // exec 'scp -p -t rfile' remotely
                    // -p: Preserves modification times, access times, and modes from the original file.
                    String command="sudo scp " + (ptimestamp ? "-p" :"") +" -t "+rfile;
                    Channel channel=session.openChannel("exec");
                    ((ChannelExec)channel).setCommand(command);
                    Log.i(App.TAG, "This the command: " + command);

                    // get I/O streams for remote scp
                    OutputStream out=channel.getOutputStream();
                    InputStream in=channel.getInputStream();

                    channel.connect();

                    if(checkAck(in)!=0){
                        //System.exit(0);
                        Log.i(App.TAG, "SCP ANTWORT: " + checkAck(in) );
                    }

                    File _lfile = new File(songClicked.getFilePath());

                    if(ptimestamp){
                        command="T"+(_lfile.lastModified()/1000)+" 0";
                        // The access time should be sent here,
                        // but it is not accessible with JavaAPI ;-<
                        command+=(" "+(_lfile.lastModified()/1000)+" 0\n");
                        out.write(command.getBytes()); out.flush();
                        if(checkAck(in)!=0){
                            //System.exit(0);
                            Log.i(App.TAG, "SCP ANTWORT: " + checkAck(in) );
                        }
                    }

                    Log.i(App.TAG, "This the command: " + command);


                    // send "C0644 filesize filename", where filename should not include '/'
                    long filesize=_lfile.length();
                    String command1="C0644 "+filesize+" ";
                    String command2;
                    if(lfile.lastIndexOf('/')>0){
                        command2 = lfile.substring(lfile.lastIndexOf('/')+1);
                    }
                    else{
                        command2=lfile;
                    }
                    if (command2.startsWith("-")){
                        command2 = command2.substring(1);
                    }
                    command2+="\n";
                    command += command1+command2;
                    out.write(command.getBytes()); out.flush();
                    if(checkAck(in)!=0){
                        //System.exit(0);
                        Log.i(App.TAG, "SCP ANTWORT: " + checkAck(in) );

                    }

                    Log.i(App.TAG, "This the command: " + command);

                    // send a content of lfile
                    fis=new FileInputStream(lfile);
                    byte[] buf=new byte[1024];
                    while(true){
                        int len=fis.read(buf, 0, buf.length);
                        if(len<=0) break;
                        out.write(buf, 0, len); //out.flush();
                    }
                    fis.close();
                    fis= null;
                    // send '\0'
                    buf[0]=0; out.write(buf, 0, 1); out.flush();
                    if(checkAck(in)!=0){
                        //System.exit(0);
                        Log.i(App.TAG, "SCP ANTWORT: " + checkAck(in) );
                    }
                    out.close();

                    channel.disconnect();

                    //System.exit(0);
                }
                catch(Exception e){
                    e.printStackTrace();
                    Log.i(App.TAG, "Could not transfer Tracks");
                    try{if(fis!=null)fis.close();}catch(Exception ee){}
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                //User Interface Updaten
                CurrentSession.getHolder().updateMPD();
                String filePath = songClicked.getFilePath();
                String path = filePath.substring(filePath.lastIndexOf("/") + 1);
                new SendCommand(App.doNothing, "mpc add \"" + path + "\"").execute();
                CurrentSession.getHolder().updatePlaylist();
                CurrentSession.getHolder().updateStatus();
                Toast.makeText(getApplicationContext(), songClicked.getSongName() + getApplicationContext().getString(R.string.toast_filesent), Toast.LENGTH_SHORT).show();

                super.onPostExecute(o);
            }
        }.execute();
    }

    /**
     * Method needed in scpFileToPi() for scp Protocol
     * @param in
     * @return - output value:
     *              0 for succes,
     *              1 for error,
     *              2 for fatal error
     *              -1
     * @throws IOException
     */
    static int checkAck(InputStream in) throws IOException {
        int b = in.read();
        // b may be 0 for success,
        //          1 for error,
        //          2 for fatal error,
        //          -1
        if(b==0) return b;
        if(b==-1) return b;

        if(b==1 || b==2){
            StringBuilder sb=new StringBuilder();
            int c;
            do {
                c=in.read();
                sb.append((char)c);
            }
            while(c!='\n');
            if(b==1){ // error
                System.out.print(sb.toString());
            }
            if(b==2){ // fatal error
                System.out.print(sb.toString());
            }
        }
        return b;
    }


    /**
     * Custom Adapter to display Fragments in Viewpager
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position==0) {
                if (fragmentSimpleList==null) {
                    fragmentSimpleList = FragmentSimpleList.newInstance(position);
                    fragmentList.add(fragmentSimpleList);
                    return fragmentSimpleList;
                } else {
                    return fragmentSimpleList;
                }
            } else if (position==1) {
                if (fragmentSimpleListDownloaded ==null) {
                    fragmentSimpleListDownloaded = FragmentSimpleList.newInstance(position);
                    fragmentList.add(fragmentSimpleListDownloaded);
                    return fragmentSimpleListDownloaded;
                } else {
                    return fragmentSimpleListDownloaded;
                }
            } else {
                throw new IllegalStateException("illegal position");
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Tracks";
                case 1:
                    return "Downloaded";

            }
            return null;
        }
    }



}


















