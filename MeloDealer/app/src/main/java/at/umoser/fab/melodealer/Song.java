package at.umoser.fab.melodealer;

import android.os.Parcel;
import android.os.Parcelable;


class Song implements Parcelable, Comparable<Song>{

    private int FLAG;
    private String songName;
    private String title;
    private String displayName;
    private String artist;
    private String filePath;
    private String timeLenght;
    private int playlistNumber;


    Song(int FLAG, String songTitle, String displayName, String songArtist, String filePath) {
        this.FLAG = FLAG;
        this.title = songTitle;
        this.displayName = displayName;
        this.artist = songArtist;
        this.filePath = filePath;
    }

    private Song(Parcel input){
        FLAG = input.readInt();
        songName = input.readString();
        title = input.readString();
        displayName = input.readString();
        artist = input.readString();
        filePath = input.readString();
        timeLenght = input.readString();
        playlistNumber = input.readInt();

    }


    int getFLAG(){return FLAG;}
    String getSongName(){
        if (songName==null){
            return buildSongName();
        } else {
            return songName;
        }
    }
    public String getTitle(){return title;}
    public String getDisplayName(){return displayName;}
    String getArtist(){return artist;}
    String getFilePath(){return filePath;}
    String getTimeLenght(){return timeLenght;}
    int getPlaylistPos(){return playlistNumber;}

    public void setTitle(String title){
        this.title = title;
    }
    void setArtist(String artistName) {
        this.artist = artistName;
    }
    void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    void setTimeLength(String timeLenght) {
        this.timeLenght = timeLenght;
    }
    private void setFLAG(int FLAG){
        this.FLAG = FLAG;
    }
    void setPlaylistNo(int playlistNumber) {
        this.playlistNumber = playlistNumber;
    }

    //Sets Flag for new Objects
    void initializeFlags() {
        if (filePath!=null && !filePath.isEmpty() && filePath.startsWith("http://")) {
            artist = filePath.split("#")[1];
            setFLAG(App.FLAG_RADIO);
        } else if (filePath!=null && !filePath.isEmpty() && filePath.startsWith("usbstick0/")) {
            setFLAG(App.FLAG_USB);
        }
    }

    //Parses paths and file extensions to song names
    private String buildSongName(){
        String name;
        if (title!=null && !title.isEmpty() && title.length()>=2){
            songName = title;
        } else if (displayName!=null && !displayName.isEmpty()){
            name = displayName;
            if (displayName.lastIndexOf(".mp3") > 0) {
                name = name.substring(0, name.lastIndexOf("."));
            }
            if (name.lastIndexOf(".m4a") > 0) {
                name = name.substring(0, name.lastIndexOf("."));
            }
            songName = name;
        } else {
            name = filePath;
            if (name.startsWith("-")){
                name = name.substring(1);
            }
            if (name.startsWith("http:") && name.contains("#")){
                name = name.split("#")[1];
            }
            if (name.startsWith("usbstick0/")){
                name = name.substring(name.lastIndexOf("/")+1);
            }
            if (name.lastIndexOf(".mp3") > 0) {
                name = name.substring(0, name.lastIndexOf("."));
            }
            if (name.lastIndexOf(".m4a") > 0) {
                name = name.substring(0, name.lastIndexOf("."));
            }
            songName = name;
        }
        return songName;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(FLAG);
        parcel.writeString(songName);
        parcel.writeString(title);
        parcel.writeString(displayName);
        parcel.writeString(artist);
        parcel.writeString(filePath);
        parcel.writeString(timeLenght);
        parcel.writeInt(playlistNumber);

    }

    public static final Parcelable.Creator<Song> CREATOR
            = new Parcelable.Creator<Song>() {
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        public Song[] newArray(int size) {
            return new Song[size];
        }
    };


    /**
     * CompareTo needed by Treeset selected_list in ActivityGUI
     * @param song
     * @return 0 if equal, -1 if less than
     */
    @Override
    public int compareTo(Song song) {

        if (song.getPlaylistPos()==getPlaylistPos()){
            return 0;
        } else if (song.getPlaylistPos()>getPlaylistPos()){
            return -1;
        } else {
            return 1;
        }

    }
}
