package at.umoser.fab.melodealer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

class AdapterMusicTitles extends ArrayAdapter<Song>{


    AdapterMusicTitles(Context context, List<Song> resource) {
        super(context, 0, resource);
    }

    private class PlaceHolder {
        ImageView currSongIcon;
        TextView currSongTitle;
        TextView currSongArtist;
        TextView currSongTime;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        PlaceHolder holder;

        if (convertView==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.song_name, parent, false);
            holder = new PlaceHolder();
            holder.currSongIcon = (ImageView) convertView.findViewById(R.id.song_icon);
            holder.currSongTitle = (TextView) convertView.findViewById(R.id.song_title);
            holder.currSongArtist = (TextView) convertView.findViewById(R.id.song_artist);
            holder.currSongTime = (TextView) convertView.findViewById(R.id.song_time_lenght);
            convertView.setTag(holder);
        } else {
            holder = (PlaceHolder) convertView.getTag();
        }

        Song songClicked = getItem(position);
        int songFlag = songClicked.getFLAG();
        String songName = songClicked.getSongName();
        String songArtist = songClicked.getArtist();
        String songTime = songClicked.getTimeLenght();

        holder.currSongArtist.setVisibility(View.VISIBLE);
        holder.currSongTime.setVisibility(View.VISIBLE);

        if (songName!=null && !songName.isEmpty()){
            holder.currSongTitle.setText(songName);
        }

        if (songArtist!=null && !songArtist.isEmpty()) {
            holder.currSongArtist.setText(songArtist);
        } else {
            holder.currSongArtist.setText("");
            holder.currSongArtist.setVisibility(View.GONE);
        }

        if (songTime!=null && !songTime.isEmpty()) {
            holder.currSongTime.setText(songTime);
        } else {
            holder.currSongTime.setText("");
        }

        //changes text colors depending on FLAG in Song class
        //greys out radios if in hotspot
        if (songFlag==App.FLAG_USB) {
            //USB Songs
            holder.currSongIcon.setBackgroundResource(R.drawable.ic_usb_black_24px);
            holder.currSongTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        } else if (songFlag==App.FLAG_RADIO){
            //Radio Songs
            holder.currSongIcon.setBackgroundResource(R.drawable.ic_radio_black_24px);
            if (CurrentSession.getHolder().getHotspotFlag()){
                holder.currSongTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.greyedOut));
                holder.currSongArtist.setTextColor(ContextCompat.getColor(getContext(), R.color.greyedOut));
                holder.currSongTime.setTextColor(ContextCompat.getColor(getContext(), R.color.greyedOut));
                holder.currSongTime.setText("Offline");
                convertView.setFocusable(true);
                convertView.setClickable(true);
            } else {
                holder.currSongTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
                holder.currSongTime.setText("");
                convertView.setFocusable(false);
                convertView.setClickable(false);

            }
        } else {
            //regular Songs
            holder.currSongIcon.setBackgroundResource(R.drawable.ic_music_note_black_24px);
            holder.currSongTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.textLightBlue));
        }


        return convertView;
    }



}

