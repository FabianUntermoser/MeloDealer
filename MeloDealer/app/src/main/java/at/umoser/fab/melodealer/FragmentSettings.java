package at.umoser.fab.melodealer;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


public class FragmentSettings extends PreferenceFragmentCompat{


    FragmentListener mListener;

    CheckBoxPreference chkBoxAutoConnect;
    CheckBoxPreference chkBoxAutoSearch;
    boolean autoSearchChecked;
    CheckBoxPreference chkBoxShowSSHResults;
    CheckBoxPreference chkBoxShowMPDResults;
    CheckBoxPreference chkBoxDebug;
    EditTextPreference chkBoxLoginName;
    EditTextPreference chkBoxLoginPW;
    FragmentManager.OnBackStackChangedListener onBackStackChangedListener;

    //FLAG to Check if any SearchResult changed its Status Flag, i.e: login info changed.
    private static boolean checkStatus = false;
    //FLAG to Check if any SearchResults should be hidden or not
    private static boolean checkHideResult = false;


    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        onBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (!isAdded()){
                    mListener.closeSettings(checkStatus, checkHideResult, onBackStackChangedListener);
                } else {
                    AppCompatActivity t = (AppCompatActivity) getActivity();
                    if (t.getSupportActionBar()!=null){
                        t.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        t.getSupportActionBar().setDisplayShowHomeEnabled(true);
                    }

                }
            }
        };

        getFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener);

    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                getFragmentManager().beginTransaction().remove(this).commit();
                mListener.closeSettings(checkStatus, checkHideResult, onBackStackChangedListener);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        view.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        getActivity().invalidateOptionsMenu();


        chkBoxShowSSHResults = (CheckBoxPreference) getPreferenceManager().findPreference(App.PREF_FILTER_SSH);
        chkBoxShowMPDResults = (CheckBoxPreference) getPreferenceManager().findPreference(App.PREF_FILTER_MPD);
        chkBoxAutoConnect = (CheckBoxPreference) getPreferenceManager().findPreference(App.PREF_AUTOCONNECT);
        chkBoxAutoSearch = (CheckBoxPreference) getPreferenceManager().findPreference(App.PREF_AUTOSEARCH);
        autoSearchChecked = chkBoxAutoSearch.isChecked();
        chkBoxDebug = (CheckBoxPreference) getPreferenceManager().findPreference(App.PREF_DEBUG);
        chkBoxLoginName = (EditTextPreference) getPreferenceManager().findPreference(App.PREF_LOGINNAME);
        chkBoxLoginPW = (EditTextPreference) getPreferenceManager().findPreference(App.PREF_LOGINPASS);



        chkBoxShowSSHResults.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                checkHideResult = true;
                return true;
            }
        });

        chkBoxShowMPDResults.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                checkHideResult = true;
                return true;
            }
        });

        chkBoxAutoConnect.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (chkBoxAutoConnect.isChecked()){
                    chkBoxAutoSearch.setChecked(true);
                } else {
                    chkBoxAutoSearch.setChecked(autoSearchChecked);
                }
                return true;
            }
        });

        chkBoxAutoSearch.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                autoSearchChecked = chkBoxAutoSearch.isChecked();
                return true;
            }
        });

        chkBoxDebug.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                if (!chkBoxDebug.isChecked()){
                    Toast.makeText(getActivity().getApplicationContext(), "Debugging is on", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Debugging is off", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        chkBoxLoginName.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String input = (String) newValue;
                if (input!=null && !input.isEmpty()){
                    CurrentSession.getHolder().setPiUsername(input);
                    checkStatus = true;
                    return true;
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Name has to be set", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        });

        chkBoxLoginPW.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String input = (String) newValue;
                CurrentSession.getHolder().setPiPassword(input);
                checkStatus = true;
                return true;
            }
        });



        //Removes Block of information about Device, if no Device is connected.
        if (CurrentSession.getHolder().getSession()!=null && CurrentSession.getHolder().getActivity()!=null && getActivity() instanceof ActivityGUI){
            CurrentSession.getHolder().updateServerStats();
        } else {
            PreferenceCategory cat = (PreferenceCategory) findPreference(App.PREF_CAT_INFO);
            getPreferenceScreen().removePreference(cat);
        }

        return view;
    }


    @Override
    public void onDetach() {
        getActivity().invalidateOptionsMenu();
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (FragmentListener) context;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public interface FragmentListener{
        void closeSettings(boolean checkStatus, boolean checkHideResult, FragmentManager.OnBackStackChangedListener listener);
    }

}
