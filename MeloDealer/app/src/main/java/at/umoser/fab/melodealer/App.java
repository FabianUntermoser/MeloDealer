package at.umoser.fab.melodealer;

import android.os.Environment;

import java.io.File;

/**
 * Class for constant variables used in Application
 */
class App {

    //String used to display Version
    final static String version = "MeloDealer 0.0.0.0 2016-12-18 Results";

    //String used for Log.i() method
    final static String TAG = "melodealerdebug";

    //Default Connection Parameters
    final static String
            hotspotSSID = "MeloDealer",             //SSID emitted from Raspberry Pi in Hotspot Mode
            defaultIP = "192.168.5.1";              //IP-Address from Raspberry Pi in Hotspot Mode
    final static int
            mpdPort = 6600,
            sshPort = 22;

    //Song FLAG values
    final static int
            FLAG_MPD = 0,                           //Indicates Song is located at MPD-Server
            FLAG_PHONE = 1,                         //Indicates Song is located on users phone
            FLAG_USB = 3,                           //Indicates Song is located at USB-Stick plugged into Raspberry Pi
            FLAG_RADIO = 4;                         //Indicates Song is Radio link

    //SearchResult STATUS values
    final static int
            STATUS_SSH = 1,                         //Indicates found SearchResult has open SSH Port (22)
            STATUS_MPD = 2,                         //Indicates found SearchResult has open MPD Port (6600)
            STATUS_SSH_MPD = 3,                     //Indicates found SearchResult has open SSH and MPD Port
            STATUS_OK = 4;                          //Indicates found SearchResult stores retrieved MAC-Address

    //ArrayList Sort Rule
    final static int
            SORT_BY_DEFAULT = 0,                    //DEFAULT: sort by PlaylistNumber
            SORT_BY_ALPHABET = 1,                   //sort alphabetically
            SORT_BY_TYPE = 2,                       //sort by FLAG
            SORT_BY_ARTIST = 3;                     //sort by Artist

    //Default Animation duration used in AnimationList
    final static int durationAnimation = 300;


    //Integers used to identify Permission Requests in onRequestPermissionsResult
    final static int MY_PERMISSION_REQUEST_EXTERNAL_STORAGE = 1;


    //String used as Keys to get Variables from Bundle in ActivityGui
    final static String
            KEY_SONGS = "LISTSONGS",                //used for ArrayList after Orientation change
            KEY_FAB = "FAB",                        //used for Floating Action Buttons after Orientation change
            KEY_DIRPATH = "DIRPATH",                //used to get Folder Path, where File was downloaded to
            KEY_RADIO = "RADIO",                    //used for ListView MultiChoiceMode and FragmentDialogs after Orientation change
            KEY_SELECTED = "SELECTED",              //used for ListView MultiChoiceMode after Orientation change
            KEY_STATUS = "STATUS",                  //used for passing around the Status Arrays
            KEY_IP = "IP",                          //used for passing around the IP-Address/Arrays
            KEY_MAC = "MAC",                        //used for passing around the MAC-Address/Arrays
            KEY_NAME = "NAME",                      //used for passing around the Name supplied to connected Pi
            KEY_RECENT = "RECENT",                  //used for passing aroung last connected IP-Address
            KEY_CONTROLACT = "CONTROLACT",          //used to get Indicator if ActivityGui should be started after Connection was established in FragmentConnect
            KEY_STATE = "STATE",                    //used to get STATE Initialisation value for FragmentDialog
            KEY_SECTION = "SECTION";                //used to get Section Number for ViewPager in FragmentSimpleList

    //Strings used as Keys to get Variables/Preferences from SharedPreferences
    final static String
            PREF_CAT_INFO = "pref_cat_device_info",  //used to get PreferenceCategory in SettingsFragment
            PREF_INTRO = "pref_intro",               //used to get Settings Intro value
            PREF_DEBUG = "pref_debug",               //used to get Settings Debug value
            PREF_AUTOCONNECT = "pref_autoconnect",   //used to get Settings Auto connect value
            PREF_AUTOSEARCH = "pref_autoconnect",    //used to get Settings Auto search value
            PREF_LOGINNAME = "pref_login_name",      //used to get Settings Login String to use while Connecting
            PREF_LOGINPASS = "pref_login_password",  //used to get Settings Password String to use while Connecting
            PREF_FILTER_SSH = "pref_search_show_ssh",//used to get Settings Filter Value for SearchResults with Status 1
            PREF_FILTER_MPD = "pref_search_show_mpd";//used to get Settings Filter Value for SearchResults with Status 2

    //Keys used to retrieve Fragments from FragmentManager
    final static String
            FRAG_SETTINGS = "SETTINGS",             //FragmentSettings
            FRAG_DIALOG = "DIALOG",                 //FragmentDialog
            FRAG_PLAYLIST = "PLAYLIST_CONTROL",     //FragmentPlaylistControl
            FRAG_INFO = "INFO",                     //FragmentInfo
            FRAG_RENAME = "RENAME",                 //FragmentRename
            FRAG_CONNECT = "CONNECT";               //FragmentConnect


    //SendCommand Parameters
    final static int
            doNothing = 0,                          //after Result Postback do nothing
            getPlaylist = 1,                        //after Result Postback populate ListView
            getStatus = 2,                          //after Result Postback display Status (Songname, Tracklength...)
            getServerStats = 3,                     //after Result Postback display ServerStats in SettingsFragment
            getAllPlaylists = 4;                    //after Result Postback display MPD Playlists in FragmentPlaylistControl


    //FragmentDialog STATE Initialisation values
    final static int
            STATE_DOWNLOAD = 1,
            STATE_REMOVE = 2;

    //FragmentInfo STATE Initialisation values
    final static int
            STATE_CONNECT = 1,
            STATE_LOGIN = 2;

    //FragmentSimpleList SECTION values
    final static int
            SECTION_TRACKS = 0,                     //Section where all Tracks found on phone are displayed
            SECTION_DOWNLOADED = 1;                 //Section where only downloaded Tracks found on phone are displayed


    //Default Path to download Music to in ActivityPhone
    final static File EXT_PUBLIC_DIR = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);




}
