package at.umoser.fab.melodealer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class FragmentRename extends Fragment {


    private View rootview;
    private InterfaceRename mListener;

    private TextInputLayout layName;
    private EditText editTextRename;
    private Button buttonRename;

    private int position;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_rename, container, false);

        layName = (TextInputLayout) rootview.findViewById(R.id.text_layout_rename);
        editTextRename = (EditText) rootview.findViewById(R.id.editTextRename);
        buttonRename = (Button) rootview.findViewById(R.id.buttonRename);

        return rootview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InterfaceRename) {
            mListener = (InterfaceRename) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        buttonRename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = editTextRename.getText().toString();

                if (!name.isEmpty()) {
                    layName.setHintEnabled(true);
                    layName.setErrorEnabled(false);
                    mListener.renameInterface(name, position);
                    //remove keyboard
                    InputMethodManager imm = (InputMethodManager) rootview.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);

                } else {
                    layName.setHintEnabled(false);
                    layName.setErrorEnabled(true);
                    layName.setError(getString(R.string.empty_name));
                }

            }
        });



    }


    public void getPosition(int pos) {
        position = pos;
    }



    public interface InterfaceRename {
        void renameInterface(String name, int position);
    }


}
