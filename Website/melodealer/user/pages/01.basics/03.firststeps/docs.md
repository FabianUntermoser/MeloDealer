---
title: Erste Schritte
taxonomy:
    category: docs
---

Standardmäßig startet der Raspberry Pi im **Hotspot Modus**. Das heißt, er sendet ein WLAN Signal mit dem Namen **MeloDealer** aus.

1. Herstellen einer Verbindung zum Hotspot
2. Öffnen der Android Applikation
3. Den Such Button anklicken
4. Gefundenen Lautsprecher auswählen
5. Eingeben der Anmelde-Informationen des Heim-WLANs

Anschließend sollte der Raspberry auch im angegebenen WLAN gefunden und gesteuert werden.
