---
title: Grundlagen
taxonomy:
    category: docs
---


![Grundriss](/user/pages/images/grundriss.png)


Ein "**Multiroom-Audiosystem**" beschreibt ein System, dass mehrere Lautsprecher in verschiedenen Räumen vorsieht. Jeder dieser Lautsprecher ist unabhängig von einander mithilfe der Android Applikation steuerbar. Um alle Lautsprecher gleichzeitig ansprechen zu können, müssen Sie sich im gleichen WLAN-Netzwerk befinden. Vorzugsweise das bereits bestehende Heim-WLAN.
