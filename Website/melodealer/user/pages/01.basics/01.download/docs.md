---
title: Download
taxonomy:
    category: docs
---

| Version 0.0.0.0 2016-12-17 | [Download](../../apk/app-debug.apk?classes=button,big) |
| :------------- | -------------: |

####changelog:
* Download der Musik Dateien vom Pi
* Durchsuchen der Songs
* Sortieren der Songs
* Designänderungen


>>>>>Applikation ist kompatibel mit **Android 4.1** aufwärts.
