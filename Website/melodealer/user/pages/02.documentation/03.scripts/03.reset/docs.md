---
title: Reset Skript
taxonomy:
    category: docs
---

Das Reset Skript ist wichtig für den Fall, das ein falsches WLAN-Netzwerk mit der Android Applikation angegeben wurde. Wenn der Raspberry nicht im Netzwerk erreichbar ist, und/oder er keinen Hotspot aussendet, muss er Resettet werden. Die Netzwerkkonfigurationen werden dann so eingerichtet, dass er wieder in den Hotspot Modus schaltet.

Besitzt der Raspberry einen Reset-Taster, so sollte in der Datei **/etc/rc.local** das **reset.py** Skript aufgerufen werden. Dieses resettet den Raspberry nur dann, wenn der entsprechende Taster betätigt wurde. Andernfalls sollte das **reset** Skript aufgerufen werden, welches nach jedem Neustart überprüft, ob eine gültige IP-Konfiguration von einem DHCP-Server erhalten wurde. Besitzt der Raspberry keine IP-Adresse, so startet er in den Hotspot Modus.

#####/home/pi/reset

```
#!/bin/sh -e
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
    printf "Connection up"
else
    printf "Connection down"
    ./home/pi/RevertToHotspot
fi
exit 0

```

---


#####/home/pi/reset.py
Wie bereits erwähnt wird dieses Skript automatisch nach dem Hochfahren mitgestartet und läuft im Hintergrund.
Nach Anschlag des Reset-Knopfes auf der Rückseite des Lautsprechers wird mittels diesem Skript das ***RevertToHotspot*** Skript gestartet.
In der Zeile `GPIO.setup` wird der *GPIO-PIN* mit der Nummer 18, auf welchem der Reset-Knopf angeschlossen ist, initialisiert.
Anschließend wird mit `GPIO.wait_for_edge` bei diesem Pin auf eine fallende Flanke, also einen Pegelwechsel gewartet.

```
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

import subprocess
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

try:
       GPIO.wait_for_edge(18, GPIO.FALLING)
       from subprocess import call
       rc=call("/home/pi/RevertToHotspot", shell=True)

except KeyboardInterrupt:
   GPIO.cleanup()
   GPIO.cleanup()

```
