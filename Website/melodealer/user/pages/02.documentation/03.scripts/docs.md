---
title: Skripte
taxonomy:
    category: docs
---

Skripte sind Dokumente, in denen eine Reihe an Terminal Inputs und Befehle definiert werden können. Diese werden dann durch ausführen des Skriptes automatisch abgearbeitet. Je nach Aktion in der Android Applikation werden verschiedenen Skripte ausgeführt.

#####Automatische Skriptausführung
Beim Hochfahren des Raspberry Pi werden mehrere Betriebsystem-Skripte automatisch ausgeführt.
Eines dieser Skripte, welches als eines der letzten ausgeführt wird, heißt **rc.local** im Ordner **/etc/**.
Diesem Skript fügen wir nun zwei Befehle hinzu:
* `sudo aplay -Ddefault:CARD=Device /home/pi/ready.wav &` - Nach dem Hochfahren wird eine WAV Datei (***/home/pi/ready.wav***) abgespielt, um die Betriebsbereitschaft zu signalisieren.

* `sudo python /home/pi/reset.py &` - Ein Skript, das auf das Drücken des Reset-Knopfes wartet, und anschließend das ***RevertToHotspot*** Skript ausführt.

```
sudo aplay -Ddefault:CARD=Device /home/pi/ready.wav &
sudo python /home/pi/reset.py &
```

Hat der Raspberry Pi keinen **Reset-Knopf**, so sollten wir ein abgeändertes Skript namens **reset** aufrufen.
```
sudo /home/pi/reset &
```

>>>Alle Skripte müssen nach erstellung erst ausführbar machen mit `sudo chmod +x filename.sh`.
