---
title: Hotspot Skript
taxonomy:
    category: docs
---

#####RevertToHotspot

Mittels dieses Skriptes wird der Raspberry Pi wieder in seinen Ausgangszustand zurückversetzt. Das **MoveHotspotConfig** Skript wird wieder an seinen ursprünglichen Platz verschoben und die Hotspot konfigurierte ***interfaces*** Datei wird mit der von **SetupNewWifiConfigFiles** geschriebenen Datei ersetzt.

```
#!/bin/bash
echo "-------------------------------------------------------------"

echo " "
echo "Moving Backup Skript back to /home/pi/"
mv /home/config/MoveHotspotConfig /home/pi

echo " "
echo "Moving Hotspot Config Files back to Default Location"
rm /etc/network/interfaces
cp /home/config/etc/network/interfaces_hotspot /etc/network/interfaces

mpc stop

reboot

echo "-------------------------------------------------------------"
Exit 0

```
