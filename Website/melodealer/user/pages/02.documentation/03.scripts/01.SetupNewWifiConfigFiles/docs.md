---
title: Wifi Skript
taxonomy:
    category: docs
---

#####MoveHotspotConfig

Dieses Skript wird mittels der App automatisiert gestartet. Das Ziel dieses Skriptes ist es den Hotspot Modus zu deaktivieren. Dies wird bewerkstelligt indem die dazu relevanten Netz-werkdateien gelöscht werden. Am Ende wird das Skript **SetupNewWifiConfigFiles** gestartet.

```
echo "-------------------------------------------------------------"

echo "Deleting old interfaces file"
 rm /etc/network/interfaces
 rm /etc/wpa_supplicant/wpa_supplicant.conf

echo "executing Skript SetupNewWifiConfigFiles"
 ./SetupNewWifiConfigFiles

echo "-------------------------------------------------------------"

```

---

#####SetupNewWifiConfigFiles

Zu Beginn wird das **MoveHotspotConfig** Skript verschoben damit dieses nicht zwei mal ausgeführt werden kann. Danach schreibt es die Dateien, welche vom vorherigen Skript gelöscht worden sind, mit den erhaltenen Informationen neu. Der Raspberry Pi wird dadurch so konfiguriert, dass er sich automatisch mit dem angegebenen Netzwerk in **wpa_supplicant.conf** verbindet. Das dort angegebene WLAN-Netzwerk wird von den Dateien **/home/pi/wifiname** und **/home/pi/passwort** gebildet. Diese Dateien werden mithilfe der App und der Eingabe des Endbenutzers beschrieben, und nach Verwendung wieder gelöscht. Nach Abschluss der Konfiguration wird der Pi neu gestartet.

```
echo "-------------------------------------------------------------"

echo "Moving MoveHotspotConfig Skript to /home/pi/BackupConfig"
mv MoveHotspotConfig /home/config/

echo " "
echo "New Interfaces File is now being created"
echo "source-directory /etc/network/interfaces.d" > /etc/network/interfaces
echo "" > /etc/network/interfaces
echo "auto lo" >> /etc/network/interfaces
echo "iface lo inet loopback" >> /etc/network/interfaces
echo "" >> /etc/network/interfaces
echo "iface eth0 inet manual" >> /etc/network/interfaces
echo "" >> /etc/network/interfaces
echo "auto wlan0" >> /etc/network/interfaces
echo "allow-hotplug wlan0" >> /etc/network/interfaces
echo "iface wlan0 inet dhcp" >> /etc/network/interfaces
echo "wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf" >> /etc/network/interfaces



echo " "
echo "New Wpa_Supplicant File is now being created"
echo "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "update_config=1" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "network={" >> /etc/wpa_supplicant/wpa_supplicant.conf
echo -n '        ssid="' >> /etc/wpa_supplicant/wpa_supplicant.conf
head -c -1  /home/pi/wifiname  >> /etc/wpa_supplicant/wpa_supplicant.conf
echo '"' >> /etc/wpa_supplicant/wpa_supplicant.conf
echo -n '        psk="' >> /etc/wpa_supplicant/wpa_supplicant.conf
head -c -1  /home/pi/password >> /etc/wpa_supplicant/wpa_supplicant.conf
echo '"' >> /etc/wpa_supplicant/wpa_supplicant.conf
echo "}" >> /etc/wpa_supplicant/wpa_supplicant.conf

rm /home/pi/wifiname
rm /home/pi/password

reboot

echo "-------------------------------------------------------------"
```
