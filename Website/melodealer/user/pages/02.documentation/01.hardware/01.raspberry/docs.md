---
title: Raspberry Pi
taxonomy:
    category: docs
---

Das Verbindungsstückes zwischen Applikation und Lautsprecher ist der Einplatinencomputer Raspberry Pi.
Dieser ist durch seine leistungsstarken Komponenten, im Vergleich mit einem normalen Desktop PC, kaum eingeschränkt.
 Von der Größe ist der Raspberry Pi in etwa mit einer Scheck-karte zu vergleichen. Entwickelt wurde der kleine PC von der Raspberry Pi Foundation.
Es gibt unterschiedliche Ausführungen beim Raspberry Pi, welche alle ähnlich zu konfigurieren sind. Besondere Modelle sind aber:
1. **Raspberry Pi Zero**
Der Raspberry Pi Zero ist das kleinste Modell und eine sehr abgespeckte Version zu den anderen Modellen. Aufgrund der fehlenden USB-Ports ist dieses Modell nicht als Lautsprecher-Basis zu empfehlen.

2. **Rasbperry Pi 3**
Das neueste Modell der Raspberry Pi Foundation bietet eine integrierte WLAN-Lösung. Damit fällt ein Bauteil, der WLAN-Adapter, bei einer Lautsprechereinheit weg. Wie in den vorgängerversionen 1 & 2 lässt die Audio Qualität des integrierten Audio-jacks zu wünschen übrig. Darum wird empfohlen, weiterhin eine USB-Soundkarte zu verwenden.
