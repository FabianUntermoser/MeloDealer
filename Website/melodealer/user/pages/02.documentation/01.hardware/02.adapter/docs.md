---
title: WLAN-Adapter
taxonomy:
    category: docs
---

Wird eine andere Version als der *Raspberry Pi 3* verwendet, so muss ein WLAN-Adapter verwendet werden. Wichtig ist, das dieser Hotspot fähig sein muss!

Der **Logilink WL0084B** eignet sich sehr gut, da er dies in seiner Auslieferung bereits unterstützt. Manche Adapter wie der **EDIMAX EW-7811Un** unterstützen diese Funktionalität nur mit einem modifiziertem Treiber.
