---
title: Hardware
taxonomy:
    category: docs
---

####Komponenten:
* **Raspberry Pi** (Zero/2/3)
Je nach Modell des Raspberry Pi wird ein WLAN-Adapter benötigt oder nicht.
* **SD-Karte**
Diese sollte mindestens 4GB groß sein.
* **USB-Soundkarte**
Die Soundkarte wird anstatt des analogen Audio-Ausganges des Raspberry Pi verwendet, da so
das Rauschen verhindert, das beim Raspberry Pi hin und wieder auftritt.
* **Optional**: Wlan-Adapter
