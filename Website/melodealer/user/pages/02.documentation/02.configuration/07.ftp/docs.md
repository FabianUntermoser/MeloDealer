---
title: FTP-Server
taxonomy:
    category: docs
---

Am besten lässt sich der Webserver des Raspberry Pi über einen FTP-Clienten (z.B *FileZilla*) updaten. Einer von vielen ist **proftpd**.

```
sudo apt-get install proftpd
```

Um den FTP Dienst PROFTPD mitzuteilen, dass wir auf den Webserver Ordner /var/www/html/ zugreifen wollen, müssen wir dessen Konfigurationsdatei unter **/etc/proftpd/proftpd.conf** folgende Zeilen anhängen.

```
DefaultRoot ~
AuthOrder mod_auth_file.c mod_auth_unix.c
AuthUserFile /etc/proftpd/ftpd.passwd
AuthPAM off
RequireValidShell off

```

Anschließend müssen FTP-Benutzer erstellt werden, mit dem wir uns in einem FTP-Clienten auf einem Computer auf den Raspberry Pi einwählen können.   
`cd /etc/proftpd/` - Wechselt in den entsprechnenden proftpd Ordner.

`sudo ftpasswd --passwd --name ftpuser --uid 33 --gid 33 --home /var/www --shell /bin/false` - Festlegen des Benutzers “***ftpuser****” der Zugriff auf den Webserver Ordner hat und der Gruppe **www-data** anbelangt.
Die Parameter **--uid** und **--guid** besagen zu welcher Benutzergruppe dieser Benutzer gehören soll. Hier sollte die **ID** der Gruppe **wwww-data** verwendet werden.

>>>>>Jeder Benutzer eines Linux-Betriebssystemes belangt zu einer oder mehreren Gruppen, die definieren, welche Berechtigungen der Benutzer hat.  
Die Gruppe www-data wird vom Apache2 verwendet. Um dessen id auszulesen kann der Befehl  `id www-data` verwendet werden.


Nach einem Neustart des FTP-Dienstes mit `sudo /etc/init.d/proftpd restart` kann die Dateiübertragung ausprobiert werden.    
Tritt noch ein Berechtigungsfehler auf, sollten folgende Befehle ausgeführt werden, um Lese/Schreibrecht auf der Ordner **/var/www** zu erhalten.

```
sudo chmod g+s /var/www
sudo chmod 775 /var/www
sudo chown -R www-data:www-data /var/www
```
