---
title: Hotspot
taxonomy:
    category: docs
---

Folgende Pakete müssen installiert werden.
* **hostapd**: Verwandelt den Raspberry Pi in einen Access Point.
(Als Standalone/Serverversion installieren)
*  **dnsmasq**: Bietet DNS / DHCP Dienste an.

```
sudo apt-get install hostapd dnsmasq
```

#####Hostapd

Als ersten Schritt müssen wir dem hostapd Dienst mitteilen, in welcher Datei wir ihn konfigurieren.   
`sudo nano /etc/default/hostapd` - Default konfigurationsdatei von hostapd.

In folgender Zeile definieren wir nun unsere Einstellungsdatei:
```
DAEMON_CONF="/etc/hostapd/hostapd.conf"
```

Mit `sudo nano /etc/hostapd/hostapd.conf` passen wir nun so unseren Hotspot an.
(`#` kennzeichnet einen Kommentar, welcher vom Code ignoriert werden)

```
#Definieren des Interfaces
interface=wlan0

#Treiber für die WLAN-Lösung (nl80211 für Raspberry Pi 3 & für Logilink WL0084B)
driver=nl80211
country_code=DE

# Deamon-Einstellungen
ctrl_interface=/var/run/hostapd
ctrl_interface_group=0

# WLAN-Konfiguration
ssid=MeloDealer
channel=1
hw_mode=g
ieee80211n=1

```

>>>Passwort Einstellungen können hier ebenfalls definiert werden!

#####Dnsmasq

`sudo nano /etc/dnsmasq.conf` - Hier befinden sich die Einstellungen des DNSMASQ Dienstes.   
Hier müssen nur zwei Zeilen am Ende der Datei angefügt werden.

```
#Definieren des Interfaces
interface=wlan0

#Definieren des IP-Bereiches, welcher an die Clients des Hotspot's verteilt werden sollen.
#    Adressbereich: 192.168.5.100 - 192.168.5.200
#    Subnetzmaske: 255.255.255.0
#    Leasetime: 12h --> Dauer, wie lange sich der Pi die Clients merkt.
dhcp-range=192.168.5.100,192.168.5.200,255.255.255.0,12h

```

---

Abschließend muss noch dem Raspberry Pi eine statische IP-Adresse vergeben werden.
Wie alle Netzwerkeinstellungen verändern wir die **interfaces** Datei mit `sudo nano /etc/network/interfaces`.

```
auto lo
iface lo inet loopback
iface eth0 inet manual
auto wlan0
allow-hotplug wlan0
iface wlan0 inet static
  address 192.168.5.1
  netmask 255.255.255.0
```
