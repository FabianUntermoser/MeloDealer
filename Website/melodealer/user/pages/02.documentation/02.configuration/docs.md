---
title: Konfiguration
taxonomy:
    category: docs
---

Im folgenden Abschnitt wird die vollständige Konfiguration eines Lautsprechers mit dem **Raspberry Pi 3** als Basis erläutert.
