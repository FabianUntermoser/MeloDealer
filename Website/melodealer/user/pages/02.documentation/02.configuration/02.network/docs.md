---
title: Netzwerk
taxonomy:
    category: docs
---
>>>>>In diesem Projekt fährt der Netzwerkverkehr ausschließlich über einen WLAN-Adapter bzw. die integrierte WLAN-Lösung. Die Konfiguration umfasst *nicht* die RJ45 Schnittstelle des Raspberry Pi


In der Konfigurationsdatei **/etc/network/interfaces** werden alle Netzwerk-Schnittstellen aufgelistet und auch konfiguriert. Desweiteren kann die Datei **/etc/wpa_supplicant/wpa_supplicant.conf** dazu verwendet werden, um die WLAN-Informationen anzugeben.

Mit `sudo nano /etc/network/interfaces` lässt sich die Datei öffnen. Nach abändern der Datei wie unten gezeigt, versucht der Raspberry Pi sich bei einem Start sich mit dem in **wpa_supplicant.conf** angegebenen Netzwerk zu verbinden.
```
auto wlan0
iface lo inet loopback
iface eth0 inet loopback
allow-hotplug wlan0
iface wlan0 inet dhcp
 wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

```

Folgend muss das Netzwerk in der Datei **/etc/wpa_supplicant/wpa_supplicant.conf** angegeben werden. `sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`

```
network={
 ssid="DER WLAN NAME"
 psk="DAS WLAN PASSWORT"
}
```

Anschließend muss der Netzwerkdienst mit `sudo service netwroking restart` neugestartet werden. Das WLAN-*Interface* kann dann (meist wlan0) mit `sudo ifup wlan0` gestartet werden.


>>>Die Applikation verwaltet nach einfügen der Skripte die Netzwerkkonfiguration. Trotzdem muss man den Raspberry Pi zunächst ins Netzwerk einbinden, um benötigte Pakete herunterzuladen.
