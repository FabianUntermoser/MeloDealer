---
title: Basiskonfiguration
taxonomy:
    category: docs
---

Nachdem das Aufspielen des Images auf die SD-Karte erfolgreich abgeschlossen wurde, kann das Betriebssystem konfiguriert werden.   
Der Raspberry Pi wird mittels HDMI an einen Monitor angeschlossen. Für die Eingabe wird eine Tastatur benötigt. Die Stromversorgung erfolgt über den Micro USB Anschluss. Nach kurzer Zeit wird man aufgefordert die Login Daten einzugeben. (Benutzername: **pi**; Passwort: **raspberry**) Hierbei ist zu beachten dass das Tastaturlayout standardmäßig ein englisches ist, somit muss man als Passwort „raspberrz“ eingeben.

Bei Raspbian Jessie Lite kommt man nun zum Terminal. Hier geben wir zu Beginn folgenden Befehl ein um zu den Einstellungen zu gelangen. Der Befehl Sudo gibt dem User erhöte Rechte
```
sudo raspi-config
```
Folgende Menüeinträge müssen nun abgearbeitet werden:

* **Internationalisation Options**
  * **Locale**   
  de_AT.UTF8 UTF8 --> system locale: de_AT.UTF8
  * **Timezone**   
  Europe, Vienna
  * **Keyboard Layout**   
  Generic --> German Austria --> Alt-GR Key: Right alt-key --> Compose Key: no compose-key
* **Passwort Ändern**   
  Dieser Schritt ist optional, allerdings sollte das Standardpasswort geändert werden.
* **Advanced**
  * **SSH**
  Da SSH die Verbindungsschnittstelle zwischen dem Lautsprecher und dem Android Gerät ist, muss diese aktiviert werden.
* **Expand Filesystem**   
  Da Win32DiskImager den Datenträger 1:1 mit der .iso Datei beschreibt,

Anschließend muss der Raspberry Pi mit `sudo reboot` neugestartet werden, sodass alle Änderungen übernommen werden.
