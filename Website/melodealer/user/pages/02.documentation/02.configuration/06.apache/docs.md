---
title: Webserver
taxonomy:
    category: docs
---

Diese Website wurde mit dem Flatfile CMS-System **Grav** erstellt und mit dem *Skeleton Theme learn2* gestaltet.
Um einen Webserver auf dem Raspberry Pi einzurichten wird das **apache2** Paket benötigt.
Die **php5** Module werden speziell von Grav benötigt.
```
sudo apt-get install apache2
sudo apt-get install php5
sudo apt-get install php5-gd
sudo apt-get install php5-curl
```
Nach der Installation können Website im **/var/www/html** Dokumente platziert werden,
die unter Verwendung der IP-Adresse des Raspberry's in einem Webbrowser erreicht werden können.


#####Eigener DNS-Name

wenn man möchte kann man seiner Website einen eigenen DNS-Namen zuteilen.
In unserem Fall möchten wir mit der Adresse **http://MeloDealer/** im Hotspot Modus auf diese Website gelangen.
Dazu muss die IP (192.168.5.1) in **/etc/hosts** mit dem gewünschten Namen eingetragen werden.

`sudo nano /etc/hosts` - Auflößung von Rechnernamen in IP-Adressen (z.B:**localhost -->  127.0.0.1**)
```
#Auflösung der Adresse 192.168.5.1 zu http://MeloDealer/
192.168.5.1     MeloDealer
```

Folgend muss mit `sudo a2enmod rewrite` das *Rewrite* Modul von apache, zum Umschreiben der Host IP-Adresse, aktiviert werden.   

Das Sicherheitsmodell von apache2 erlaubt es standardmäßig nicht auf andere Unterordner zu verlinken. Besitzt man Seiten, welche sich in Unterordner befinden muss zuerst folgende Option eingeschaltet werden, um diese über Hyperlinks auf der Website öffnen zu können.

`sudo nano /etc/apache2/apache2.conf` - Öffnet die Standardkonfigurationsdatei des Webservers apache2.

Hier muss die Zeile `AllowOverride` geändert werden:
```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```

`sudo service apache2 restart` - Neustart des Apache Webservers.
