---
title: Betriebssystem
taxonomy:
    category: docs
---

Der Raspberry Pi unterstützt ein breites Spektrum an Betriebssystemen. Neben den vielen Linux-Distributionen ist es neuerdings auch möglich Windows 10 auf den Pi zu installieren. Das wohl bekannteste Betriebssystem für den Computer im Scheckkartenformat ist Raspbian. Deshalb nahmen wir uns folgende zwei Versionen von Raspbian genauer vor:

**Raspbian Jessie **  
Raspbian Jessie ist die aktuellste, mit GUI versehene, Raspbian Version. In Sachen Grafische Benutzeroberfläche wird hierbei auf das Ressourcen-schonende LXDE verwiesen.
Wir haben uns anfänglich für diese Raspbian Jessie Version entschieden um ein Gefühl für die Linux Distribution zu bekommen. Nach kurzer Zeit beschlossen wir allerdings die erzielten Konfigurationen auf dem Betriebssystem Jessie Lite nachzubilden, da im Endeffekt der Raspberry Pi keine Grafische Benutzeroberfläche benötigt, und dies sonst nur unnötig Resourcen verschwenden würde.

**Raspbian Jessie Lite**  
Raspbian Jessie Lite ist eine "abgespeckte" Version von Raspbian Jessie. Der Hauptunterschied von Jessie Lite liegt bei der nicht vorhanden Grafischen Benutzeroberfläche. Es wird also alles über das Terminal gesteuert.

Zuerst muss das aktuelle Raspbian Betriebssystem heruntergeladen werden. Dieses wird in Form eines Images bereitgestellt und kann auf der offiziellen [Webseite](https://www.raspberrypi.org/downloads/) von Raspberry Pi heruntergeladen werden. Der heruntergeladene Zip-Ordner, muss mit einem Programm wie *Win-Rar* oder *7-Zip* entpackt werden.

Im nächsten Schritt haben wir unter Windows den **Win32DiskImager** heruntergeladen und installiert. Dieser ist dafür da, das heruntergeladene Image auf einem Datenträger zu brennen. Anschließend muss eine SD-Karte an den PC angeschlossen werden, ist kein SD-Karten Leser enthalten, muss ein USB Adapter für Abhilfe schaffen. Die SD-Karte muss vor dem beschreiben der **.iso** Datei, vollständig formartiert werden! Danach wird der Win32DiskImager als Administrator ausgeführt. Hier kann man nun den Speicherort des Images und die Ziel-SD-Karte auswählen. Im letzten Schritt klickt man auf den Write-Button und dass Image wird installiert. Nach vollständiger Installation kann man die SD-Karte in den Kartenslot des Raspberry Pi's schieben und diesen starten.

>>>Kann die SD-Karte im Win32DiskImager nicht ausgewählt werden, kann das mehrere Gründe haben   
**•** Die SD-Karte hat Partitionen, die das Windows Betriebssystem nicht lesen kann   
**•** Die SD-Karte ist bietet unvollständige Volumeninformationen an.   
In der **Datenträgerverwaltung** des Computers ist es möglich den Datenträger zu formatieren.   
**Wählen Sie nicht den falschen Datenträger aus!**   
Als letzte Maßnahme können Sie versuchen, die SD-Karte mittels **diskpart** zu bereinigen.

Schritte zum formatieren eines Datenträgers mittels **dispkart**:
1. Öffnen einer Eingabeaufforderung
2. Eingeben des Befehles `dispkart`, dieser öffnet nun die dispkart-Shell
3. `list disk` - Zeigt alle verfügbaren Datenträger an.
4. `select disk x` - X ist eine Zahl, welcher aus der Liste abgelesen werden kann.
5. Hoffen, dass Sie nicht ihre Hauptfestplatte ausgewählt haben.
6. `clean` - Befehl löscht die Volumeninformationen des Datenträgers.
7. ???
8. PROFIT!
