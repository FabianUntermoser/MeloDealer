---
title: USB
taxonomy:
    category: docs
---

Um in der App die Möglichkeit zu haben, einen USB Stick nach Musik zu scannen und diese der Playlist hinzuzufügen, müssen USB-Sticks automatisch gemountet werden. In den Raspbian Versionen mit einer grafischen Oberfläche ist dies ohne weiteres möglich, in der *Lite*-Version ist dies nicht der Fall.
Der erste Schritt wäre den Treiber für die das **NTFS**-Dateisystem zu installieren. **FAT**-Treiber sind bereits vorhanden.
* **ntfs-3g**: Fehlendes NTFS-Treiberpaket
* **usbmount**: Tool, welches die Mounting-Arbeit für uns übernimmt.


```
sudo apt-get install ntfs-3g usbmount
```

`sudo nano /etc/usbmount/usbmount.conf`- Konfigurationsdatei von *usbmount*

In diesem Dokument muss festgehalten werden, welche Dateisysteme gemountet werden sollen.

```
FILESYSTEMS="vfat ntfs fuseblk ext2 ext3 ext4 hfsplus"
```
Desweiteren muss definiert sein, wie diese Dateisysteme gemountet werden sollen.
Diese Einträge in *FS_MOUNTOPTIONS* sind besonders wichtig:
* **ntfs-3g / fuseblk**
* **vfat**

```
FS_MOUNTOPTIONS="-fstype=ntfs-3g,nls=utf8,umask=007,gid=46
-fstype=fuseblk,nls=utf8,umask=007,gid=46 -fstype=vfat,gid=1000,uid=1000,umask=007"
```

>>>Um NTFS-Dateisysteme mounten zu können muss in **FILESYSTEMS=""** die Einträge **ntfs** und **fuseblk** hinzugefügt werden. Manchmal listet das *ntfs-3g*-Paket NTFS-Geräte nämlich als *fuseblk*.   
In **FS_MOUNTOPTIONS=""** können Dateisysteme beginnend mit **-fstype=** definiert werden.


---

#####Bugfix

Es kann auftreten, dass USB-Sticks nun gemountet werden, allerdings nach einer Sekunde wieder entfernt werden und nicht mehr zugreifbar sind.
Christian Weinberger hat im Internet einen Fix für dieses Problem veröffentlicht. Dazu müssen folgende Schritte befolgt werden.

`sudo nano /etc/udev/rules.d/usbmount.rules` - Regeln zum Entfernen von USB-Geräten

```
KERNEL=="sd*", DRIVERS=="sbp2",         ACTION=="add",  PROGRAM="/bin/systemd-escape -p --template=usbmount@.service $env{DEVNAME}", ENV{SYSTEMD_WANTS}+="%c"
KERNEL=="sd*", SUBSYSTEMS=="usb",       ACTION=="add",  PROGRAM="/bin/systemd-escape -p --template=usbmount@.service $env{DEVNAME}", ENV{SYSTEMD_WANTS}+="%c"
KERNEL=="ub*", SUBSYSTEMS=="usb",       ACTION=="add",  PROGRAM="/bin/systemd-escape -p --template=usbmount@.service $env{DEVNAME}", ENV{SYSTEMD_WANTS}+="%c"
KERNEL=="sd*",                          ACTION=="remove",       RUN+="/usr/share/usbmount/usbmount remove"
KERNEL=="ub*",                          ACTION=="remove",       RUN+="/usr/share/usbmount/usbmount remove"
```

`sudo nano /etc/systemd/system/usbmount@.service` - Benötigter Service

```
[Unit]
BindTo=%i.device
After=%i.device

[Service]
Type=oneshot
TimeoutStartSec=0
Environment=DEVNAME=%I
ExecStart=/usr/share/usbmount/usbmount add
RemainAfterExit=yes

```


`sudo reboot` - Startet den Raspberry neu.

>>>Mit dem Befehl `cat /etc/mtab` sieht man, auf welches Verzeichnis die eingebundenen USB-Geräte zugewiesen sind. Standardmäßig werden die Geräte im Ordner **/media/usb0** bis **/media/usb7** zugewiesen. Eine weitere Auflistungsmöglichkeit, welche einige Details verbergt, ist `ls -l /dev/disk/by-uuid/`.

---

#####Symbolische Links
Dieser Schritt ist wichtig, damit die Applikation bei Verwaltung eines USB-Sticks richtig funktioniert. Eine Vorraussetzung ist es, das gemountete USB-Sticks von dem Ordner **/home/pi/Musik/usbstick0** aus erreichbar sind. Um jetzt nicht alle gemountete Geräte, in den Musik Ordner des MPD-Servers zu verschieben, erstellen wir einen symbolischen Link mit `sudo ln –s /media/usbstick0 /home/pi/Musik/usbstick0`.
