---
title: MPD / MPC
taxonomy:
    category: docs
---

***Music Player Daemon*** ist das Kern-Softwarepaket im Raspberry Pi, der als Musik Server fungiert. Neben der unterstützung vieler Audioformate bietet er Playlist-Funktionalität und dank vieler Plugins und Bibliotheken, kann er verschieden gestaltet werden.

Der Aufbau des **mpd** Paketes erlaubt es, verschiedene Pakete zur Steuerung des Musikservers zu verwenden. Das Terminalpaket **mpc**, erlaubt es mit einfachen Terminalbefehlen den Musikserver zu steuern.

```
sudo apt-get install mpd mpc
```

`sudo nano /etc/mpd/mpd.conf` - Öffnen der Standarteinstellungen des **MPD-Servers**

Folgende Abänderungen müssen getroffen werden, damit der Musikserver Ordnungsgemäß funktioniert.
(`//` kennzeichnet einen Kommentar, welcher vom Code ignoriert werden)
```
//Standardkonfiguration des MPD-Servers
//Ordner, indem sich die Musikdateien befinden
music_directory         "/home/pi/Musik"

//Ordner, indem sich Playlist Dateien (.m3u) befinden
playlist_directory     "/home/pi/Playlists"

//Wird kein/e user/group angegeben, startet der MPD-Server mit Root-Rechten
#user                  "mpd"
#group                 "nogroup"

//Ermöglicht die Erreichbarkeit des Servers im Netzwerk
#bind_to_address       "localhost"

//Standardport des MPD-Servers
//wird in der Applikation zur Netzwerksuche verwendet.
port                   "6600"

//Einstellungen für das ALSA-Paket
audio_output {
     type            "alsa"
     name            "My ALSA Device"

     //hw:1,0 gibt die Soundkarte als Ausgabegerät an
     device          "hw:1,0"

     //"software" - erlaubt mpc die Lautstärkeregelung
     mixer_type      "software"
}
```


`sudo service mpd restart` - Startet den MPD-Dienst neu.

Nun können abgelegte Musikdateien im Ordner **/home/pi/Musik/**, vom MPD-Server in seine Datenbank eingelesen werden.

>>>Werden Dateien geändert, muss der MPD-Server zu aktualisierung seiner Datenbank aufgerufen werden. Dies wird mit dem Befehl `mpc update` bewerkstelligt.     
**Troubleshooting Befehle**:  
**• sudo service mpd status** - Anzeigen der Service Status Informationen     
**• sudo service mpd restart** - Neustarten des Services  
