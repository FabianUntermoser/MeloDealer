---
title: Audio
taxonomy:
    category: docs
---
Mithilfe des ALSA-Paketes wird die Soundkarte angesprochen. Zum installieren des Paketes muss folgender Befehl ausgeführt werden.

```
sudo apt-get install alsa-utils
```

`sudo modprobe snd_bcm2835` - Ladet das Audio Modul

`sudo amixer cset numid=3 1` - Wählt den Standardaudioausgang (Zahl am Ende)

>>>>>**Audioausgang**:   
**• 0** = Automatische Auswahl   
**• 1** = 3,5mm analoge Audiobuchse   
**• 2** = HDMI:   


`sudo alsamixer` - Öffnet den Audiomixer des ALSA-Paketes

`sudo alsactl store` - Speichern der Onboard-Soundkarteneinstellungen

Hier kann man mit der Taste **F6** die richtige Soundkarte anwählen und die Standard Musikwiedergabelautspreche auf der Hardware-Seite definieren. (Aus Qualitätsgründen sollte hardwareseitig die Wiedergabelautstärke maximal angeschlagen sein.)


>>>**Troubleshooting Befehle**:  
**• cat /proc/asound/cards** - Anzeigen der Soundkarten  
**• amixer** - Anzeigen der Alsa-Mixer Einstellungen  
