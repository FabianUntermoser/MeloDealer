<?php

/* partials/logo.html.twig */
class __TwigTemplate_f5467dedf7d52195c642162a2cf4f377fc8d5bd2161723bdc64d52c9f655f58f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg id=\"logo-svg\"  style=\"background-color:#ffffff00\" version=\"1.1\"
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
   xmlns:cc=\"http://creativecommons.org/ns#\"
   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
   xmlns:svg=\"http://www.w3.org/2000/svg\"
   xmlns=\"http://www.w3.org/2000/svg\"
   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"
   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"
   width=\"525\"
   height=\"85\"
   viewBox=\"0 0 525 85.000004\"
   id=\"svg2\"
   version=\"1.1\"
   inkscape:version=\"0.91 r13725\"
   sodipodi:docname=\"MeloDealer Font.svg\">
  <defs
     id=\"defs4\" />
  <sodipodi:namedview
     id=\"base\"
     pagecolor=\"#ffffff\"
     bordercolor=\"#666666\"
     borderopacity=\"1.0\"
     inkscape:pageopacity=\"0.0\"
     inkscape:pageshadow=\"2\"
     inkscape:zoom=\"2.0000001\"
     inkscape:cx=\"270.54379\"
     inkscape:cy=\"25.646239\"
     inkscape:document-units=\"px\"
     inkscape:current-layer=\"layer1\"
     showgrid=\"false\"
     inkscape:window-width=\"2560\"
     inkscape:window-height=\"1377\"
     inkscape:window-x=\"-8\"
     inkscape:window-y=\"-8\"
     inkscape:window-maximized=\"1\"
     units=\"px\" />
  <metadata
     id=\"metadata7\">
    <rdf:RDF>
      <cc:Work
         rdf:about=\"\">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label=\"Ebene 1\"
     inkscape:groupmode=\"layer\"
     id=\"layer1\"
     transform=\"translate(0,-967.36227)\">
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 97.589492,967.50266 -48.675462,43.26624 -1.327426,-1.4985 -46.82983044,-41.62224 0,37.53534 46.82983044,41.6274 1.327426,1.4934 48.675462,-43.2664 0,-37.53524 z\"
       id=\"path4287\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 249.57138,967.55886 0.008,0.008 -0.0766,0 0,71.29264 13.00872,0 0,-58.35797 30.85995,30.86497 -28.28169,28.2869 10.59383,10.5963 35.18683,-35.1893 -3.90568,-3.9057 3.6938,-3.6939 -39.89662,-39.9016 -21.19021,0 z\"
       id=\"path4285\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 163.92984,991.3299 0,58.7868 7.88028,0 0,-58.7868 -7.88028,0 z\"
       id=\"path4283\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 424.82651,991.9961 0,58.7868 7.88029,0 0,-58.7868 -7.88029,0 z\"
       id=\"path4281\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 129.03575,1011.7112 c -5.46493,0 -10.1004,1.9457 -13.90472,5.8407 -3.77412,3.8647 -5.6594,8.4397 -5.6594,13.7235 0,5.6461 1.87209,10.3709 5.61601,14.1753 3.74395,3.8043 8.39262,5.7079 13.94811,5.7079 7.24631,0 13.02822,-2.7924 17.34581,-8.3781 l -5.7513,-4.3498 c -3.01929,3.7137 -6.76346,5.5726 -11.23201,5.5726 -2.89854,0 -5.58572,-0.8757 -8.06153,-2.6268 -2.44563,-1.7814 -4.01535,-4.5895 -4.70979,-8.424 l 31.8836,0 c 0.0602,-6.2801 -1.82743,-11.3836 -5.66195,-15.3087 -3.83451,-3.9554 -8.43848,-5.9326 -13.81283,-5.9326 z m 0,7.2447 c 4.95166,0 8.6958,2.5688 11.23202,7.7016 l -22.82652,0 c 2.32487,-5.1328 6.18997,-7.7016 11.5945,-7.7016 z\"
       id=\"path4279\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 206.15884,1011.7112 c -5.22337,0 -9.85888,1.9932 -13.90472,5.9786 -4.01569,3.9554 -6.02444,8.483 -6.02444,13.5856 0,5.3442 2.02453,9.9953 6.07039,13.9506 4.07605,3.9553 8.87717,5.9326 14.4025,5.9326 5.16299,0 9.7065,-1.9929 13.63159,-5.9784 3.95529,-4.0157 5.93254,-8.6512 5.93254,-13.9048 0,-5.2234 -2.00877,-9.7826 -6.02445,-13.6775 -4.01565,-3.925 -8.70904,-5.8867 -14.08341,-5.8867 z m 0.2706,7.7884 c 3.32121,0 6.1898,1.1781 8.60525,3.5331 2.41542,2.3249 3.62231,5.0724 3.62231,8.2427 0,3.2004 -1.22264,6.0085 -3.66827,8.424 -2.44561,2.4155 -5.29846,3.6223 -8.55929,3.6223 -3.35143,0 -6.28048,-1.1753 -8.7865,-3.5304 -2.47584,-2.3551 -3.71423,-5.1025 -3.71423,-8.2427 0,-3.2911 1.22521,-6.1151 3.67083,-8.47 2.47582,-2.3853 5.41807,-3.579 8.8299,-3.579 z\"
       id=\"path4277\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 333.43541,1012.3775 c -5.46494,0 -10.09785,1.9482 -13.90216,5.8432 -3.77414,3.8647 -5.66196,8.4397 -5.66196,13.7235 0,5.6461 1.87207,10.371 5.61601,14.1753 3.74393,3.8043 8.3926,5.7054 13.94811,5.7054 7.24631,0 13.02821,-2.7925 17.34581,-8.3781 l -5.7513,-4.3473 c -3.0193,3.7137 -6.76345,5.57 -11.23201,5.57 -2.89853,0 -5.58571,-0.8754 -8.06153,-2.6267 -2.44563,-1.7813 -4.01534,-4.5894 -4.70979,-8.424 l 31.8836,0 c 0.0604,-6.2801 -1.82489,-11.3835 -5.65939,-15.3087 -3.83451,-3.9552 -8.44104,-5.9326 -13.81539,-5.9326 z m 0,7.2472 c 4.95164,0 8.69581,2.5664 11.23201,7.6991 l -22.82396,0 c 2.32486,-5.1327 6.1874,-7.6991 11.59195,-7.6991 z\"
       id=\"path4275\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 384.22708,1012.3775 c -4.74029,0 -8.87879,1.9642 -12.41137,5.8892 -3.50238,3.895 -5.25351,8.4541 -5.25351,13.6775 0,5.5253 1.73793,10.2186 5.21011,14.0834 3.50239,3.8647 7.71202,5.7973 12.63347,5.7973 5.67627,0 10.4327,-2.8239 14.26721,-8.47 l 0,7.4285 7.88027,0 0,-37.3643 -7.88027,0 0,7.6097 c -1.44926,-2.5664 -3.487,-4.6513 -6.1138,-6.2516 -2.62679,-1.6002 -5.40339,-2.3997 -8.33211,-2.3997 z m 3.03264,7.4285 c 3.4118,0 6.34088,1.2225 8.78651,3.6682 2.44562,2.4155 3.66827,5.3288 3.66827,8.7405 0,3.3816 -1.22265,6.3107 -3.66827,8.7866 -2.44563,2.4455 -5.37471,3.6682 -8.78651,3.6682 -3.442,0 -6.37105,-1.2384 -8.78649,-3.7142 -2.41543,-2.4759 -3.62233,-5.4786 -3.62233,-9.0111 0,-3.2608 1.23841,-6.1006 3.71423,-8.5159 2.47583,-2.4154 5.37336,-3.6223 8.69459,-3.6223 z\"
       id=\"path4273\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 467.05552,1012.3775 c -5.46492,0 -10.1004,1.9482 -13.90472,5.8432 -3.77412,3.8647 -5.66195,8.4397 -5.66195,13.7235 0,5.6461 1.87208,10.371 5.616,14.1753 3.74393,3.8043 8.39516,5.7054 13.95067,5.7054 7.24631,0 13.02821,-2.7925 17.3458,-8.3781 l -5.75385,-4.3473 c -3.0193,3.7137 -6.76345,5.57 -11.23201,5.57 -2.89853,0 -5.58569,-0.8754 -8.06153,-2.6267 -2.44562,-1.7813 -4.01534,-4.5894 -4.70978,-8.424 l 31.8836,0 c 0.0604,-6.2801 -1.82489,-11.3835 -5.6594,-15.3087 -3.83451,-3.9552 -8.43848,-5.9326 -13.81283,-5.9326 z m 0,7.2472 c 4.95166,0 8.69325,2.5664 11.22947,7.6991 l -22.82396,0 c 2.32485,-5.1327 6.18995,-7.6991 11.59449,-7.6991 z\"
       id=\"path4271\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 521.24058,1012.3771 c -2.68717,0 -4.87481,0.6942 -6.56562,2.083 -1.66061,1.3587 -3.36703,3.5779 -5.11822,6.6576 l 0,-7.699 -7.69904,0 0,37.3642 7.88028,0 0,-20.6975 c 0,-2.7778 0.89132,-5.0863 2.67271,-6.9282 1.81157,-1.8718 4.04394,-2.8079 6.70092,-2.8079 0.48308,0 1.47959,0.1499 2.98924,0.4518 l 2.40213,-7.8803 c -1.41907,-0.3623 -2.50757,-0.5437 -3.2624,-0.5437 z\"
       id=\"path4269\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 97.58693,1013.8577 -37.63235,33.4509 0,4.23 37.680853,0 0,-37.6809 -0.04849,0 z\"
       id=\"path4267\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 0.69295556,1014.1793 0,37.3593 37.68085044,0 0,-4.1942 -37.62979644,-33.1651 -0.05105,0 z\"
       id=\"path4265\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 249.59691,1041.5858 0,10.063 23.06647,0 -10.05775,-10.0604 0,0 -13.00872,0 z\"
       id=\"text4134\"
       inkscape:connector-curvature=\"0\" />
  </g>
</svg>
";
    }

    public function getTemplateName()
    {
        return "partials/logo.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSource()
    {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg id=\"logo-svg\"  style=\"background-color:#ffffff00\" version=\"1.1\"
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
   xmlns:cc=\"http://creativecommons.org/ns#\"
   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
   xmlns:svg=\"http://www.w3.org/2000/svg\"
   xmlns=\"http://www.w3.org/2000/svg\"
   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"
   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"
   width=\"525\"
   height=\"85\"
   viewBox=\"0 0 525 85.000004\"
   id=\"svg2\"
   version=\"1.1\"
   inkscape:version=\"0.91 r13725\"
   sodipodi:docname=\"MeloDealer Font.svg\">
  <defs
     id=\"defs4\" />
  <sodipodi:namedview
     id=\"base\"
     pagecolor=\"#ffffff\"
     bordercolor=\"#666666\"
     borderopacity=\"1.0\"
     inkscape:pageopacity=\"0.0\"
     inkscape:pageshadow=\"2\"
     inkscape:zoom=\"2.0000001\"
     inkscape:cx=\"270.54379\"
     inkscape:cy=\"25.646239\"
     inkscape:document-units=\"px\"
     inkscape:current-layer=\"layer1\"
     showgrid=\"false\"
     inkscape:window-width=\"2560\"
     inkscape:window-height=\"1377\"
     inkscape:window-x=\"-8\"
     inkscape:window-y=\"-8\"
     inkscape:window-maximized=\"1\"
     units=\"px\" />
  <metadata
     id=\"metadata7\">
    <rdf:RDF>
      <cc:Work
         rdf:about=\"\">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label=\"Ebene 1\"
     inkscape:groupmode=\"layer\"
     id=\"layer1\"
     transform=\"translate(0,-967.36227)\">
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 97.589492,967.50266 -48.675462,43.26624 -1.327426,-1.4985 -46.82983044,-41.62224 0,37.53534 46.82983044,41.6274 1.327426,1.4934 48.675462,-43.2664 0,-37.53524 z\"
       id=\"path4287\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 249.57138,967.55886 0.008,0.008 -0.0766,0 0,71.29264 13.00872,0 0,-58.35797 30.85995,30.86497 -28.28169,28.2869 10.59383,10.5963 35.18683,-35.1893 -3.90568,-3.9057 3.6938,-3.6939 -39.89662,-39.9016 -21.19021,0 z\"
       id=\"path4285\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 163.92984,991.3299 0,58.7868 7.88028,0 0,-58.7868 -7.88028,0 z\"
       id=\"path4283\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 424.82651,991.9961 0,58.7868 7.88029,0 0,-58.7868 -7.88029,0 z\"
       id=\"path4281\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 129.03575,1011.7112 c -5.46493,0 -10.1004,1.9457 -13.90472,5.8407 -3.77412,3.8647 -5.6594,8.4397 -5.6594,13.7235 0,5.6461 1.87209,10.3709 5.61601,14.1753 3.74395,3.8043 8.39262,5.7079 13.94811,5.7079 7.24631,0 13.02822,-2.7924 17.34581,-8.3781 l -5.7513,-4.3498 c -3.01929,3.7137 -6.76346,5.5726 -11.23201,5.5726 -2.89854,0 -5.58572,-0.8757 -8.06153,-2.6268 -2.44563,-1.7814 -4.01535,-4.5895 -4.70979,-8.424 l 31.8836,0 c 0.0602,-6.2801 -1.82743,-11.3836 -5.66195,-15.3087 -3.83451,-3.9554 -8.43848,-5.9326 -13.81283,-5.9326 z m 0,7.2447 c 4.95166,0 8.6958,2.5688 11.23202,7.7016 l -22.82652,0 c 2.32487,-5.1328 6.18997,-7.7016 11.5945,-7.7016 z\"
       id=\"path4279\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 206.15884,1011.7112 c -5.22337,0 -9.85888,1.9932 -13.90472,5.9786 -4.01569,3.9554 -6.02444,8.483 -6.02444,13.5856 0,5.3442 2.02453,9.9953 6.07039,13.9506 4.07605,3.9553 8.87717,5.9326 14.4025,5.9326 5.16299,0 9.7065,-1.9929 13.63159,-5.9784 3.95529,-4.0157 5.93254,-8.6512 5.93254,-13.9048 0,-5.2234 -2.00877,-9.7826 -6.02445,-13.6775 -4.01565,-3.925 -8.70904,-5.8867 -14.08341,-5.8867 z m 0.2706,7.7884 c 3.32121,0 6.1898,1.1781 8.60525,3.5331 2.41542,2.3249 3.62231,5.0724 3.62231,8.2427 0,3.2004 -1.22264,6.0085 -3.66827,8.424 -2.44561,2.4155 -5.29846,3.6223 -8.55929,3.6223 -3.35143,0 -6.28048,-1.1753 -8.7865,-3.5304 -2.47584,-2.3551 -3.71423,-5.1025 -3.71423,-8.2427 0,-3.2911 1.22521,-6.1151 3.67083,-8.47 2.47582,-2.3853 5.41807,-3.579 8.8299,-3.579 z\"
       id=\"path4277\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 333.43541,1012.3775 c -5.46494,0 -10.09785,1.9482 -13.90216,5.8432 -3.77414,3.8647 -5.66196,8.4397 -5.66196,13.7235 0,5.6461 1.87207,10.371 5.61601,14.1753 3.74393,3.8043 8.3926,5.7054 13.94811,5.7054 7.24631,0 13.02821,-2.7925 17.34581,-8.3781 l -5.7513,-4.3473 c -3.0193,3.7137 -6.76345,5.57 -11.23201,5.57 -2.89853,0 -5.58571,-0.8754 -8.06153,-2.6267 -2.44563,-1.7813 -4.01534,-4.5894 -4.70979,-8.424 l 31.8836,0 c 0.0604,-6.2801 -1.82489,-11.3835 -5.65939,-15.3087 -3.83451,-3.9552 -8.44104,-5.9326 -13.81539,-5.9326 z m 0,7.2472 c 4.95164,0 8.69581,2.5664 11.23201,7.6991 l -22.82396,0 c 2.32486,-5.1327 6.1874,-7.6991 11.59195,-7.6991 z\"
       id=\"path4275\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 384.22708,1012.3775 c -4.74029,0 -8.87879,1.9642 -12.41137,5.8892 -3.50238,3.895 -5.25351,8.4541 -5.25351,13.6775 0,5.5253 1.73793,10.2186 5.21011,14.0834 3.50239,3.8647 7.71202,5.7973 12.63347,5.7973 5.67627,0 10.4327,-2.8239 14.26721,-8.47 l 0,7.4285 7.88027,0 0,-37.3643 -7.88027,0 0,7.6097 c -1.44926,-2.5664 -3.487,-4.6513 -6.1138,-6.2516 -2.62679,-1.6002 -5.40339,-2.3997 -8.33211,-2.3997 z m 3.03264,7.4285 c 3.4118,0 6.34088,1.2225 8.78651,3.6682 2.44562,2.4155 3.66827,5.3288 3.66827,8.7405 0,3.3816 -1.22265,6.3107 -3.66827,8.7866 -2.44563,2.4455 -5.37471,3.6682 -8.78651,3.6682 -3.442,0 -6.37105,-1.2384 -8.78649,-3.7142 -2.41543,-2.4759 -3.62233,-5.4786 -3.62233,-9.0111 0,-3.2608 1.23841,-6.1006 3.71423,-8.5159 2.47583,-2.4154 5.37336,-3.6223 8.69459,-3.6223 z\"
       id=\"path4273\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 467.05552,1012.3775 c -5.46492,0 -10.1004,1.9482 -13.90472,5.8432 -3.77412,3.8647 -5.66195,8.4397 -5.66195,13.7235 0,5.6461 1.87208,10.371 5.616,14.1753 3.74393,3.8043 8.39516,5.7054 13.95067,5.7054 7.24631,0 13.02821,-2.7925 17.3458,-8.3781 l -5.75385,-4.3473 c -3.0193,3.7137 -6.76345,5.57 -11.23201,5.57 -2.89853,0 -5.58569,-0.8754 -8.06153,-2.6267 -2.44562,-1.7813 -4.01534,-4.5894 -4.70978,-8.424 l 31.8836,0 c 0.0604,-6.2801 -1.82489,-11.3835 -5.6594,-15.3087 -3.83451,-3.9552 -8.43848,-5.9326 -13.81283,-5.9326 z m 0,7.2472 c 4.95166,0 8.69325,2.5664 11.22947,7.6991 l -22.82396,0 c 2.32485,-5.1327 6.18995,-7.6991 11.59449,-7.6991 z\"
       id=\"path4271\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 521.24058,1012.3771 c -2.68717,0 -4.87481,0.6942 -6.56562,2.083 -1.66061,1.3587 -3.36703,3.5779 -5.11822,6.6576 l 0,-7.699 -7.69904,0 0,37.3642 7.88028,0 0,-20.6975 c 0,-2.7778 0.89132,-5.0863 2.67271,-6.9282 1.81157,-1.8718 4.04394,-2.8079 6.70092,-2.8079 0.48308,0 1.47959,0.1499 2.98924,0.4518 l 2.40213,-7.8803 c -1.41907,-0.3623 -2.50757,-0.5437 -3.2624,-0.5437 z\"
       id=\"path4269\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 97.58693,1013.8577 -37.63235,33.4509 0,4.23 37.680853,0 0,-37.6809 -0.04849,0 z\"
       id=\"path4267\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 0.69295556,1014.1793 0,37.3593 37.68085044,0 0,-4.1942 -37.62979644,-33.1651 -0.05105,0 z\"
       id=\"path4265\"
       inkscape:connector-curvature=\"0\" />
    <path
       style=\"font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"
       d=\"m 249.59691,1041.5858 0,10.063 23.06647,0 -10.05775,-10.0604 0,0 -13.00872,0 z\"
       id=\"text4134\"
       inkscape:connector-curvature=\"0\" />
  </g>
</svg>
";
    }
}
