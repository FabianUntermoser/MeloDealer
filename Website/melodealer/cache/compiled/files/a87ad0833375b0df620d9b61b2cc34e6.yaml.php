<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Website/grav/user/localhost/config/system.yaml',
    'modified' => 1478499332,
    'data' => [
        'assets' => [
            'css_pipeline' => false,
            'js_pipeline' => false
        ],
        'twig' => [
            'cache' => true,
            'debug' => true
        ],
        'debugger' => [
            'enabled' => false
        ]
    ]
];
