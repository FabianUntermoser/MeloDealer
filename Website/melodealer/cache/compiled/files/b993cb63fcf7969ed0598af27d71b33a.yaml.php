<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'E:/xampp/htdocs/Website/melodealer/user/localhost/config/system.yaml',
    'modified' => 1478499332,
    'data' => [
        'assets' => [
            'css_pipeline' => false,
            'js_pipeline' => false
        ],
        'twig' => [
            'cache' => true,
            'debug' => true
        ],
        'debugger' => [
            'enabled' => false
        ]
    ]
];
