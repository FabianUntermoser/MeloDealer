<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/Website/grav/user/config/plugins/simplesearch.yaml',
    'modified' => 1478499332,
    'data' => [
        'enabled' => true,
        'built_in_css' => false,
        'route' => '/search',
        'template' => 'simplesearch_results',
        'filters' => [
            'category' => 'docs'
        ]
    ]
];
