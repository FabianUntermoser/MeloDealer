# MeloDealer - Open-source Multi-Room System

---
Programmierung einer Android Applikation, welche die Ansteuerung einer Lautsprechereinheit über ein Netzwerk ermöglicht.
---

Realisiert mit [MPD](https://www.musicpd.org/) [MPC](https://www.musicpd.org/clients/mpc/) 